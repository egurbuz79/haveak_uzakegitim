﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;


namespace WsHEgitim
{

    [ServiceContract]   
    public interface IPersonel
    {
        [XmlSerializerFormat]
        [WebInvoke(Method = "POST", UriTemplate = "PersonelKaydet/{PersonelData}")]
        [OperationContract]
        string PersonelKaydetTr(PersonelData data);

        [XmlSerializerFormat]
        [WebInvoke(Method = "POST", UriTemplate = "PersonelKaydet/{PersonelData}")]
        [OperationContract]
        string PersonelKaydetEn(PersonelData data);          

    }

    [DataContract]
    public class PersonelData : IDisposable
    {
        [DataMember]
        public String AktifPersonelId { get; set; }
        [DataMember]
        public String PersonelAdi { get; set; }
        [DataMember]
        public String Soyadi { get; set; }
        [DataMember]
        public String TCNo { get; set; }
        [DataMember]
        public String EPosta { get; set; }
        [DataMember]
        public String CepTelefon { get; set; }
        [DataMember]
        public String Resim { get; set; }
        [DataMember]
        public String Dil { get; set; }
        [DataMember]
        public String Sifre { get; set; }

        public void Dispose()
        {
            if (Resim != null)
            {
                Resim = null;
            }
            GC.SuppressFinalize(this);
        }
    }

    //[DataContract]
    //public class SinavSonucu : IDisposable
    //{
    //    [DataMember]
    //    public Boolean egitimSonucu { get; set; }
    //    [DataMember]
    //    public String kimlikNo { get; set; }
    //    [DataMember]
    //    public String referansId { get; set; }
    //    [DataMember]
    //    public String[] resimler { get; set; }
    //    [DataMember]
    //    public String sertifikaNo { get; set; }       
      

    //    public void Dispose()
    //    {
    //        GC.SuppressFinalize(this);
    //    }       
    //}

}
