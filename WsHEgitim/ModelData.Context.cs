﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WsHEgitim
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Heas_UzakEntities : DbContext
    {
        public Heas_UzakEntities()
            : base("name=Heas_UzakEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<AltSiklar> AltSiklars { get; set; }
        public DbSet<AtananBolumler> AtananBolumlers { get; set; }
        public DbSet<Bolum> Bolums { get; set; }
        public DbSet<Duyurular> Duyurulars { get; set; }
        public DbSet<Egitim_Arsivi> Egitim_Arsivi { get; set; }
        public DbSet<EgitimGirisSayfasi> EgitimGirisSayfasis { get; set; }
        public DbSet<Firmalar> Firmalars { get; set; }
        public DbSet<Kull_Rol_Adi> Kull_Rol_Adi { get; set; }
        public DbSet<Kull_Rolleri> Kull_Rolleri { get; set; }
        public DbSet<Kullanici> Kullanicis { get; set; }
        public DbSet<KursModuller> KursModullers { get; set; }
        public DbSet<Loglama> Loglamas { get; set; }
        public DbSet<Mesajlar> Mesajlars { get; set; }
        public DbSet<Moduller> Modullers { get; set; }
        public DbSet<OkunanUniteler> OkunanUnitelers { get; set; }
        public DbSet<Personel> Personels { get; set; }
        public DbSet<Personel_Sinavlar> Personel_Sinavlar { get; set; }
        public DbSet<Personel_Testler> Personel_Testler { get; set; }
        public DbSet<Sinav_Sorular> Sinav_Sorular { get; set; }
        public DbSet<SinavCevaplar> SinavCevaplars { get; set; }
        public DbSet<Sinavlar> Sinavlars { get; set; }
        public DbSet<SinavTipi> SinavTipis { get; set; }
        public DbSet<Sinif> Sinifs { get; set; }
        public DbSet<Sinif_Ogrenci> Sinif_Ogrenci { get; set; }
        public DbSet<SoruSiklari> SoruSiklaris { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<Test_Sorular> Test_Sorular { get; set; }
        public DbSet<TestCevaplar> TestCevaplars { get; set; }
        public DbSet<Testler> Testlers { get; set; }
        public DbSet<Unite> Unites { get; set; }
        public DbSet<UniteModul> UniteModuls { get; set; }
        public DbSet<UniteSorular> UniteSorulars { get; set; }
    }
}
