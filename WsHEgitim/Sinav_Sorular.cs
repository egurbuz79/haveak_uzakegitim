//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WsHEgitim
{
    using System;
    using System.Collections.Generic;
    
    public partial class Sinav_Sorular
    {
        public Sinav_Sorular()
        {
            this.SinavCevaplars = new HashSet<SinavCevaplar>();
        }
    
        public int ID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public int Sinavlar_Ref { get; set; }
        public int UniteSorular_Ref { get; set; }
        public int SinavTipi_Ref { get; set; }
    
        public virtual ICollection<SinavCevaplar> SinavCevaplars { get; set; }
        public virtual SinavTipi SinavTipi { get; set; }
        public virtual Sinavlar Sinavlar { get; set; }
        public virtual UniteSorular UniteSorular { get; set; }
    }
}
