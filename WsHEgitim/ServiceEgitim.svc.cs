﻿using System;
using System.Linq;
using System.IO;
using System.Web;
using System.Net;
using System.ServiceModel;


namespace WsHEgitim
{
    public class ServiceEgitim : IPersonel
    {

        public string PersonelKaydetTr(PersonelData data)
        {
            string durum = "";
            Heas_UzakEntities db = new Heas_UzakEntities();
            try
            {
                Personel detay = db.Personels.Where(q => q.TCNo == data.TCNo).FirstOrDefault();
                if (detay == null)
                {
                    detay = new Personel();
                    detay.IsActive = true;
                    detay.IsDelete = false;
                    detay.Adi = data.PersonelAdi;
                    detay.Soyadi = data.Soyadi;
                    detay.TCNo = data.TCNo;
                    detay.CepTelefon = data.CepTelefon ?? "";
                    detay.Email = data.EPosta ?? "";
                    detay.Adresi = "-";
                    detay.Cinsiyet = "-";
                    detay.DogumTarihi = DateTime.Now;
                    detay.Firma_Ref = 1;
                    detay.IsTelefon = "-";
                    detay.EvTelefon = "-";
                    if (data.Resim != null)
                    {
                        detay.ServiceImage = data.Resim;
                    }
                    db.Personels.Add(detay);
                    db.SaveChanges();
                    KullaniciKaydet(detay.ID, data.TCNo, data.Sifre, data.AktifPersonelId);
                }
                else
                {
                    detay.CepTelefon = data.CepTelefon ?? "";
                    detay.Email = data.EPosta ?? "";
                    if (data.Resim != null)
                    {
                        detay.ServiceImage = data.Resim;
                    }
                    db.SaveChanges();
                    KullaniciKaydet(detay.ID, data.TCNo, data.Sifre, data.AktifPersonelId);
                }

            }
            catch
            {
                //durum = "İşlem sırasında hata meydane geldi mevcut data ve şartları kontrol ederek tekrar deneyiniz";
            }

            return durum;
        }

        public string PersonelKaydetEn(PersonelData data)
        {
            string durum = "";
            Heas_Uzak_ENGLEntities dben = new Heas_Uzak_ENGLEntities();
            try
            {
                Personel detay = dben.Personels.Where(q => q.TCNo == data.TCNo).FirstOrDefault();
                if (detay == null)
                {
                    detay = new Personel();
                    detay.IsActive = true;
                    detay.IsDelete = false;
                    detay.Adi = data.PersonelAdi;
                    detay.Soyadi = data.Soyadi;
                    detay.TCNo = data.TCNo;
                    detay.CepTelefon = data.CepTelefon;
                    detay.Email = data.EPosta;
                    detay.Adresi = "-";
                    detay.Cinsiyet = "-";
                    detay.DogumTarihi = DateTime.Now;
                    detay.Firma_Ref = 1;
                    detay.IsTelefon = "-";
                    detay.EvTelefon = "-";
                    if (data.Resim != null)
                    {
                        detay.ServiceImage = data.Resim;
                    }

                    dben.Personels.Add(detay);
                    dben.SaveChanges();
                    KullaniciKaydetENG(detay.ID, data.TCNo, data.Sifre, data.AktifPersonelId); ;
                }
                else
                {
                    detay.CepTelefon = data.CepTelefon;
                    detay.Email = data.EPosta;
                    if (data.Resim != null)
                    {
                        detay.ServiceImage = data.Resim;
                        //detay.Resim = savedocument(data.PersonelAdi + data.Soyadi, data.Resim);
                    }
                    dben.SaveChanges();
                    KullaniciKaydetENG(detay.ID, data.TCNo, data.Sifre, data.AktifPersonelId);
                }

            }
            catch (Exception ex)
            {
                durum = ex.Message;
            }

            return durum;
        }

        //public SinavSonucu SinavSonucuDurumENG(string aktifPersonelId)
        //{
        //    int apID = Convert.ToInt32(aktifPersonelId);
        //    Heas_Uzak_ENGLEntities dben = new Heas_Uzak_ENGLEntities();
        //    var data = dben.Personel_Sinavlar.
        //        OrderByDescending(q => q.ID).
        //        Where(e =>
        //            e.Sinif_Ogrenci.AktifPersonelId == apID).
        //            Select(w =>
        //                new SinavSonucu
        //                {
        //                    egitimSonucu = w.SinavAciklama,
        //                    kimlikNo = w.Sinif_Ogrenci.Personel.TCNo,
        //                    referansId = w.Sinif_Ogrenci.AktifPersonelId.Value.ToString(),
        //                    sertifikaNo = w.SertifikaNo
        //                }).FirstOrDefault();
        //    string path = "http://www.heasuzaktanegitim.xyz/ENG/DocumentFiles/Personel/" + data.KlasorYol;
        //    string[] filePaths = Directory.GetFiles(path);
        //    System.Collections.Generic.List<String> Rsmler = new System.Collections.Generic.List<String>();
        //    foreach (var adres in filePaths)
        //    {
        //        using (System.Drawing.Image image = System.Drawing.Image.FromFile(adres))
        //        {
        //            using (MemoryStream m = new MemoryStream())
        //            {
        //                image.Save(m, image.RawFormat);
        //                byte[] imageBytes = m.ToArray();
        //                // Convert byte[] to Base64 String
        //                string base64String = Convert.ToBase64String(imageBytes);
        //                Rsmler.Add(base64String);
        //            }
        //        }
        //    }
        //    for (int i = 0; i < Rsmler.Count; i++)
        //    {
        //        data.resimler[i] = Rsmler[i];
        //    }
        //    return data;
        //}

        //public SinavSonucu SinavSonucuDurumTUR(string aktifPersonelId)
        //{
        //    int apID = Convert.ToInt32(aktifPersonelId);
        //    Heas_UzakEntities dben = new Heas_UzakEntities();
        //    var data = dben.Personel_Sinavlar.OrderByDescending(q => q.ID).
        //        Where(e => e.Sinif_Ogrenci.AktifPersonelId == apID).
        //        Select(w =>
        //            new SinavSonucu
        //            {
        //                AdSoyad = w.Sinif_Ogrenci.Personel.Adi + "_" + w.Sinif_Ogrenci.Personel.Soyadi,
        //                Not = w.SinavNotuTeorik.Value,
        //                KlasorYol = w.KlasorYolu,
        //                AktifPersonelId = w.Sinif_Ogrenci.AktifPersonelId.Value.ToString(),
        //                SertifikaNo = w.SertifikaNo
        //            }).FirstOrDefault();

        //    string path = "http://www.heasuzaktanegitim.xyz/DocumentFiles/Personel/" + data.KlasorYol;
        //    string[] filePaths = Directory.GetFiles(path);
        //    System.Collections.Generic.List<String> Rsmler = new System.Collections.Generic.List<String>();
        //    foreach (var adres in filePaths)
        //    {
        //        using (System.Drawing.Image image = System.Drawing.Image.FromFile(adres))
        //        {
        //            using (MemoryStream m = new MemoryStream())
        //            {
        //                image.Save(m, image.RawFormat);
        //                byte[] imageBytes = m.ToArray();
        //                // Convert byte[] to Base64 String
        //                string base64String = Convert.ToBase64String(imageBytes);
        //                Rsmler.Add(base64String);
        //            }
        //        }

        //    }
        //    for (int i = 0; i < Rsmler.Count; i++)
        //    {
        //        data.Resimler[i] = Rsmler[i];
        //    }

        //    return data;
        //}

        #region Türkçe
        public void SinavSonucOnayTr(string tcno, int aktifPersonelId, string sonuc)
        {
            Heas_UzakEntities db = new Heas_UzakEntities();
            var personel = db.Personel_Sinavlar.Where(e => e.Sinif_Ogrenci.AktifPersonelId == aktifPersonelId && e.Sinif_Ogrenci.Personel.TCNo == tcno).FirstOrDefault();
            if (sonuc != "GEÇTİ")
            {
                personel.SinavNotuTeorik = 0;
                personel.Durum = "KALDI";
            }
            db.SaveChanges();
        }


        public void SinavSonucOnayEn(string tcno, int aktifPersonelId, string sonuc)
        {
            Heas_Uzak_ENGLEntities db = new Heas_Uzak_ENGLEntities();
            var personel = db.Personel_Sinavlar.Where(e => e.Sinif_Ogrenci.AktifPersonelId == aktifPersonelId && e.Sinif_Ogrenci.Personel.TCNo == tcno).FirstOrDefault();
            if (sonuc != "success")
            {
                personel.SinavNotuTeorik = 0;
                personel.Durum = "unseccess";
            }
            db.SaveChanges();
        }

        protected void KullaniciKaydet(int prs_Ref, string tcno, string sifre, string aktifPersonelId)
        {
            //string durum = "";            
            Heas_UzakEntities db = new Heas_UzakEntities();
            try
            {
                Kullanici kontrol = db.Kullanicis.Where(t => t.Personel.TCNo == tcno).FirstOrDefault();
                //if (kontrol != null)
                //{
                //    int ay, yil;
                //    ay = kontrol.BaslangicTarihi.Month;
                //    yil = kontrol.BaslangicTarihi.Year;
                //    yil = yil + 3;
                //    var gectiKaldi = db.Personel_Sinavlar.OrderByDescending(y => y.ID).Where(w => w.Sinif_Ogrenci.Personel_Ref == kontrol.Personel_Ref).Select(r => r.SinavNotuTeorik).FirstOrDefault();
                //    if ((ay == DateTime.Now.Month && yil < DateTime.Now.Year) || gectiKaldi < 70)
                //    { }
                //    else
                //    {
                //        return;
                //    }
                //}


                if (kontrol != null)
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    kontrol.BaslangicTarihi = DateTime.Now;
                    kontrol.BitisTarihi = DateTime.Now.AddDays(2);
                    kontrol.KullaniciAdi = tcno;
                    var sfr = sifre;
                    if (string.IsNullOrEmpty(sifre))
                    {
                        sfr = Guid.NewGuid().ToString().Split('-')[0];
                    }
                    kontrol.Sifre = sfr;
                    db.SaveChanges();
                }
                else
                {
                    kontrol = new Kullanici();
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    kontrol.KullaniciAdi = tcno;
                    var sfr = sifre;
                    if (string.IsNullOrEmpty(sifre))
                    {
                        sfr = Guid.NewGuid().ToString().Split('-')[0];
                    }
                    //string sfr = Guid.NewGuid().ToString().Split('-')[0];                  
                    kontrol.Sifre = sfr;
                    kontrol.BaslangicTarihi = DateTime.Now;
                    kontrol.BitisTarihi = DateTime.Now.AddDays(2);
                    kontrol.Personel_Ref = prs_Ref;
                    kontrol.Sistemde = false;
                    kontrol.Songiristarihi = DateTime.Now.Date;
                    db.Kullanicis.Add(kontrol);
                    db.SaveChanges();

                    for (int i = 2; i < 4; i++)
                    {
                        Kull_Rolleri rolKontrol = new Kull_Rolleri()
                        {
                            IsActive = true,
                            IsDelete = false,
                            User_Role_Name_Ref = i,
                            User_Ref = kontrol.ID
                        };
                        db.Kull_Rolleri.Add(rolKontrol);
                    }
                    db.SaveChanges();
                }

                int apId = Convert.ToInt32(aktifPersonelId);
                Sinif_Ogrenci kayitKontrol = db.Sinif_Ogrenci.Where(e => e.AktifPersonelId == apId).FirstOrDefault();
                if (kayitKontrol == null)
                {
                    Sinif snf = db.Sinifs.OrderByDescending(e => e.ID).FirstOrDefault();
                    int lastID = snf.ID + 1;
                    int sinifOgrenciSayisi = db.Sinif_Ogrenci.Where(w => w.Sinif_Ref == snf.ID).Count();
                    if (sinifOgrenciSayisi > 50)
                    {
                        snf = new Sinif();
                        snf.IsActive = true;
                        snf.IsDelete = false;
                        snf.Firma_Ref = 1; //Uzaktan Eğitim Merkezi
                        snf.SinifAdi = "Uzak_Egitim" + lastID;
                        db.Sinifs.Add(snf);
                        db.SaveChanges();
                    }

                    Sinif_Ogrenci ogr = new Sinif_Ogrenci()
                    {
                        Personel_Ref = prs_Ref,
                        Sinif_Ref = snf.ID,
                        Basladi = true,
                        Bitti = false,
                        BaslamaTarihi = DateTime.Now,
                        BitisTarihi = DateTime.Now.AddDays(2),
                        AktifPersonelId = apId
                    };
                    db.Sinif_Ogrenci.Add(ogr);
                    db.SaveChanges();

                    AtananBolumler blm = new AtananBolumler();
                    blm.Bolum_Ref = 1;
                    blm.Sinif_Ogrenci_Ref = ogr.ID;
                    db.AtananBolumlers.Add(blm);
                    db.SaveChanges();

                    //bool sinavEkle = SinavKontrol(prs_Ref);
                    //if (sinavEkle)
                    //{
                    DateTime bugun = DateTime.Now.Date;
                    //TimeSpan baslaSaati = TimeSpan.Parse("16:00:00");
                    //TimeSpan bitisSaati = TimeSpan.Parse("16:20:00");
                    Personel_Sinavlar sinavata = new Personel_Sinavlar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        Sinif_Ogrenci_Ref = ogr.ID,
                        Sinavlar_Ref = SivanNoAta(prs_Ref),
                        SinavAciklama = false,
                        SinavNotuUygulama = 0,
                        SinavNotuTeorik = 0,
                        Durum = "AÇIKLANMADI",
                        DogruSayisiUygulama = 0,
                        YanlisSayisiUgulama = 0,
                        UygulamaBos = 0,
                        DogruSayisiTeorik = 0,
                        YanlisSayisiTeorik = 0,
                        TeorikBos = 0,
                        SinavTarihi = bugun,
                        //BaslamaSaati = baslaSaati,
                        //BitisSaati = bitisSaati,
                        SinavSuresi = 20,
                        Basladi = 0,
                        Bitti = 0
                    };
                    db.Personel_Sinavlar.Add(sinavata);
                    db.SaveChanges();
                    //durum = "Kayıt başarı ile gerçekleşti.";
                    //}
                }
                else
                {
                    Sinif snf = db.Sinifs.OrderByDescending(e => e.ID).FirstOrDefault();
                    int sinifOgrenciSayisi = db.Sinif_Ogrenci.Where(w => w.Sinif_Ref == snf.ID).Count();
                    bool sinifKontrol = true;
                    if (sinifOgrenciSayisi > 50)
                    {
                        snf = new Sinif();
                        snf.IsActive = true;
                        snf.IsDelete = false;
                        snf.Firma_Ref = 1; //Uzaktan Eğitim Merkezi
                        snf.SinifAdi = "Uzak_Egitim" + (snf.ID + 1);
                        db.Sinifs.Add(snf);
                        db.SaveChanges();
                        sinifKontrol = false;
                    }
                    Sinif_Ogrenci ogr = db.Sinif_Ogrenci.Where(q => q.AktifPersonelId == apId).FirstOrDefault();
                    ogr.Basladi = true;
                    ogr.Bitti = false;
                    ogr.BaslamaTarihi = DateTime.Now;
                    ogr.BitisTarihi = DateTime.Now.AddDays(2);
                    if (!sinifKontrol)
                    {
                        ogr.Personel_Ref = kontrol.Personel_Ref;
                        ogr.AktifPersonelId = apId;
                        ogr.Sinif_Ref = snf.ID;
                        db.Sinif_Ogrenci.Add(ogr);
                    }
                    db.SaveChanges();

                    if (!sinifKontrol)
                    {
                        var atamaKontrol = db.AtananBolumlers.Where(w => w.Sinif_Ogrenci_Ref == ogr.ID).FirstOrDefault();
                        AtananBolumler blm = new AtananBolumler();
                        blm.Bolum_Ref = 1;
                        blm.Sinif_Ogrenci_Ref = ogr.ID;
                        db.AtananBolumlers.Add(blm);
                        db.SaveChanges();
                    }

                    //bool sinavEkle = SinavKontrol(prs_Ref);
                    //if (sinavEkle)
                    //{
                    DateTime bugun = DateTime.Now.Date;
                    //TimeSpan baslaSaati = TimeSpan.Parse("16:00:00");
                    //TimeSpan bitisSaati = TimeSpan.Parse("16:20:00");
                    Personel_Sinavlar sinavata = new Personel_Sinavlar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        Sinif_Ogrenci_Ref = ogr.ID,
                        Sinavlar_Ref = SivanNoAta(prs_Ref),
                        SinavAciklama = false,
                        SinavNotuUygulama = 0,
                        SinavNotuTeorik = 0,
                        Durum = "AÇIKLANMADI",
                        DogruSayisiUygulama = 0,
                        YanlisSayisiUgulama = 0,
                        UygulamaBos = 0,
                        DogruSayisiTeorik = 0,
                        YanlisSayisiTeorik = 0,
                        TeorikBos = 0,
                        SinavTarihi = bugun,
                        //BaslamaSaati = baslaSaati,
                        //BitisSaati = bitisSaati,
                        SinavSuresi = 20,
                        Basladi = 0,
                        Bitti = 0
                    };
                    db.Personel_Sinavlar.Add(sinavata);
                    db.SaveChanges();
                    //durum = "Kayıt başarı ile gerçekleşti.";
                    //}
                }

            }
            catch
            {
                //durum = "İşlem sırasında hata oluştu ! kontrol edip bilgileri tekrar deneyiniz.";
            }
            //return durum;
        }

        bool SinavKontrol(int personelRef)
        {
            bool durum = true;
            using (Heas_UzakEntities db = new Heas_UzakEntities())
            {
                DateTime bugun = DateTime.Now.Date;
                int kontrol = db.Personel_Sinavlar.Where(d => d.SinavTarihi == null && d.Sinif_Ogrenci.Personel_Ref == personelRef).Count();
                if (kontrol > 0)
                {
                    durum = false;
                }
            }
            return durum;
        }

        int SivanNoAta(int personelRef)
        {
            int? sinavNo = 0;
            using (Heas_UzakEntities db = new Heas_UzakEntities())
            {
                int[] sinavlar = db.Personel_Sinavlar.Where(w => w.Sinif_Ogrenci.Personel_Ref == personelRef).Select(r => r.Sinavlar_Ref).ToArray();
                sinavNo = (from c in db.Sinavlars where !sinavlar.Contains(c.ID) select c.ID).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
                if (sinavNo == 0 || sinavNo == null)
                {
                    sinavNo = (from c in db.Sinavlars where sinavlar.Contains(c.ID) select c.ID).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
                }
            }
            return sinavNo.Value;
        }
        #endregion

        #region Ingilizce
        private string savedocumentENG(string fileName, string fileStream)
        {
            string adres = "";
            if (fileStream != null)
            {
                string Fname = Guid.NewGuid().ToString().Split('-')[0] + fileName;
                adres = "http://heasuzaktanegitim.xyz/ENG/DocumentFiles/Personel/" + Fname;

                byte[] imageBytes = Convert.FromBase64String(fileStream);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                image.Save(adres);
                //FileStream fileToupload = new FileStream(adres, FileMode.Create);

                //byte[] bytearray = new byte[10000];
                //int bytesRead, totalBytesRead = 0;
                //do
                //{
                //    bytesRead = fileStream.Read(bytearray, 0, bytearray.Length);
                //    totalBytesRead += bytesRead;
                //} while (bytesRead > 0);

                //fileToupload.Write(bytearray, 0, bytearray.Length);
                //fileToupload.Close();
                //fileToupload.Dispose();
            }
            return adres;
        }

        protected void KullaniciKaydetENG(int prs_Ref, string tcno, string sifre, string aktifPersonelId)
        {
            //string durum = "";

            Heas_Uzak_ENGLEntities dben = new Heas_Uzak_ENGLEntities();
            try
            {
                Kullanici kontrol = dben.Kullanicis.Where(t => t.Personel.TCNo == tcno).FirstOrDefault();


                if (kontrol != null)
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    kontrol.BaslangicTarihi = DateTime.Now;
                    kontrol.BitisTarihi = DateTime.Now.AddDays(2);
                    kontrol.KullaniciAdi = tcno;
                    var sfr = sifre;
                    if (string.IsNullOrEmpty(sifre))
                    {
                        sfr = Guid.NewGuid().ToString().Split('-')[0];
                    }
                    kontrol.Sifre = sfr;
                    dben.SaveChanges();
                }
                else
                {
                    kontrol = new Kullanici();
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    kontrol.KullaniciAdi = tcno;
                    var sfr = sifre;
                    if (string.IsNullOrEmpty(sifre))
                    {
                        sfr = Guid.NewGuid().ToString().Split('-')[0];
                    }
                    //string sfr = Guid.NewGuid().ToString().Split('-')[0];                  
                    kontrol.Sifre = sfr;
                    kontrol.BaslangicTarihi = DateTime.Now;
                    kontrol.BitisTarihi = DateTime.Now.AddDays(2);
                    kontrol.Personel_Ref = prs_Ref;
                    kontrol.Sistemde = false;
                    kontrol.Songiristarihi = DateTime.Now.Date;
                    dben.Kullanicis.Add(kontrol);
                    dben.SaveChanges();

                    for (int i = 2; i < 4; i++)
                    {
                        Kull_Rolleri rolKontrol = new Kull_Rolleri()
                        {
                            IsActive = true,
                            IsDelete = false,
                            User_Role_Name_Ref = i,
                            User_Ref = kontrol.ID
                        };
                        dben.Kull_Rolleri.Add(rolKontrol);
                    }
                    dben.SaveChanges();
                }

                int apIdEng = Convert.ToInt32(aktifPersonelId);
                Sinif_Ogrenci kayitKontrol = dben.Sinif_Ogrenci.Where(e => e.AktifPersonelId == apIdEng).FirstOrDefault();
                if (kayitKontrol == null)
                {
                    Sinif snf = dben.Sinifs.OrderByDescending(e => e.ID).FirstOrDefault();
                    int sinifOgrenciSayisi = dben.Sinif_Ogrenci.Where(w => w.Sinif_Ref == snf.ID).Count();
                    if (sinifOgrenciSayisi > 50)
                    {
                        snf = new Sinif();
                        snf.IsActive = true;
                        snf.IsDelete = false;
                        snf.Firma_Ref = 1;
                        snf.SinifAdi = "Uzak_Egitim" + (snf.ID + 1);
                        dben.Sinifs.Add(snf);
                        dben.SaveChanges();
                    }

                    Sinif_Ogrenci ogr = new Sinif_Ogrenci()
                    {
                        Personel_Ref = prs_Ref,
                        Sinif_Ref = snf.ID,
                        Basladi = true,
                        Bitti = false,
                        BaslamaTarihi = DateTime.Now,
                        BitisTarihi = DateTime.Now.AddDays(2),
                        AktifPersonelId = apIdEng
                    };
                    dben.Sinif_Ogrenci.Add(ogr);
                    dben.SaveChanges();

                    AtananBolumler blm = new AtananBolumler();
                    blm.Bolum_Ref = 1;
                    blm.Sinif_Ogrenci_Ref = ogr.ID;
                    dben.AtananBolumlers.Add(blm);
                    dben.SaveChanges();


                    //bool sinavEkle = SinavKontrol(prs_Ref);
                    //if (sinavEkle)
                    //{
                    DateTime bugun = DateTime.Now.Date;
                    //TimeSpan baslaSaati = TimeSpan.Parse("16:00:00");
                    //TimeSpan bitisSaati = TimeSpan.Parse("16:20:00");
                    Personel_Sinavlar sinavata = new Personel_Sinavlar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        Sinif_Ogrenci_Ref = ogr.ID,
                        Sinavlar_Ref = SivanNoAta(prs_Ref),
                        SinavAciklama = false,
                        SinavNotuUygulama = 0,
                        SinavNotuTeorik = 0,
                        Durum = "Undisclosed",
                        DogruSayisiUygulama = 0,
                        YanlisSayisiUgulama = 0,
                        UygulamaBos = 0,
                        DogruSayisiTeorik = 0,
                        YanlisSayisiTeorik = 0,
                        TeorikBos = 0,
                        SinavTarihi = bugun,
                        //BaslamaSaati = baslaSaati,
                        //BitisSaati = bitisSaati,
                        SinavSuresi = 20,
                        Basladi = 0,
                        Bitti = 0
                    };
                    dben.Personel_Sinavlar.Add(sinavata);
                    dben.SaveChanges();
                    //}
                }
                else
                {
                    Sinif snf = dben.Sinifs.OrderByDescending(e => e.ID).FirstOrDefault();
                    int sinifOgrenciSayisi = dben.Sinif_Ogrenci.Where(w => w.Sinif_Ref == snf.ID).Count();
                    bool sinifKontrol = true;
                    int snfID = snf.ID + 1;
                    if (sinifOgrenciSayisi > 50)
                    {
                        snf = new Sinif();
                        snf.IsActive = true;
                        snf.IsDelete = false;
                        snf.Firma_Ref = 1; //Uzaktan eğitim;
                        snf.SinifAdi = "Uzak_Egitim" + snfID;
                        dben.Sinifs.Add(snf);
                        dben.SaveChanges();
                        sinifKontrol = false;
                    }

                    Sinif_Ogrenci ogr = dben.Sinif_Ogrenci.Where(q => q.AktifPersonelId == apIdEng).FirstOrDefault();
                    ogr.Basladi = true;
                    ogr.Bitti = false;
                    ogr.BaslamaTarihi = DateTime.Now;
                    ogr.BitisTarihi = DateTime.Now.AddDays(2);
                    if (!sinifKontrol)
                    {
                        ogr.Personel_Ref = kontrol.Personel_Ref;
                        ogr.AktifPersonelId = apIdEng;
                        ogr.Sinif_Ref = snf.ID;
                        dben.Sinif_Ogrenci.Add(ogr);
                    }
                    dben.SaveChanges();

                    if (!sinifKontrol)
                    {
                        var atamaKontrol = dben.AtananBolumlers.Where(w => w.Sinif_Ogrenci_Ref == ogr.ID).FirstOrDefault();
                        AtananBolumler blm = new AtananBolumler();
                        blm.Bolum_Ref = 1;
                        blm.Sinif_Ogrenci_Ref = ogr.ID;
                        dben.AtananBolumlers.Add(blm);
                        dben.SaveChanges();
                    }


                    //bool sinavEkle = SinavKontrol(prs_Ref);
                    //if (sinavEkle)
                    //{
                    DateTime bugun = DateTime.Now.Date;
                    //TimeSpan baslaSaati = TimeSpan.Parse("16:00:00");
                    //TimeSpan bitisSaati = TimeSpan.Parse("16:20:00");
                    Personel_Sinavlar sinavata = new Personel_Sinavlar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        Sinif_Ogrenci_Ref = ogr.ID,
                        Sinavlar_Ref = SivanNoAtaENG(prs_Ref),
                        SinavAciklama = false,
                        SinavNotuUygulama = 0,
                        SinavNotuTeorik = 0,
                        Durum = "Undisclosed",
                        DogruSayisiUygulama = 0,
                        YanlisSayisiUgulama = 0,
                        UygulamaBos = 0,
                        DogruSayisiTeorik = 0,
                        YanlisSayisiTeorik = 0,
                        TeorikBos = 0,
                        SinavTarihi = bugun,
                        //BaslamaSaati = baslaSaati,
                        //BitisSaati = bitisSaati,
                        SinavSuresi = 20,
                        Basladi = 0,
                        Bitti = 0
                    };
                    dben.Personel_Sinavlar.Add(sinavata);
                    dben.SaveChanges();
                    //durum = "Kayıt başarı ile gerçekleşti.";
                    //} 

                }

            }
            catch
            {
                //durum = "İşlem sırasında hata oluştu ! kontrol edip bilgileri tekrar deneyiniz.";
            }
            //return durum;
        }

        bool SinavKontrolENG(int personelRef)
        {
            bool durum = true;
            using (Heas_Uzak_ENGLEntities dben = new Heas_Uzak_ENGLEntities())
            {
                DateTime bugun = DateTime.Now.Date;
                int kontrol = dben.Personel_Sinavlar.Where(d => d.SinavTarihi == bugun && d.Sinif_Ogrenci.Personel_Ref == personelRef).Count();
                if (kontrol > 0)
                {
                    durum = false;
                }
            }
            return durum;
        }

        int SivanNoAtaENG(int personelRef)
        {
            int? sinavNo = 0;
            using (Heas_Uzak_ENGLEntities dben = new Heas_Uzak_ENGLEntities())
            {
                int[] sinavlar = dben.Personel_Sinavlar.Where(w => w.Sinif_Ogrenci.Personel_Ref == personelRef).Select(r => r.Sinavlar_Ref).ToArray();
                sinavNo = (from c in dben.Sinavlars where !sinavlar.Contains(c.ID) select c.ID).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
                if (sinavNo == 0 || sinavNo == null)
                {
                    sinavNo = (from c in dben.Sinavlars where sinavlar.Contains(c.ID) select c.ID).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
                }
            }
            return sinavNo.Value;
        }

        #endregion


    }

}
