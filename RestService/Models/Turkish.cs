﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestService.Models
{
    public class Turkish
    {
        #region Türkçe
        public int FirmaKontrol(string firmaAdi)
        {
            using (Heas_UzakEntities db = new Heas_UzakEntities())
            {
                int firmaRef = 1;
                var konrol = db.Firmalars.Where(e => e.FirmaAdi == firmaAdi).FirstOrDefault();
                if (konrol != null)
                {
                    firmaRef = konrol.ID;
                }
                else
                {
                    Firmalar frm = new Firmalar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        FirmaAdi = firmaAdi,
                        Email = "",
                        Telefon = "",
                        Detay = ""
                    };
                    db.Firmalars.Add(frm);
                    db.SaveChanges();
                    firmaRef = frm.ID;
                }
                return firmaRef;
            }
        }


        public void SinavSonucOnayTr(string tcno, int aktifPersonelId, string sonuc)
        {
            Heas_UzakEntities db = new Heas_UzakEntities();
            var personel = db.Personel_Sinavlar.Where(e => e.Sinif_Ogrenci.AktifPersonelId == aktifPersonelId && e.Sinif_Ogrenci.Personel.TCNo == tcno).FirstOrDefault();
            if (sonuc != "GEÇTİ")
            {
                personel.SinavNotuTeorik = 0;
                personel.Durum = "KALDI";
            }
            db.SaveChanges();
        }


        public void SinavSonucOnayEn(string tcno, int aktifPersonelId, string sonuc)
        {
            Heas_Uzak_ENEntities db = new Heas_Uzak_ENEntities();
            var personel = db.Personel_Sinavlar.Where(e => e.Sinif_Ogrenci.AktifPersonelId == aktifPersonelId && e.Sinif_Ogrenci.Personel.TCNo == tcno).FirstOrDefault();
            if (sonuc != "success")
            {
                personel.SinavNotuTeorik = 0;
                personel.Durum = "unseccess";
            }
            db.SaveChanges();
        }

        public void KullaniciKaydet(int prs_Ref, string tcno, string sifre, string aktifPersonelId)
        {
            //string durum = "";            
            Heas_UzakEntities db = new Heas_UzakEntities();
            try
            {
                Kullanici kontrol = db.Kullanicis.Where(t => t.Personel.TCNo == tcno).FirstOrDefault();
                //if (kontrol != null)
                //{
                //    int ay, yil;
                //    ay = kontrol.BaslangicTarihi.Month;
                //    yil = kontrol.BaslangicTarihi.Year;
                //    yil = yil + 3;
                //    var gectiKaldi = db.Personel_Sinavlar.OrderByDescending(y => y.ID).Where(w => w.Sinif_Ogrenci.Personel_Ref == kontrol.Personel_Ref).Select(r => r.SinavNotuTeorik).FirstOrDefault();
                //    if ((ay == DateTime.Now.Month && yil < DateTime.Now.Year) || gectiKaldi < 70)
                //    { }
                //    else
                //    {
                //        return;
                //    }
                //}


                if (kontrol != null)
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    kontrol.BaslangicTarihi = DateTime.Now;
                    kontrol.BitisTarihi = DateTime.Now.AddDays(1);
                    kontrol.KullaniciAdi = tcno;
                    var sfr = sifre;
                    if (string.IsNullOrEmpty(sifre))
                    {
                        sfr = Guid.NewGuid().ToString().Split('-')[0];
                    }
                    kontrol.Sifre = sfr;
                    db.SaveChanges();
                }
                else
                {
                    kontrol = new Kullanici();
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    kontrol.KullaniciAdi = tcno;
                    var sfr = sifre;
                    if (string.IsNullOrEmpty(sifre))
                    {
                        sfr = Guid.NewGuid().ToString().Split('-')[0];
                    }
                    //string sfr = Guid.NewGuid().ToString().Split('-')[0];                  
                    kontrol.Sifre = sfr;
                    kontrol.BaslangicTarihi = DateTime.Now;
                    kontrol.BitisTarihi = DateTime.Now.AddDays(1);
                    kontrol.Personel_Ref = prs_Ref;
                    kontrol.Sistemde = false;
                    kontrol.Songiristarihi = DateTime.Now.Date;
                    db.Kullanicis.Add(kontrol);
                    db.SaveChanges();

                    for (int i = 2; i < 4; i++)
                    {
                        Kull_Rolleri rolKontrol = new Kull_Rolleri()
                        {
                            IsActive = true,
                            IsDelete = false,
                            User_Role_Name_Ref = i,
                            User_Ref = kontrol.ID
                        };
                        db.Kull_Rolleri.Add(rolKontrol);
                    }
                    db.SaveChanges();
                }

                int apId = Convert.ToInt32(aktifPersonelId);
                Sinif_Ogrenci kayitKontrol = db.Sinif_Ogrenci.Where(e => e.AktifPersonelId == apId).FirstOrDefault();
                if (kayitKontrol == null)
                {
                    Sinif snf = db.Sinifs.OrderByDescending(e => e.ID).FirstOrDefault();
                    int lastID = snf.ID + 1;
                    int sinifOgrenciSayisi = db.Sinif_Ogrenci.Where(w => w.Sinif_Ref == snf.ID).Count();
                    if (sinifOgrenciSayisi > 50)
                    {
                        snf = new Sinif();
                        snf.IsActive = true;
                        snf.IsDelete = false;
                        snf.Firma_Ref = 1; //Uzaktan Eğitim Merkezi
                        snf.SinifAdi = "Uzak_Egitim" + lastID;
                        db.Sinifs.Add(snf);
                        db.SaveChanges();
                    }

                    Sinif_Ogrenci ogr = new Sinif_Ogrenci()
                    {
                        Personel_Ref = prs_Ref,
                        Sinif_Ref = snf.ID,
                        Basladi = true,
                        Bitti = false,
                        BaslamaTarihi = DateTime.Now,
                        BitisTarihi = DateTime.Now.AddDays(1),
                        AktifPersonelId = apId
                    };
                    db.Sinif_Ogrenci.Add(ogr);
                    db.SaveChanges();

                    AtananBolumler blm = new AtananBolumler();
                    blm.Bolum_Ref = 1;
                    blm.Sinif_Ogrenci_Ref = ogr.ID;
                    db.AtananBolumlers.Add(blm);
                    db.SaveChanges();

                    //bool sinavEkle = SinavKontrol(prs_Ref);
                    //if (sinavEkle)
                    //{
                    DateTime bugun = DateTime.Now.Date;
                    //TimeSpan baslaSaati = TimeSpan.Parse("16:00:00");
                    //TimeSpan bitisSaati = TimeSpan.Parse("16:20:00");
                    Personel_Sinavlar sinavata = new Personel_Sinavlar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        Sinif_Ogrenci_Ref = ogr.ID,
                        Sinavlar_Ref = SivanNoAta(prs_Ref),
                        SinavAciklama = false,
                        SinavNotuUygulama = 0,
                        SinavNotuTeorik = 0,
                        Durum = "AÇIKLANMADI",
                        DogruSayisiUygulama = 0,
                        YanlisSayisiUgulama = 0,
                        UygulamaBos = 0,
                        DogruSayisiTeorik = 0,
                        YanlisSayisiTeorik = 0,
                        TeorikBos = 0,
                        SinavTarihi = bugun,
                        //BaslamaSaati = baslaSaati,
                        //BitisSaati = bitisSaati,
                        SinavSuresi = 20,
                        Basladi = 0,
                        Bitti = 0
                    };
                    db.Personel_Sinavlar.Add(sinavata);
                    db.SaveChanges();
                    //durum = "Kayıt başarı ile gerçekleşti.";
                    //}
                }
                else
                {
                    Sinif snf = db.Sinifs.OrderByDescending(e => e.ID).FirstOrDefault();
                    int sinifOgrenciSayisi = db.Sinif_Ogrenci.Where(w => w.Sinif_Ref == snf.ID).Count();
                    bool sinifKontrol = true;
                    if (sinifOgrenciSayisi > 50)
                    {
                        snf = new Sinif();
                        snf.IsActive = true;
                        snf.IsDelete = false;
                        snf.Firma_Ref = 1; //Uzaktan Eğitim Merkezi
                        snf.SinifAdi = "Uzak_Egitim" + (snf.ID + 1);
                        db.Sinifs.Add(snf);
                        db.SaveChanges();
                        sinifKontrol = false;
                    }
                    Sinif_Ogrenci ogr = db.Sinif_Ogrenci.Where(q => q.AktifPersonelId == apId).FirstOrDefault();
                    ogr.Basladi = true;
                    ogr.Bitti = false;
                    ogr.BaslamaTarihi = DateTime.Now;
                    ogr.BitisTarihi = DateTime.Now.AddDays(1);
                    if (!sinifKontrol)
                    {
                        ogr.Personel_Ref = kontrol.Personel_Ref;
                        ogr.AktifPersonelId = apId;
                        ogr.Sinif_Ref = snf.ID;
                        db.Sinif_Ogrenci.Add(ogr);
                    }
                    db.SaveChanges();

                    if (!sinifKontrol)
                    {
                        var atamaKontrol = db.AtananBolumlers.Where(w => w.Sinif_Ogrenci_Ref == ogr.ID).FirstOrDefault();
                        AtananBolumler blm = new AtananBolumler();
                        blm.Bolum_Ref = 1;
                        blm.Sinif_Ogrenci_Ref = ogr.ID;
                        db.AtananBolumlers.Add(blm);
                        db.SaveChanges();
                    }

                    //bool sinavEkle = SinavKontrol(prs_Ref);
                    //if (sinavEkle)
                    //{
                    DateTime bugun = DateTime.Now.Date;
                    //TimeSpan baslaSaati = TimeSpan.Parse("16:00:00");
                    //TimeSpan bitisSaati = TimeSpan.Parse("16:20:00");
                    Personel_Sinavlar sinavata = new Personel_Sinavlar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        Sinif_Ogrenci_Ref = ogr.ID,
                        Sinavlar_Ref = SivanNoAta(prs_Ref),
                        SinavAciklama = false,
                        SinavNotuUygulama = 0,
                        SinavNotuTeorik = 0,
                        Durum = "AÇIKLANMADI",
                        DogruSayisiUygulama = 0,
                        YanlisSayisiUgulama = 0,
                        UygulamaBos = 0,
                        DogruSayisiTeorik = 0,
                        YanlisSayisiTeorik = 0,
                        TeorikBos = 0,
                        SinavTarihi = bugun,
                        //BaslamaSaati = baslaSaati,
                        //BitisSaati = bitisSaati,
                        SinavSuresi = 20,
                        Basladi = 0,
                        Bitti = 0
                    };
                    db.Personel_Sinavlar.Add(sinavata);
                    db.SaveChanges();
                    //durum = "Kayıt başarı ile gerçekleşti.";
                    //}
                }

            }
            catch
            {
                //durum = "İşlem sırasında hata oluştu ! kontrol edip bilgileri tekrar deneyiniz.";
            }
            //return durum;
        }

        bool SinavKontrol(int personelRef)
        {
            bool durum = true;
            using (Heas_UzakEntities db = new Heas_UzakEntities())
            {
                DateTime bugun = DateTime.Now.Date;
                int kontrol = db.Personel_Sinavlar.Where(d => d.SinavTarihi == null && d.Sinif_Ogrenci.Personel_Ref == personelRef).Count();
                if (kontrol > 0)
                {
                    durum = false;
                }
            }
            return durum;
        }

        int SivanNoAta(int personelRef)
        {
            int? sinavNo = 0;
            using (Heas_UzakEntities db = new Heas_UzakEntities())
            {
                int[] sinavlar = db.Personel_Sinavlar.Where(w => w.Sinif_Ogrenci.Personel_Ref == personelRef).Select(r => r.Sinavlar_Ref).ToArray();
                sinavNo = (from c in db.Sinavlars where !sinavlar.Contains(c.ID) select c.ID).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
                if (sinavNo == 0 || sinavNo == null)
                {
                    sinavNo = (from c in db.Sinavlars where sinavlar.Contains(c.ID) select c.ID).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
                }
            }
            return sinavNo.Value;
        }
        #endregion
    }
}