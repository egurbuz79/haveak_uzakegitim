﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace RestService.Models
{
    public class PersonelData
    {
        public string AktifPersonelId { get; set; }
        public string PersonelAdi { get; set; }
        public string Soyadi { get; set; }
        public string TCNo { get; set; }
        public string EPosta { get; set; }
        public string CepTelefon { get; set; }
        public string Resim { get; set; }
        public string Dil { get; set; }
        public string Sifre { get; set; }
        public string FirmaAdi { get; set; }
    }

    public class GetIpAddress
    {
        public static string GetClientIp(HttpRequestMessage request)
        {
            string ip = string.Empty;
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {                
                ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(ip))
                {
                    ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
            }
            return ip;
        }
    }
}