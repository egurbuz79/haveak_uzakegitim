﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestService.Models
{
    public class SonucView
    {
        public int sonuc { get; set; }
        public string cevap { get; set; }
        private string clientip;
        public string ClientIP
        {
            get { return clientip; }
            set
            {
                clientip = System.Configuration.ConfigurationManager.AppSettings["clientIP"];
            }
        }
    }
}