﻿using RestService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestService.Controllers
{
    public class EnController : ApiController
    {
        [HttpPost]
        [Route("~/en/PersonelKaydetEn/{data}")]
        public IHttpActionResult PersonelKaydetEn(PersonelData data)
        {
            string durum = "";
            Heas_Uzak_ENEntities dben = new Heas_Uzak_ENEntities();
            try
            {
                Personel detay = dben.Personels.Where(q => q.TCNo == data.TCNo).FirstOrDefault();
                if (detay == null)
                {
                    detay = new Personel();
                    detay.IsActive = true;
                    detay.IsDelete = false;
                    detay.Adi = data.PersonelAdi;
                    detay.Soyadi = data.Soyadi;
                    detay.TCNo = data.TCNo;
                    detay.CepTelefon = data.CepTelefon;
                    detay.Email = data.EPosta;
                    detay.Adresi = "-";
                    detay.Cinsiyet = "-";
                    detay.DogumTarihi = DateTime.Now;
                    detay.Firma_Ref = new English().FirmaKontrolEng(data.FirmaAdi);
                    detay.IsTelefon = "-";
                    detay.EvTelefon = "-";
                    if (data.Resim != null)
                    {
                        detay.ServiceImage = data.Resim;
                    }
                    dben.Personels.Add(detay);
                    dben.SaveChanges();
                    new English().KullaniciKaydetENG(detay.ID, data.TCNo, data.Sifre, data.AktifPersonelId); ;
                }
                else
                {
                    detay.CepTelefon = data.CepTelefon;
                    detay.Email = data.EPosta;
                    if (data.Resim != null)
                    {
                        detay.ServiceImage = data.Resim;
                        //detay.Resim = savedocument(data.PersonelAdi + data.Soyadi, data.Resim);
                    }
                    dben.SaveChanges();
                    new English().KullaniciKaydetENG(detay.ID, data.TCNo, data.Sifre, data.AktifPersonelId);
                }

            }
            catch (Exception ex)
            {
                durum = ex.Message;
            }

            return Json(durum);
        }

        [HttpPost]
        [Route("~/en/SinavIptalEn/{aktifPersonelId}")]
        public IHttpActionResult SinavIptalEn(int aktifPersonelId)
        {
            string sonuc = string.Empty;
            using (Heas_Uzak_ENEntities db = new Heas_Uzak_ENEntities())
            {
                var personel = db.Personel_Sinavlar.Where(e => e.Sinif_Ogrenci.AktifPersonelId == aktifPersonelId).FirstOrDefault();
                if (personel.Durum == "GEÇTİ")
                {
                    personel.Durum = "KALDI";
                    personel.SertifikaNo = "TEKRAR SINAVA GİRECEK";
                    personel.SinavNotuTeorik = 0;
                    db.SaveChanges();
                    sonuc = "Personel Sınavı iptal edildi";
                }
            }
            return Json(sonuc);
        }
    }
}
