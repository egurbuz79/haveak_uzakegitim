﻿using RestService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestService.Controllers
{
    public class TestController : ApiController
    {
        [HttpGet]
        public IHttpActionResult TestData()
        {
            string adres = GetIpAddress.GetClientIp(this.Request);
            SonucView sv = new SonucView();
            string adresgelen = string.Empty;
            if (adres != "::1")
            {
                adresgelen = "ok";
                sv.ClientIP = adres;
            }
            else
            {
                adresgelen = "IP bilginiz onunamadı!";
                sv.ClientIP = "-";
            }
            sv.cevap = adresgelen;
            sv.sonuc = 1;


            return Json(sv);
        }
    }
}
