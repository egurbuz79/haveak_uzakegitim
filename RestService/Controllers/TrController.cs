﻿using RestService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestService.Controllers
{
    public class TrController : ApiController
    {


        //[Route("api/tr/PersonelKaydetTr/{data}")]
        [HttpGet]
        public IHttpActionResult PersonelKaydetTr(PersonelData data)
        {
            string adres = GetIpAddress.GetClientIp(this.Request);
            SonucView sonuc = new SonucView();
            if (adres != sonuc.ClientIP)
            {
                Heas_UzakEntities db = new Heas_UzakEntities();
                try
                {
                    Personel detay = db.Personels.Where(q => q.TCNo == data.TCNo).FirstOrDefault();
                    if (detay == null)
                    {
                        detay = new Personel();
                        detay.IsActive = true;
                        detay.IsDelete = false;
                        detay.Adi = data.PersonelAdi;
                        detay.Soyadi = data.Soyadi;
                        detay.TCNo = data.TCNo;
                        detay.CepTelefon = data.CepTelefon ?? "";
                        detay.Email = data.EPosta ?? "";
                        detay.Adresi = "-";
                        detay.Cinsiyet = "-";
                        detay.DogumTarihi = DateTime.Now;
                        detay.Firma_Ref = new Turkish().FirmaKontrol(data.FirmaAdi);
                        detay.IsTelefon = "-";
                        detay.EvTelefon = "-";
                        if (data.Resim != null)
                        {
                            detay.ServiceImage = data.Resim;
                        }
                        db.Personels.Add(detay);
                        db.SaveChanges();
                        new Turkish().KullaniciKaydet(detay.ID, data.TCNo, data.Sifre, data.AktifPersonelId);
                    }
                    else
                    {
                        detay.CepTelefon = data.CepTelefon ?? "";
                        detay.Email = data.EPosta ?? "";
                        if (data.Resim != null)
                        {
                            detay.ServiceImage = data.Resim;
                        }
                        db.SaveChanges();
                        new Turkish().KullaniciKaydet(detay.ID, data.TCNo, data.Sifre, data.AktifPersonelId);                       
                    }

                }
                catch (Exception ex)
                {
                    sonuc.sonuc = 0;
                    sonuc.cevap = "İşlem sırasında hata meydane geldi Eğitim Proje birimi ile irtibata geçiniz"; // serviceımage resimler şişmiştir. Temizle.
                                                                                                                 //durum = "Personel ID:" +data.AktifPersonelId + " - Firma Adı:" + data.FirmaAdi + " - GSM:" + data.CepTelefon + " - Dil:" + data.Dil + " - EPosta:" + data.EPosta + " - Adı:" + data.PersonelAdi + " - Soyadı:" + data.Soyadi + " - TCNo" + data.TCNo + " - Sifre:" + data.Sifre + " - Resim:" + data.Resim;
                }
            }
            else
            {
                sonuc.sonuc = 0;
                sonuc.cevap = "Uyuşmayan IP adresi!!";
            }
            return Json(sonuc);
        }


        //[Route("tr/SinavIptalTr/{aktifPersonelId}")]
        [HttpGet]
        public IHttpActionResult SinavIptalTr(int aktifPersonelId)
        {
            string adres = GetIpAddress.GetClientIp(this.Request);
            SonucView sonuc = new SonucView();
            if (adres == sonuc.ClientIP)
            {
                using (Heas_UzakEntities db = new Heas_UzakEntities())
                {
                    try
                    {
                        var personel = db.Personel_Sinavlar.Where(e => e.Sinif_Ogrenci.AktifPersonelId == aktifPersonelId).FirstOrDefault();
                        if (personel != null && personel.Durum == "GEÇTİ")
                        {
                            personel.Durum = "KALDI";
                            personel.SertifikaNo = "TEKRAR SINAVA GİRECEK";
                            personel.SinavNotuTeorik = 0;
                            sonuc.sonuc = db.SaveChanges();
                            sonuc.cevap = "Personel Sınavı iptal edildi";
                        }
                    }
                    catch (Exception ex)
                    {
                        sonuc.sonuc = 0;
                        sonuc.cevap = ex.Message;
                    }
                }
            }
            else
            {
                sonuc.sonuc = 0;
                sonuc.cevap = "Uyuşmayan IP adresi!!";
            }
            return Json(sonuc);
        }        
    }
}
