//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebServiceEgitim
{
    using System;
    using System.Collections.Generic;
    
    public partial class Personel
    {
        public Personel()
        {
            this.Kullanicis = new HashSet<Kullanici>();
            this.Mesajlars = new HashSet<Mesajlar>();
            this.Sinif_Ogrenci = new HashSet<Sinif_Ogrenci>();
        }
    
        public int ID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
        public string TCNo { get; set; }
        public string IsTelefon { get; set; }
        public string EvTelefon { get; set; }
        public string CepTelefon { get; set; }
        public string Email { get; set; }
        public string Adresi { get; set; }
        public string Cinsiyet { get; set; }
        public System.DateTime DogumTarihi { get; set; }
        public string Resim { get; set; }
        public Nullable<int> Firma_Ref { get; set; }
        public string ServiceImage { get; set; }
    
        public virtual Firmalar Firmalar { get; set; }
        public virtual ICollection<Kullanici> Kullanicis { get; set; }
        public virtual ICollection<Mesajlar> Mesajlars { get; set; }
        public virtual ICollection<Sinif_Ogrenci> Sinif_Ogrenci { get; set; }
    }
}
