﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebServiceEgitim
{

    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class wservis : System.Web.Services.WebService
    {
        [WebMethod]
        public string PersonelKaydetTr(PersonelData data)
        {
            string durum = "";
            DateTime bugun = DateTime.Now.Date;
            Heas_UzakEntities db = new Heas_UzakEntities();
            try
            {
                if (string.IsNullOrEmpty(data.KurumId) || string.IsNullOrEmpty(data.AktifPersonelId) || string.IsNullOrEmpty(data.PersonelAdi) || string.IsNullOrEmpty(data.Soyadi) || string.IsNullOrEmpty(data.EPosta) || string.IsNullOrEmpty(data.TCNo))
                {
                    durum = "Personel ID:" + data.AktifPersonelId + " - Firma ID:" + data.KurumId + " - EPosta:" + data.EPosta + " - Adı:" + data.PersonelAdi + " - Soyadı:" + data.Soyadi + " - TCNo" + data.TCNo + " <br /> kişisine ait Eksik bilgi mevcuttur";
                }
                else
                {
                    bool sinavatamasonuc = false;
                    string sinaveklebilecektarih = string.Empty;
                    IQueryable<Personel_Sinavlar> sinavKontrol = db.Personel_Sinavlar.OrderByDescending(t => t.ID).Where(r => r.Sinif_Ogrenci.Personel.TCNo.Trim() == data.TCNo.Trim() && r.SinavTarihi.Value.Year >= bugun.Year && r.IsActive && !r.IsDelete);
                    if (sinavKontrol.Count() < 21)
                    {
                        if (sinavKontrol.Take(5).Count() > 5)
                        {
                            int gectikontrol = sinavKontrol.Where(r => r.Durum == "GEÇTİ").Count();
                            if (gectikontrol == 0)
                            {
                                Personel_Sinavlar sonsinav = sinavKontrol.OrderByDescending(q => q.ID).FirstOrDefault();
                                if (sonsinav.SinavTarihi.Value.AddDays(7) < bugun.Date)
                                {
                                    sinavatamasonuc = true;
                                }
                                else
                                {
                                    sinaveklebilecektarih = sonsinav.SinavTarihi.Value.AddDays(7).ToShortDateString();
                                }
                            }
                            else
                            {
                                sinavatamasonuc = true;
                            }
                        }
                        else
                        {
                            sinavatamasonuc = true;
                        }

                        if (sinavatamasonuc)
                        {
                            Personel detay = db.Personels.Where(q => q.TCNo.Trim() == data.TCNo.Trim()).FirstOrDefault();
                            if (detay == null)
                            {
                                detay = new Personel();
                                detay.IsActive = true;
                                detay.IsDelete = false;
                                detay.Adi = data.PersonelAdi;
                                detay.Soyadi = data.Soyadi;
                                detay.TCNo = data.TCNo;
                                detay.Adresi = "-";
                                detay.Cinsiyet = "-";
                                detay.DogumTarihi = DateTime.Now;
                                detay.IsTelefon = "-";
                                detay.EvTelefon = "-";
                                db.Personels.Add(detay);
                            }

                            if (data.Resim != null)
                            {
                                detay.ServiceImage = data.Resim;
                            }
                            detay.CepTelefon = data.CepTelefon ?? "";
                            detay.Email = data.EPosta ?? "";
                            detay.Firma_Ref = FirmaKontrol(data.KurumId, data.FirmaAdi, data.FirmaEposta);
                            db.SaveChanges();
                            KullaniciKaydet(detay.ID, data.TCNo, data.Sifre, data.AktifPersonelId);
                        }
                        else
                        {
                            durum = $"Sınav süresi 7 Gün kuralına takıldı. Tazeleme Eğtimi {sinaveklebilecektarih} tarihinden önce atanamaz!";
                            List<string> adres = new List<string>() { "bilgi@heasegitim.com", data.FirmaEposta };
                            var body = "Personel " + data.PersonelAdi + " " + data.Soyadi + "(" + data.TCNo + ") " + data.FirmaAdi + " Firması çalışanı " + durum;
                            MailSender.SendMail(adres, "Heaş Servis Hata!", body);
                        }
                    }
                    else
                    {
                        durum = "(1) yıl içerisinde 20 kere sınava girmiştir. Yeni sınav atanamaz!";
                        List<string> adres = new List<string>() { "bilgi@heasegitim.com" };
                        var body = "Personel " + data.PersonelAdi + " " + data.Soyadi + "(" + data.TCNo + ") " + data.FirmaAdi + " Firması çalışanı " + durum;
                        MailSender.SendMail(adres, "Heaş Servis Hata!", body);
                    }
                }
            }
            catch (Exception ex)
            {
                List<string> adres = new List<string>() { "erhangurbuz79@gmail.com", "bilgi@heasegitim.com" };
                var body = "Personel ID:" + data.AktifPersonelId + " - Firma Adı:" + data.FirmaAdi + " - GSM:" + data.CepTelefon + " - Dil:" + data.Dil + " - EPosta:" + data.EPosta + " - Adı:" + data.PersonelAdi + " - Soyadı:" + data.Soyadi + " - TCNo" + data.TCNo + " - Sifre:" + data.Sifre + " - Resim:" + data.Resim + " <br /> kişisine ait bilgi hata dolayısı ile eklenemedi! <br/>" + ex.Message;
                MailSender.SendMail(adres, "Heaş Servis Hata!", body);
            }

            return durum;
        }

        [WebMethod]
        public string SinavIptalTr(int aktifPersonelId)
        {
            string sonuc = string.Empty;
            using (Heas_UzakEntities db = new Heas_UzakEntities())
            {
                var personel = db.Personel_Sinavlar.Where(e => e.Sinif_Ogrenci.AktifPersonelId == aktifPersonelId).FirstOrDefault();
                if (personel.Durum == "GEÇTİ")
                {
                    personel.Durum = "KALDI";
                    personel.SertifikaNo = "TEKRAR SINAVA GİRECEK";
                    personel.SinavNotuTeorik = 0;
                    db.SaveChanges();
                    sonuc = "Personel Sınavı iptal edildi";
                }
            }
            return sonuc;
        }


        [WebMethod]
        public string PersonelKaydetEn(PersonelData data)
        {
            string durum = "";
            DateTime bugun = DateTime.Now.Date;
            Heas_Uzak_ENEntities dben = new Heas_Uzak_ENEntities();
            try
            {
                if (string.IsNullOrEmpty(data.KurumId) || string.IsNullOrEmpty(data.AktifPersonelId) || string.IsNullOrEmpty(data.PersonelAdi) || string.IsNullOrEmpty(data.Soyadi) || string.IsNullOrEmpty(data.EPosta) || string.IsNullOrEmpty(data.TCNo))
                {
                    durum = "Personel ID:" + data.AktifPersonelId + " - Firma ID:" + data.KurumId + " - EPosta:" + data.EPosta + " - Adı:" + data.PersonelAdi + " - Soyadı:" + data.Soyadi + " - TCNo" + data.TCNo + " <br /> kişisine ait Eksik bilgi mevcuttur";
                }
                else
                {
                    bool sinavatamasonuc = false;
                    string sinaveklebilecektarih = string.Empty;
                    IQueryable<Personel_Sinavlar> sinavKontrol = dben.Personel_Sinavlar.OrderByDescending(t => t.ID).Where(r => r.Sinif_Ogrenci.Personel.TCNo.Trim() == data.TCNo.Trim() && r.SinavTarihi.Value.Year >= bugun.Year && r.IsActive && !r.IsDelete);
                    if (sinavKontrol.Count() < 21)
                    {
                        if (sinavKontrol.Take(5).Count() > 5)
                        {
                            int gectikontrol = sinavKontrol.Where(r => r.Durum == "GEÇTİ").Count();
                            if (gectikontrol == 0)
                            {
                                Personel_Sinavlar sonsinav = sinavKontrol.OrderByDescending(q => q.ID).FirstOrDefault();
                                if (sonsinav.SinavTarihi.Value.AddDays(7) < bugun.Date)
                                {
                                    sinavatamasonuc = true;
                                }
                                else
                                {
                                    sinaveklebilecektarih = sonsinav.SinavTarihi.Value.AddDays(7).ToShortDateString();
                                }
                            }
                            else
                            {
                                sinavatamasonuc = true;
                            }
                        }
                        else
                        {
                            sinavatamasonuc = true;
                        }

                        if (sinavatamasonuc)
                        {


                            Personel detay = dben.Personels.Where(q => q.TCNo == data.TCNo).FirstOrDefault();
                            if (detay == null)
                            {
                                detay = new Personel();
                                detay.IsActive = true;
                                detay.IsDelete = false;
                                detay.Adi = data.PersonelAdi;
                                detay.Soyadi = data.Soyadi;
                                detay.TCNo = data.TCNo;
                                detay.CepTelefon = data.CepTelefon;
                                detay.Email = data.EPosta;
                                detay.Adresi = "-";
                                detay.Cinsiyet = "-";
                                detay.DogumTarihi = DateTime.Now;
                                detay.Firma_Ref = 1; //data.FirmaId == null ? 1 : Convert.ToInt32(data.FirmaId);
                                detay.IsTelefon = "-";
                                detay.EvTelefon = "-";
                                if (data.Resim != null)
                                {
                                    detay.ServiceImage = data.Resim;
                                }

                                dben.Personels.Add(detay);
                                dben.SaveChanges();
                                KullaniciKaydetENG(detay.ID, data.TCNo, data.Sifre, data.AktifPersonelId); ;
                            }
                            else
                            {
                                detay.CepTelefon = data.CepTelefon;
                                detay.Email = data.EPosta;
                                if (data.Resim != null)
                                {
                                    detay.ServiceImage = data.Resim;
                                    //detay.Resim = savedocument(data.PersonelAdi + data.Soyadi, data.Resim);
                                }
                                dben.SaveChanges();
                                KullaniciKaydetENG(detay.ID, data.TCNo, data.Sifre, data.AktifPersonelId);
                            }
                        }
                        else
                        {
                            durum = $"Sınav süresi 7 Gün kuralına takıldı. Tazeleme Eğtimi {sinaveklebilecektarih} tarihinden önce atanamaz!";
                            List<string> adres = new List<string>() { "bilgi@heasegitim.com", data.FirmaEposta };
                            var body = "Personel " + data.PersonelAdi + " " + data.Soyadi + "(" + data.TCNo + ") " + data.FirmaAdi + " Firması çalışanı " + durum;
                            MailSender.SendMail(adres, "Heaş Servis Hata!", body);
                        }
                    }
                    else
                    {
                        durum = "(1) yıl içerisinde 20 kere sınava girmiştir. Yeni sınav atanamaz!";
                        List<string> adres = new List<string>() { "bilgi@heasegitim.com" };
                        var body = "Personel " + data.PersonelAdi + " " + data.Soyadi + "(" + data.TCNo + ") " + data.FirmaAdi + " Firması çalışanı " + durum;
                        MailSender.SendMail(adres, "Heaş Servis Hata!", body);
                    }
                }
            }
            catch (Exception ex)
            {
                durum = ex.Message;
            }

            return durum;
        }

        [WebMethod]
        public string SinavIptalEn(int aktifPersonelId)
        {
            string sonuc = string.Empty;
            using (Heas_Uzak_ENEntities db = new Heas_Uzak_ENEntities())
            {
                var personel = db.Personel_Sinavlar.Where(e => e.Sinif_Ogrenci.AktifPersonelId == aktifPersonelId).FirstOrDefault();
                if (personel.Durum == "GEÇTİ")
                {
                    personel.Durum = "KALDI";
                    personel.SertifikaNo = "TEKRAR SINAVA GİRECEK";
                    personel.SinavNotuTeorik = 0;
                    db.SaveChanges();
                    sonuc = "Personel Sınavı iptal edildi";
                }
            }
            return sonuc;
        }

        #region Türkçe
        public int FirmaKontrol(string kurumId, string firmaAdi, string firmaEposta)
        {
            using (Heas_UzakEntities db = new Heas_UzakEntities())
            {
                int firmaRef = 1;
                var konrol = db.Firmalars.Where(e => e.FirmaAdi == firmaAdi || e.Detay == kurumId).FirstOrDefault();
                if (konrol != null)
                {
                    firmaRef = konrol.ID;
                    konrol.Email = firmaEposta;
                    konrol.FirmaAdi = firmaAdi;
                    db.SaveChanges();
                }
                else
                {
                    Firmalar frm = new Firmalar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        FirmaAdi = firmaAdi,
                        Email = firmaEposta,
                        Telefon = "",
                        Detay = kurumId
                    };
                    db.Firmalars.Add(frm);
                    db.SaveChanges();
                    firmaRef = frm.ID;
                }
                return firmaRef;
            }
        }


        public void SinavSonucOnayTr(string tcno, int aktifPersonelId, string sonuc)
        {
            Heas_UzakEntities db = new Heas_UzakEntities();
            var personel = db.Personel_Sinavlar.Where(e => e.Sinif_Ogrenci.AktifPersonelId == aktifPersonelId && e.Sinif_Ogrenci.Personel.TCNo == tcno).FirstOrDefault();
            if (sonuc != "GEÇTİ")
            {
                personel.SinavNotuTeorik = 0;
                personel.Durum = "KALDI";
            }
            db.SaveChanges();
        }


        public void SinavSonucOnayEn(string tcno, int aktifPersonelId, string sonuc)
        {
            Heas_Uzak_ENEntities db = new Heas_Uzak_ENEntities();
            var personel = db.Personel_Sinavlar.Where(e => e.Sinif_Ogrenci.AktifPersonelId == aktifPersonelId && e.Sinif_Ogrenci.Personel.TCNo == tcno).FirstOrDefault();
            if (sonuc != "success")
            {
                personel.SinavNotuTeorik = 0;
                personel.Durum = "unseccess";
            }
            db.SaveChanges();
        }

        protected void KullaniciKaydet(int prs_Ref, string tcno, string sifre, string aktifPersonelId)
        {
            //string durum = "";            
            Heas_UzakEntities db = new Heas_UzakEntities();
            try
            {
                Kullanici kontrol = db.Kullanicis.Where(t => t.Personel.TCNo == tcno.Trim()).FirstOrDefault();
                //if (kontrol != null)
                //{
                //    int ay, yil;
                //    ay = kontrol.BaslangicTarihi.Month;
                //    yil = kontrol.BaslangicTarihi.Year;
                //    yil = yil + 3;
                //    var gectiKaldi = db.Personel_Sinavlar.OrderByDescending(y => y.ID).Where(w => w.Sinif_Ogrenci.Personel_Ref == kontrol.Personel_Ref).Select(r => r.SinavNotuTeorik).FirstOrDefault();
                //    if ((ay == DateTime.Now.Month && yil < DateTime.Now.Year) || gectiKaldi < 70)
                //    { }
                //    else
                //    {
                //        return;
                //    }
                //}


                if (kontrol != null)
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    kontrol.BaslangicTarihi = DateTime.Now;
                    kontrol.BitisTarihi = DateTime.Now.AddDays(2);
                    kontrol.KullaniciAdi = tcno;
                    var sfr = sifre;
                    if (string.IsNullOrEmpty(sifre))
                    {
                        sfr = Guid.NewGuid().ToString().Split('-')[0];
                    }
                    kontrol.Sifre = sfr;
                    db.SaveChanges();
                }
                else
                {
                    kontrol = new Kullanici();
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    kontrol.KullaniciAdi = tcno;
                    var sfr = sifre;
                    if (string.IsNullOrEmpty(sifre))
                    {
                        sfr = Guid.NewGuid().ToString().Split('-')[0];
                    }
                    //string sfr = Guid.NewGuid().ToString().Split('-')[0];                  
                    kontrol.Sifre = sfr;
                    kontrol.BaslangicTarihi = DateTime.Now;
                    kontrol.BitisTarihi = DateTime.Now.AddDays(2);
                    kontrol.Personel_Ref = prs_Ref;
                    kontrol.Sistemde = false;
                    kontrol.Songiristarihi = DateTime.Now.Date;
                    db.Kullanicis.Add(kontrol);
                    db.SaveChanges();

                    for (int i = 2; i < 4; i++)
                    {
                        Kull_Rolleri rolKontrol = new Kull_Rolleri()
                        {
                            IsActive = true,
                            IsDelete = false,
                            User_Role_Name_Ref = i,
                            User_Ref = kontrol.ID
                        };
                        db.Kull_Rolleri.Add(rolKontrol);
                    }
                    db.SaveChanges();
                }

                int apId = Convert.ToInt32(aktifPersonelId);
                Sinif_Ogrenci ogr = db.Sinif_Ogrenci.Where(e => e.AktifPersonelId == apId).FirstOrDefault();
                if (ogr == null)
                {
                    Sinif snf = db.Sinifs.OrderByDescending(e => e.ID).FirstOrDefault();
                    int lastID = snf.ID + 1;
                    int sinifOgrenciSayisi = db.Sinif_Ogrenci.Where(w => w.Sinif_Ref == snf.ID).Count();
                    if (sinifOgrenciSayisi > 50)
                    {
                        snf = new Sinif();
                        snf.IsActive = true;
                        snf.IsDelete = false;
                        snf.Firma_Ref = 1; //Uzaktan Eğitim Merkezi
                        snf.SinifAdi = "Uzak_Egitim" + lastID;
                        db.Sinifs.Add(snf);
                        db.SaveChanges();
                    }

                    ogr = new Sinif_Ogrenci()
                    {
                        Personel_Ref = prs_Ref,
                        Sinif_Ref = snf.ID,
                        Basladi = true,
                        Bitti = false,
                        BaslamaTarihi = DateTime.Now,
                        BitisTarihi = DateTime.Now.AddDays(1),
                        AktifPersonelId = apId
                    };
                    db.Sinif_Ogrenci.Add(ogr);
                    db.SaveChanges();

                    AtananBolumler blm = new AtananBolumler();
                    blm.Bolum_Ref = 1;
                    blm.Sinif_Ogrenci_Ref = ogr.ID;
                    db.AtananBolumlers.Add(blm);
                    db.SaveChanges();

                    //bool sinavEkle = SinavKontrol(prs_Ref);
                    //if (sinavEkle)
                    //{
                    DateTime bugun = DateTime.Now.Date;
                    //TimeSpan baslaSaati = TimeSpan.Parse("16:00:00");
                    //TimeSpan bitisSaati = TimeSpan.Parse("16:20:00");
                    Personel_Sinavlar sinavata = new Personel_Sinavlar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        Sinif_Ogrenci_Ref = ogr.ID,
                        Sinavlar_Ref = SivanNoAta(prs_Ref),
                        SinavAciklama = false,
                        SinavNotuUygulama = 0,
                        SinavNotuTeorik = 0,
                        Durum = "AÇIKLANMADI",
                        DogruSayisiUygulama = 0,
                        YanlisSayisiUgulama = 0,
                        UygulamaBos = 0,
                        DogruSayisiTeorik = 0,
                        YanlisSayisiTeorik = 0,
                        TeorikBos = 0,
                        SinavTarihi = bugun,
                        //BaslamaSaati = baslaSaati,
                        //BitisSaati = bitisSaati,
                        SinavSuresi = 20,
                        Basladi = 0,
                        Bitti = 0
                    };
                    db.Personel_Sinavlar.Add(sinavata);
                    db.SaveChanges();
                    //durum = "Kayıt başarı ile gerçekleşti.";
                    //}
                    List<string> adres = new List<string>();
                    adres.Add(kontrol.Personel.Firmalar.Email);
                    string firma = kontrol.Personel.Firmalar.FirmaAdi;
                    string kisi = kontrol.Personel.Adi + " " + kontrol.Personel.Soyadi;
                    string body = "Sayın <I>" + firma.ToUpper() + " Yetkilisi; </I> <br />";
                    string bildirim = kontrol.Personel.Adi + " " + kontrol.Personel.Soyadi + "'Heas Uzaktan Eğitim Kurs1' eğitim ataması yapılmıştır.";
                    body += kisi + " isimli personelinizin" + DateTime.Now.ToLongDateString() + "  tarihi itibarı ile Kurs1 Eğitim sistemine kayıt işlemi başarıyla gerçekleştirilmiştir. <br />";
                    body += "Bu mesaj bilgilendirme amacıyla tarfınıza gönderilmiştir.";

                    MailSender.SendMail(adres, bildirim, body);
                }
                else
                {
                    Sinif snf = db.Sinifs.OrderByDescending(e => e.ID).FirstOrDefault();
                    int sinifOgrenciSayisi = db.Sinif_Ogrenci.Where(w => w.Sinif_Ref == snf.ID).Count();
                    bool sinifKontrol = true;
                    if (sinifOgrenciSayisi > 50)
                    {
                        snf = new Sinif();
                        snf.IsActive = true;
                        snf.IsDelete = false;
                        snf.Firma_Ref = 1; //Uzaktan Eğitim Merkezi
                        snf.SinifAdi = "Uzak_Egitim" + (snf.ID + 1);
                        db.Sinifs.Add(snf);
                        db.SaveChanges();
                        sinifKontrol = false;
                    }
                    //Sinif_Ogrenci ogr = db.Sinif_Ogrenci.Where(q => q.AktifPersonelId == apId).FirstOrDefault();
                    ogr.Basladi = true;
                    ogr.Bitti = false;
                    ogr.BaslamaTarihi = DateTime.Now;
                    ogr.BitisTarihi = DateTime.Now.AddDays(2);
                    if (!sinifKontrol)
                    {
                        ogr.Personel_Ref = kontrol.Personel_Ref;
                        ogr.AktifPersonelId = apId;
                        ogr.Sinif_Ref = snf.ID;
                        db.Sinif_Ogrenci.Add(ogr);
                    }
                    db.SaveChanges();

                    if (!sinifKontrol)
                    {
                        var atamaKontrol = db.AtananBolumlers.Where(w => w.Sinif_Ogrenci_Ref == ogr.ID).FirstOrDefault();
                        AtananBolumler blm = new AtananBolumler();
                        blm.Bolum_Ref = 1;
                        blm.Sinif_Ogrenci_Ref = ogr.ID;
                        db.AtananBolumlers.Add(blm);
                        db.SaveChanges();
                    }

                    //bool sinavEkle = SinavKontrol(prs_Ref);
                    //if (sinavEkle)
                    //{
                    DateTime bugun = DateTime.Now.Date;
                    //TimeSpan baslaSaati = TimeSpan.Parse("16:00:00");
                    //TimeSpan bitisSaati = TimeSpan.Parse("16:20:00");
                    Personel_Sinavlar sinavata = new Personel_Sinavlar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        Sinif_Ogrenci_Ref = ogr.ID,
                        Sinavlar_Ref = SivanNoAta(prs_Ref),
                        SinavAciklama = false,
                        SinavNotuUygulama = 0,
                        SinavNotuTeorik = 0,
                        Durum = "AÇIKLANMADI",
                        DogruSayisiUygulama = 0,
                        YanlisSayisiUgulama = 0,
                        UygulamaBos = 0,
                        DogruSayisiTeorik = 0,
                        YanlisSayisiTeorik = 0,
                        TeorikBos = 0,
                        SinavTarihi = bugun,
                        //BaslamaSaati = baslaSaati,
                        //BitisSaati = bitisSaati,
                        SinavSuresi = 20,
                        Basladi = 0,
                        Bitti = 0
                    };
                    db.Personel_Sinavlar.Add(sinavata);
                    db.SaveChanges();
                    //durum = "Kayıt başarı ile gerçekleşti.";
                    //}
                    List<string> adres = new List<string>();
                    adres.Add(kontrol.Personel.Firmalar.Email);
                    string firma = kontrol.Personel.Firmalar.FirmaAdi;
                    string kisi = kontrol.Personel.Adi + " " + kontrol.Personel.Soyadi;
                    string body = "Sayın <I>" + firma.ToUpper() + " Yetkilisi; </I> <br />";
                    string bildirim = kontrol.Personel.Adi + " " + kontrol.Personel.Soyadi + "'Heas Uzaktan Eğitim Kurs1' eğitim ataması yapılmıştır.";
                    body += kisi + " isimli personelinizin" + DateTime.Now.ToLongDateString() + "  tarihi itibarı ile Kurs1 Eğitim sistemine kayıt işlemi başarıyla gerçekleştirilmiştir. <br />";
                    body += "Bu mesaj bilgilendirme amacıyla tarfınıza gönderilmiştir.";

                    MailSender.SendMail(adres, bildirim, body);
                }
            }
            catch
            {
                //durum = "İşlem sırasında hata oluştu ! kontrol edip bilgileri tekrar deneyiniz.";
            }
            //return durum;
        }

        //Tuple<bool,DateTime> SinavKontrol(int personelRef)
        //{
        //    Tuple<bool, DateTime> result = null;
        //    using (Heas_UzakEntities db = new Heas_UzakEntities())
        //    {            
        //        int engelle = 0;
        //        var kontrol = db.Personel_Sinavlar.OrderByDescending(e=>e.ID).Where(d => d.Sinif_Ogrenci.Personel_Ref == personelRef).Take(5).ToList();
        //        foreach (var item in kontrol)
        //        {                   
        //            if (item.Basladi== 0 && item.Bitti == 0)
        //            {
        //                engelle++;
        //            }
        //            else
        //            {
        //                engelle = 0; 
        //            }                    
        //        }
        //        if (engelle==5)
        //        {
        //            result = new Tuple<bool, DateTime>(true, kontrol[4].SinavTarihi.Value);                     
        //        }                
        //    }
        //    return result;
        //}

        int SivanNoAta(int personelRef)
        {
            int? sinavNo = 0;
            using (Heas_UzakEntities db = new Heas_UzakEntities())
            {
                int[] sinavlar = db.Personel_Sinavlar.Where(w => w.Sinif_Ogrenci.Personel_Ref == personelRef).Select(r => r.Sinavlar_Ref).ToArray();
                sinavNo = (from c in db.Sinavlars where !sinavlar.Contains(c.ID) select c.ID).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
                if (sinavNo == 0 || sinavNo == null)
                {
                    sinavNo = (from c in db.Sinavlars where sinavlar.Contains(c.ID) select c.ID).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
                }
            }
            return sinavNo.Value;
        }
        #endregion

        #region Ingilizce
        public int FirmaKontrolEng(string firmaAdi)
        {
            using (Heas_Uzak_ENEntities db = new Heas_Uzak_ENEntities())
            {
                int firmaRef = 1;
                var konrol = db.Firmalars.Where(e => e.FirmaAdi == firmaAdi).FirstOrDefault();
                if (konrol != null)
                {
                    firmaRef = konrol.ID;
                }
                else
                {
                    Firmalar frm = new Firmalar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        FirmaAdi = firmaAdi,
                        Email = "",
                        Telefon = "",
                        Detay = ""
                    };
                    db.Firmalars.Add(frm);
                    db.SaveChanges();
                    firmaRef = frm.ID;
                }
                return firmaRef;
            }
        }

        private string savedocumentENG(string fileName, string fileStream)
        {
            string adres = "";
            if (fileStream != null)
            {
                string Fname = Guid.NewGuid().ToString().Split('-')[0] + fileName;
                adres = "http://heasuzaktanegitim.xyz/ENG/DocumentFiles/Personel/" + Fname;

                byte[] imageBytes = Convert.FromBase64String(fileStream);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                image.Save(adres);
                //FileStream fileToupload = new FileStream(adres, FileMode.Create);

                //byte[] bytearray = new byte[10000];
                //int bytesRead, totalBytesRead = 0;
                //do
                //{
                //    bytesRead = fileStream.Read(bytearray, 0, bytearray.Length);
                //    totalBytesRead += bytesRead;
                //} while (bytesRead > 0);

                //fileToupload.Write(bytearray, 0, bytearray.Length);
                //fileToupload.Close();
                //fileToupload.Dispose();
            }
            return adres;
        }

        protected void KullaniciKaydetENG(int prs_Ref, string tcno, string sifre, string aktifPersonelId)
        {
            //string durum = "";

            Heas_Uzak_ENEntities dben = new Heas_Uzak_ENEntities();
            try
            {
                Kullanici kontrol = dben.Kullanicis.Where(t => t.Personel.TCNo == tcno).FirstOrDefault();


                if (kontrol != null)
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    kontrol.BaslangicTarihi = DateTime.Now;
                    kontrol.BitisTarihi = DateTime.Now.AddDays(1);
                    kontrol.KullaniciAdi = tcno;
                    var sfr = sifre;
                    if (string.IsNullOrEmpty(sifre))
                    {
                        sfr = Guid.NewGuid().ToString().Split('-')[0];
                    }
                    kontrol.Sifre = sfr;
                    dben.SaveChanges();
                }
                else
                {
                    kontrol = new Kullanici();
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    kontrol.KullaniciAdi = tcno;
                    var sfr = sifre;
                    if (string.IsNullOrEmpty(sifre))
                    {
                        sfr = Guid.NewGuid().ToString().Split('-')[0];
                    }
                    //string sfr = Guid.NewGuid().ToString().Split('-')[0];                  
                    kontrol.Sifre = sfr;
                    kontrol.BaslangicTarihi = DateTime.Now;
                    kontrol.BitisTarihi = DateTime.Now.AddDays(1);
                    kontrol.Personel_Ref = prs_Ref;
                    kontrol.Sistemde = false;
                    kontrol.Songiristarihi = DateTime.Now.Date;
                    dben.Kullanicis.Add(kontrol);
                    dben.SaveChanges();

                    for (int i = 2; i < 4; i++)
                    {
                        Kull_Rolleri rolKontrol = new Kull_Rolleri()
                        {
                            IsActive = true,
                            IsDelete = false,
                            User_Role_Name_Ref = i,
                            User_Ref = kontrol.ID
                        };
                        dben.Kull_Rolleri.Add(rolKontrol);
                    }
                    dben.SaveChanges();
                }

                int apIdEng = Convert.ToInt32(aktifPersonelId);
                Sinif_Ogrenci kayitKontrol = dben.Sinif_Ogrenci.Where(e => e.AktifPersonelId == apIdEng).FirstOrDefault();
                if (kayitKontrol == null)
                {
                    Sinif snf = dben.Sinifs.OrderByDescending(e => e.ID).FirstOrDefault();
                    int sinifOgrenciSayisi = dben.Sinif_Ogrenci.Where(w => w.Sinif_Ref == snf.ID).Count();
                    if (sinifOgrenciSayisi > 50)
                    {
                        snf = new Sinif();
                        snf.IsActive = true;
                        snf.IsDelete = false;
                        snf.Firma_Ref = 1;
                        snf.SinifAdi = "Uzak_Egitim" + (snf.ID + 1);
                        dben.Sinifs.Add(snf);
                        dben.SaveChanges();
                    }

                    Sinif_Ogrenci ogr = new Sinif_Ogrenci()
                    {
                        Personel_Ref = prs_Ref,
                        Sinif_Ref = snf.ID,
                        Basladi = true,
                        Bitti = false,
                        BaslamaTarihi = DateTime.Now,
                        BitisTarihi = DateTime.Now.AddDays(1),
                        AktifPersonelId = apIdEng
                    };
                    dben.Sinif_Ogrenci.Add(ogr);
                    dben.SaveChanges();

                    AtananBolumler blm = new AtananBolumler();
                    blm.Bolum_Ref = 1;
                    blm.Sinif_Ogrenci_Ref = ogr.ID;
                    dben.AtananBolumlers.Add(blm);
                    dben.SaveChanges();

                    //Tuple<bool, DateTime> sinavKontrolu = SinavKontrol(prs_Ref);
                    //bool sinavEkle = SinavKontrol(prs_Ref);
                    //if (sinavKontrolu != null)
                    //{
                    DateTime bugun = DateTime.Now.Date;
                    //TimeSpan baslaSaati = TimeSpan.Parse("16:00:00");
                    //TimeSpan bitisSaati = TimeSpan.Parse("16:20:00");
                    Personel_Sinavlar sinavata = new Personel_Sinavlar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        Sinif_Ogrenci_Ref = ogr.ID,
                        Sinavlar_Ref = SivanNoAta(prs_Ref),
                        SinavAciklama = false,
                        SinavNotuUygulama = 0,
                        SinavNotuTeorik = 0,
                        Durum = "Undisclosed",
                        DogruSayisiUygulama = 0,
                        YanlisSayisiUgulama = 0,
                        UygulamaBos = 0,
                        DogruSayisiTeorik = 0,
                        YanlisSayisiTeorik = 0,
                        TeorikBos = 0,
                        SinavTarihi = bugun,
                        //BaslamaSaati = baslaSaati,
                        //BitisSaati = bitisSaati,
                        SinavSuresi = 20,
                        Basladi = 0,
                        Bitti = 0
                    };
                    dben.Personel_Sinavlar.Add(sinavata);
                    dben.SaveChanges();
                    //}
                    //List<string> adres = new List<string>();
                    //adres.Add(kayitKontrol.Personel.Firmalar.Email);
                    //string firma = kayitKontrol.Personel.Firmalar.FirmaAdi;
                    //string kisi = kayitKontrol.Personel.Adi + " " + kayitKontrol.Personel.Soyadi;
                    //string body = "Sayın <I>" + firma.ToUpper() + " Yetkilisi; </I> <br />";
                    //string bildirim = kayitKontrol.Personel.Adi + " " + kayitKontrol.Personel.Soyadi + " adlı personel 'Heas Uzaktan Eğitim Kurs1' kayıt giriş onayı";
                    //body += kisi + " isimli personelinizin Kurs1 Eğitim sistemine kayıt işemi başarıyla gerçekleştirilmiştirtir. <br />";
                    //body += "Bu mesaj bilgilendirme amacıyla tarfınıza gönderilmiştir.";

                    //MailSender.SendMail(adres, "Personel 'Kurs1' sınav sonuc bildirimi", body);
                    //}
                    //else
                    //{
                    //    List<string> adres = new List<string>();
                    //    adres.Add(kayitKontrol.Personel.Firmalar.Email);
                    //    adres.Add("ozcan.ozturk@heas.com.tr");
                    //    adres.Add("gokmen.test@heas.com.tr");

                    //    string firma = kayitKontrol.Personel.Firmalar.FirmaAdi;
                    //    string kisi = kayitKontrol.Personel.Adi + " " + kayitKontrol.Personel.Soyadi;
                    //    string body = "Sayın <I>" + firma.ToUpper() + " Yetkilisi; </I> <br />";
                    //    string bildirim = kayitKontrol.Personel.Adi + " " + kayitKontrol.Personel.Soyadi + " adlı personel 'Heas Uzaktan Eğitim Kurs1' kayıt giriş onayı";
                    //    body += kisi + " isimli personelinizin Kurs1 Eğitim sistemine kayıt işemi başarıyla gerçekleştirilmiştirtir. <br />";
                    //    body += "Bu mesaj bilgilendirme amacıyla tarfınıza gönderilmiştir.";

                    //    MailSender.SendMail(adres, "Personel 'Kurs1' sınav sonuc bildirimi", body);
                    //}
                }
                else
                {
                    Sinif snf = dben.Sinifs.OrderByDescending(e => e.ID).FirstOrDefault();
                    int sinifOgrenciSayisi = dben.Sinif_Ogrenci.Where(w => w.Sinif_Ref == snf.ID).Count();
                    bool sinifKontrol = true;
                    int snfID = snf.ID + 1;
                    if (sinifOgrenciSayisi > 50)
                    {
                        snf = new Sinif();
                        snf.IsActive = true;
                        snf.IsDelete = false;
                        snf.Firma_Ref = 1; //Uzaktan eğitim;
                        snf.SinifAdi = "Uzak_Egitim" + snfID;
                        dben.Sinifs.Add(snf);
                        dben.SaveChanges();
                        sinifKontrol = false;
                    }

                    Sinif_Ogrenci ogr = dben.Sinif_Ogrenci.Where(q => q.AktifPersonelId == apIdEng).FirstOrDefault();
                    ogr.Basladi = true;
                    ogr.Bitti = false;
                    ogr.BaslamaTarihi = DateTime.Now;
                    ogr.BitisTarihi = DateTime.Now.AddDays(1);
                    if (!sinifKontrol)
                    {
                        ogr.Personel_Ref = kontrol.Personel_Ref;
                        ogr.AktifPersonelId = apIdEng;
                        ogr.Sinif_Ref = snf.ID;
                        dben.Sinif_Ogrenci.Add(ogr);
                    }
                    dben.SaveChanges();

                    if (!sinifKontrol)
                    {
                        var atamaKontrol = dben.AtananBolumlers.Where(w => w.Sinif_Ogrenci_Ref == ogr.ID).FirstOrDefault();
                        AtananBolumler blm = new AtananBolumler();
                        blm.Bolum_Ref = 1;
                        blm.Sinif_Ogrenci_Ref = ogr.ID;
                        dben.AtananBolumlers.Add(blm);
                        dben.SaveChanges();
                    }


                    //bool sinavEkle = SinavKontrol(prs_Ref);
                    //if (sinavEkle)
                    //{
                    DateTime bugun = DateTime.Now.Date;
                    //TimeSpan baslaSaati = TimeSpan.Parse("16:00:00");
                    //TimeSpan bitisSaati = TimeSpan.Parse("16:20:00");
                    Personel_Sinavlar sinavata = new Personel_Sinavlar()
                    {
                        IsActive = true,
                        IsDelete = false,
                        Sinif_Ogrenci_Ref = ogr.ID,
                        Sinavlar_Ref = SivanNoAtaENG(prs_Ref),
                        SinavAciklama = false,
                        SinavNotuUygulama = 0,
                        SinavNotuTeorik = 0,
                        Durum = "Undisclosed",
                        DogruSayisiUygulama = 0,
                        YanlisSayisiUgulama = 0,
                        UygulamaBos = 0,
                        DogruSayisiTeorik = 0,
                        YanlisSayisiTeorik = 0,
                        TeorikBos = 0,
                        SinavTarihi = bugun,
                        //BaslamaSaati = baslaSaati,
                        //BitisSaati = bitisSaati,
                        SinavSuresi = 20,
                        Basladi = 0,
                        Bitti = 0
                    };
                    dben.Personel_Sinavlar.Add(sinavata);
                    dben.SaveChanges();
                    //durum = "Kayıt başarı ile gerçekleşti.";
                    //} 
                    List<string> adres = new List<string>();
                    adres.Add(ogr.Personel.Firmalar.Email);
                    string firma = ogr.Personel.Firmalar.FirmaAdi;
                    string kisi = ogr.Personel.Adi + " " + ogr.Personel.Soyadi;
                    string body = "Sayın <I>" + firma.ToUpper() + " Yetkilisi; </I> <br />";
                    string bildirim = ogr.Personel.Adi + " " + ogr.Personel.Soyadi + " adlı personel 'Heas Uzaktan Eğitim Kurs1' kayıt giriş onayı";
                    body += kisi + " isimli personelinizin Kurs1 Eğitim sistemine kayıt işemi başarıyla gerçekleştirilmiştirtir. <br />";
                    body += "Bu mesaj bilgilendirme amacıyla tarfınıza gönderilmiştir.";

                    MailSender.SendMail(adres, "Personel 'Kurs1' sınav sonuc bildirimi", body);
                }

            }
            catch
            {
                //durum = "İşlem sırasında hata oluştu ! kontrol edip bilgileri tekrar deneyiniz.";
            }
            //return durum;
        }

        bool SinavKontrolENG(int personelRef)
        {
            bool durum = true;
            using (Heas_Uzak_ENEntities dben = new Heas_Uzak_ENEntities())
            {
                DateTime bugun = DateTime.Now.Date;
                int kontrol = dben.Personel_Sinavlar.Where(d => d.SinavTarihi == bugun && d.Sinif_Ogrenci.Personel_Ref == personelRef).Count();
                if (kontrol > 0)
                {
                    durum = false;
                }
            }
            return durum;
        }

        int SivanNoAtaENG(int personelRef)
        {
            int? sinavNo = 0;
            using (Heas_Uzak_ENEntities dben = new Heas_Uzak_ENEntities())
            {
                int[] sinavlar = dben.Personel_Sinavlar.Where(w => w.Sinif_Ogrenci.Personel_Ref == personelRef).Select(r => r.Sinavlar_Ref).ToArray();
                sinavNo = (from c in dben.Sinavlars where !sinavlar.Contains(c.ID) select c.ID).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
                if (sinavNo == 0 || sinavNo == null)
                {
                    sinavNo = (from c in dben.Sinavlars where sinavlar.Contains(c.ID) select c.ID).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
                }
            }
            return sinavNo.Value;
        }

        #endregion

    }

    public class PersonelData : IDisposable
    {
        public String KurumId { get; set; }
        public String AktifPersonelId { get; set; }
        public String PersonelAdi { get; set; }
        public String Soyadi { get; set; }
        public String TCNo { get; set; }
        public String EPosta { get; set; }
        public String CepTelefon { get; set; }
        public String Resim { get; set; }
        public String Dil { get; set; }
        public String Sifre { get; set; }
        public String FirmaAdi { get; set; }
        public String FirmaEposta { get; set; }
        public void Dispose()
        {
            if (Resim != null)
            {
                Resim = null;
            }
            GC.SuppressFinalize(this);
        }
    }
}
