﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceEgitim
{
    [Serializable]
    public class DataSerialize
    {
        public string referansId { get; set; }
        public string kimlikNo { get; set; }
        public string sertifikaNo { get; set; }
        public string sno { get; set; }
        public int egitimSonucu { get; set; }
        public List<string> resimler { get; set; }
    }
}