﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using WebMatrix.WebData;

namespace Haveak_Pro
{


    public class MvcApplication : System.Web.HttpApplication
    {
        Heas_UzakEntities Entity = new Heas_UzakEntities();
        //List<string> aktifKullanicilar = new List<string>();
        protected void Application_Start()
        {

            Application["OnlineZiyaretci"] = 0;
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

        }
        protected void Application_End(object sender, EventArgs e)
        {
            Application.Remove("OnlineZiyaretci");
            string sifre = HttpContext.Current.Session["sifre"].ToString();
            var userq = Entity.Kullanicis.Where(t => t.KullaniciAdi == HttpContext.Current.User.Identity.Name && t.Sifre == sifre).FirstOrDefault();
            if (userq != null)
            {
                userq.Sistemde = false;
                Entity.SaveChanges();
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Application.Lock();
            Application["OnlineZiyaretci"] = (int)Application["OnlineZiyaretci"] + 1;
            Application.UnLock();
        }

        protected void Session_End(object sender, EventArgs e)
        {
            Application.Lock();
            Application["OnlineZiyaretci"] = (int)Application["OnlineZiyaretci"] - 1;
            //string bilgi = AktifKullanici.Aktif.EName + " " + AktifKullanici.Aktif.ESurName;
            //aktifKullanicilar.Remove(bilgi);
            Application.UnLock();
            try
            {
                string sifre = HttpContext.Current.Session["sifre"].ToString();
                var userx = Entity.Kullanicis.Where(t => t.KullaniciAdi == HttpContext.Current.User.Identity.Name && t.Sifre == sifre).FirstOrDefault();
                if (userx != null)
                {
                    userx.Sistemde = false;
                    Entity.SaveChanges();
                }
            }
            catch
            {
                //Response.Redirect("~/Kontrol/Giris",true);
            }
        }

        protected void Application_AcquireRequestState(Object sender, EventArgs e)
        {
            #region Attach roles to User
            if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var session = HttpContext.Current.Session;
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["Kimlik"] != null)
                {
                    var kimlik = (KimlikIdentity)Session["Kimlik"];
                    HttpContext.Current.User = kimlik.ToPrensip();
                }
                else if (HttpContext.Current.Session != null)
                {
                    using (Heas_UzakEntities Entities = new Heas_UzakEntities())
                    {
                        var sifre = HttpContext.Current.Session["sifre"] == null ? "" : (string)HttpContext.Current.Session["sifre"];
                        var User = Entities.Kullanicis.Where(t => t.KullaniciAdi == HttpContext.Current.User.Identity.Name && t.Sifre == sifre).FirstOrDefault();
                        if (User != null)
                        {
                            try
                            {
                                var ulm = new UserLoginModel(User.ID);
                                //VERİ TABANINDAN KULLANICI ADI:HttpContext.Current.User.Identity.Name OLANIN KİMLİK BİLGİLERİ ÇEKİLECEK
                                KimlikIdentity kimlik = new KimlikIdentity(HttpContext.Current.User.Identity.Name);
                                //Kullanıcı modülleri seçilir                

                                kimlik.Roller = ulm.UserRole.Select(t => t.Kull_Rol_Adi.Name).ToList();
                                kimlik.UserName = ulm.User.KullaniciAdi;
                                kimlik.Id = ulm.User.ID;

                                //kimlik.Picture = ulm.User.Personel.Resim;
                                kimlik.User_Ref = ulm.Employee.ID;
                                kimlik.Picture = new ImageConvert().ImageReader(ulm.Employee.Resim) == "" ? ulm.Employee.Resim == null ? "/DocumentFiles/Personel/RYok.jpg" : ulm.Employee.Resim : new ImageConvert().ImageReader(ulm.Employee.Resim);
                                //kimlik.ServiceImage = ulm.Employee.ServiceImage;
                                kimlik.identity = ulm.Employee.TCNo;
                                kimlik.EName = ulm.Employee.Adi;
                                kimlik.ESurName = ulm.Employee.Soyadi;
                                kimlik.Telephone = ulm.Employee.CepTelefon != null ? ulm.Employee.CepTelefon : "";
                                kimlik.EMail = ulm.Employee.Email != null ? ulm.Employee.Email : "";
                                kimlik.Moduller = ulm.UserModule.Select(r => r.Name).ToList();
                                kimlik.ModullerRef = ulm.UserModule.Select(w => w.ID).ToList();
                                kimlik.FirmaId = ulm.User.Personel.Firma_Ref;
                                if (kimlik.FirmaId != null)
                                {
                                    kimlik.FirmaAdi = ulm.Firma.FirmaAdi;
                                }

                                if (kimlik.EMail == null)
                                {
                                    User.Sistemde = false;
                                    FormsAuthentication.SignOut();
                                    Session.Abandon();
                                    var context = new RequestContext(new HttpContextWrapper(System.Web.HttpContext.Current), new RouteData());
                                    var urlHelper = new UrlHelper(context);
                                    var url = urlHelper.Action("Giris", "Kontrol", new { area = "" });
                                    HttpContext.Current.Response.Redirect(url);
                                }
                                else
                                {
                                    User.Sistemde = true;
                                    User.Songiristarihi = DateTime.Now.Date;
                                    Session["Kimlik"] = kimlik;
                                    HttpContext.Current.User = kimlik.ToPrensip();
                                }
                            }
                            catch
                            {
                                User.Sistemde = false;
                                FormsAuthentication.SignOut();
                                Session.Abandon();
                                Response.Redirect("~/Kontrol/Giris");
                            }
                            Entities.SaveChanges();
                        }
                    }

                }
            }
            #endregion
        }
    }
}