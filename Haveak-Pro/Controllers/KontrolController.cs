﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
//using Haveak_Pro.Models;
using CaptchaMvc.HtmlHelpers;
using System.Drawing;
using System.IO;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using Haveak_Pro.Models;
using Haveak_Pro.Areas.SINAV.Models;
using System.Drawing.Imaging;

namespace Haveak_Pro.Controllers
{
    [System.Web.Mvc.OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class KontrolController : Controller
    {

        //
        // GET: /Account/
        Haveak_Pro.Models.Heas_UzakEntities db = new Haveak_Pro.Models.Heas_UzakEntities();
        public ActionResult Giris()
        {           
            Session.Abandon();
            FormsAuthentication.SignOut();
            return View();
        }

        public JsonResult SifreKontrol(string userName, string password)
        {
            bool durum = false;
            DateTime zaman = DateTime.Now.Date;
            Haveak_Pro.Models.Kullanici accountControl = db.Kullanicis.Where(t => t.IsActive == true && t.KullaniciAdi == userName && t.Sifre == password && t.BitisTarihi >= zaman).FirstOrDefault();
            if (accountControl != null)
            {
                durum = true;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Giris(string userName, string password, string Secure)
        {
            try
            {
                if (userName != null)
                {
                    if (Session["Captcha"] == null)
                    {
                        return null;
                    }
                    int total = Convert.ToInt32(Session["Captcha"]);
                    if (Secure == "")
                    {
                        Secure = "0";
                    }
                    if (int.Parse(Secure) == total)
                    {
                        Haveak_Pro.Models.Kullanici accountControl = db.Kullanicis.Where(t => t.IsActive == true && t.KullaniciAdi == userName && t.Sifre == password).FirstOrDefault();
                        if (accountControl != null)
                        {
                            //string serviceImageControl = accountControl.Personel.ServiceImage;
                            //if (serviceImageControl != null && serviceImageControl != "")
                            //{  
                            //    accountControl.Personel.Resim = new ImageConvert().ImageReader(accountControl.Personel.ServiceImage);                              
                            //}
                            KullaniciLoglama(accountControl.ID);
                            HttpContext.Session.Add("sifre", password);
                            FormsAuthentication.RedirectFromLoginPage(accountControl.KullaniciAdi, true);
                        }
                    }
                    else
                    {
                        Logoff();
                    }
                }
            }
            catch
            {
                Logoff();
            }
            return View();
        }
                

        public ActionResult Logoff()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Giris", "Kontrol");
        }

        public JsonResult SendMail(string adress)
        {
            bool durum = false;
            try
            {
                List<string> adres = new List<string>();
                adres.Add(adress);
                var data = db.Personels.Where(t => t.Email == adress).FirstOrDefault();
                string kisi = data.Adi + " " + data.Soyadi;

                string body = "Sayın <I>" + kisi + "; </I> <br />";
                body += "Proje Kullanıcı adı: <b>[" + data.Kullanicis.Select(t => t.KullaniciAdi).FirstOrDefault() + "] Şifre: [" + data.Kullanicis.Select(t => t.Sifre).FirstOrDefault() + "]</b> dir. <br />";
                body += "Sorun halinde temes kurunuz.";
                               
                Haveak_Pro.Models.MailHelper.SendMail(adres, "Kullanıcı adı,Şifre Bilgisi", body);
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ResimGetir(string user)
        {
            ResimData data = db.Kullanicis.Include("AltSiklar").Where(t => t.IsDelete == false && t.IsActive == true && t.KullaniciAdi == user).Select(q => new ResimData
            {
                Resim = q.Personel.Resim,
                Adi = q.Personel.Adi,
                Soyadi = q.Personel.Soyadi,
                ServiceImage = q.Personel.ServiceImage,
                Tcno = q.Personel.TCNo
            }).FirstOrDefault();
            if (data == null)
            {
                data = new ResimData();
                data.Adi = "Kullanıcı";
                data.Resim = "/DocumentFiles/Personel/RYok.jpg";
                data.Soyadi = "Bulunamadı!";
            }
            else
            {
                if (data.ServiceImage != null)
                {
                    string gelenResim = new Haveak_Pro.Models.ImageConvert().ImageWrite(data.ServiceImage, data.Tcno);
                    Haveak_Pro.Models.Personel resimGuncelle = db.Personels.Where(t => t.IsDelete == false && t.IsActive == true && t.TCNo == data.Tcno).FirstOrDefault();
                    resimGuncelle.Resim = gelenResim;
                    resimGuncelle.ServiceImage = null;
                    db.SaveChanges();
                    data.Resim = new Haveak_Pro.Models.ImageConvert().ImageReader(gelenResim);
                }
                else
                {
                    data.Resim = new Haveak_Pro.Models.ImageConvert().ImageReader(data.Resim) == "" ? data.Resim == null ? "/DocumentFiles/Personel/RYok.jpg" : data.Resim : new Haveak_Pro.Models.ImageConvert().ImageReader(data.Resim);
                }
            }
            //double benzerlikOrani = oranDonder();
            //ViewBag.benzerlikOran = benzerlikOrani;
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult KalanSuresi(int user)
        {
            string kalan = db.Kullanicis.Where(w => w.Personel_Ref == user).Select(e => e.BitisTarihi.Value).FirstOrDefault().ToString("yyyy-MM-dd HH:mm:ss"); ;

            return Json(kalan, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        //private string savedocument(string fileName, string fileStream)
        //{
        //    string Fname = "";
        //    try
        //    {
        //        if (fileStream != null)
        //        {
        //            string adres = "";
        //            Fname = Guid.NewGuid().ToString().Split('-')[0] + fileName + ".jpg";
        //            adres = Server.MapPath("~/DocumentFiles/Personel/" + Fname);
        //            Image presim = Base64ToImage(fileStream);
        //            presim.Save(adres, System.Drawing.Imaging.ImageFormat.Jpeg);
        //        }
        //    }
        //    catch 
        //    {
        //        Fname = null;
        //    }
        //    return Fname;
        //}

        public JsonResult ImageCapture(string imagePath, string userName, string password)
        {
            bool durum = false;
            try
            {
                string tcno = string.Empty;
                DateTime zaman = DateTime.Now.Date;
                Haveak_Pro.Models.Kullanici accountControl = db.Kullanicis.Where(t => t.IsActive == true && t.KullaniciAdi == userName && t.Sifre == password && t.BitisTarihi >= zaman).FirstOrDefault();
                if (accountControl != null)
                {
                    tcno = accountControl.Personel.TCNo;
                }

                var path = Server.MapPath("~/DocumentFiles/PersonelWebResim/");
                //if (System.IO.File.Exists(path + kullanici.identity+".jpg"))
                //{
                //    System.IO.File.Delete(path + kullanici.identity + ".jpg");
                //}

                var pathnew = path + tcno + ".jpg";
                Image image = Base64ToImage(imagePath);
                image.Save(pathnew, ImageFormat.Jpeg);
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum);
        }

        protected Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }


        public bool KullaniciLoglama(int kullaniciRef)
        {
            bool durum = false;
            try
            {
                var bugun = DateTime.Now.Date; ;
                var logdetay = db.Loglamas.Where(e => e.GirisTarihi == bugun && e.Kullanici_Ref == kullaniciRef).FirstOrDefault();
                if (logdetay == null)
                {
                    logdetay = new Haveak_Pro.Models.Loglama();
                    logdetay.IsActive = true;
                    logdetay.IsDelete = false;
                    logdetay.Kullanici_Ref = kullaniciRef;
                    logdetay.GirisTarihi = bugun;
                    logdetay.YapilanIs = "Kullanicı Girişi";
                    db.Loglamas.Add(logdetay);
                    db.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return durum;
        }

        //public JsonResult ReadData()
        //{
        //    ManagementScope scope = new ManagementScope("\\\\" + Environment.MachineName + "\\root\\cimv2");

        //    scope.Connect();

        //    ManagementObject wmiClass = new ManagementObject(scope, new ManagementPath("Win32_BaseBoard.Tag=\"Base Board\""), new ObjectGetOptions());

        //    foreach (PropertyData propData in wmiClass.Properties)
        //    {
        //        SerialNumber = scope.Path.ToString();
        //    }
        //}
        public ActionResult CaptchaImage(string prefix, bool noisy = true)
        {
            var rand = new Random((int)DateTime.Now.Ticks);

            //yeni soru üret
            int a = rand.Next(10, 99);
            int b = rand.Next(0, 9);
            var captcha = string.Format("{0} + {1} = ?", a, b);

            //cevabı sakla
            Session["Captcha" + prefix] = a + b;

            //resim oluştur
            FileContentResult img = null;

            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(130, 30))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));

                //karmaşa ekle
                if (noisy)
                {
                    int i, r, x, y;
                    var pen = new Pen(Color.Yellow);
                    for (i = 1; i < 10; i++)
                    {
                        pen.Color = Color.FromArgb(
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)));

                        r = rand.Next(0, (130 / 3));
                        x = rand.Next(0, 130);
                        y = rand.Next(0, 30);

                        gfx.DrawEllipse(pen, x - r, y - r, r, r);
                    }
                }

                //soruyu ekle
                gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.Gray, 2, 3);

                //resim olarak çiz
                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
                img = this.File(mem.GetBuffer(), "image/Jpeg");
            }

            return img;
        }

        public JsonResult ResimKlasorKontrol()
        {
            bool durum = false;
            DateTime bugun = DateTime.Now.Date;
            var sinavlar = db.Personel_Sinavlar.Where(q => q.SinavTarihi < bugun).ToList();
            if (sinavlar.Count > 0)
            {
                foreach (var klasor in sinavlar)
                {
                    Haveak_Pro.Models.Personel_Sinavlar klasoryol = klasor;
                    var pathf = Server.MapPath("~/DocumentFiles/SinavResim/" + klasor.KlasorYolu);
                    Directory.Delete(pathf, true);
                    klasoryol.KlasorYolu = "-";
                    durum = true;
                }
                db.SaveChanges();
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PersonelWebResimSave()
        {
            bool durum = false;
            try
            {
                var kullanici = AktifKullanici.Aktif;
                using (Heas_UzakEntities db = new Heas_UzakEntities())
                {

                    Personel personel = db.Personels.Where(e => e.ID == kullanici.Id).SingleOrDefault();
                    var pathf = Server.MapPath("~/DocumentFiles/PersonelWebResim/" + personel.TCNo);
                    if (!string.IsNullOrEmpty(pathf))
                    {

                        if (System.IO.File.Exists(pathf))
                        {
                            System.IO.File.Delete(pathf);
                        }
                    }
                    DateTime nm = DateTime.Now;
                    string date = nm.ToString("hmmss");
                    var path = pathf + ".jpg";

                    //Image resim = Base64ToImage(path);
                    //resim.Save(path, ImageFormat.Jpeg);


                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum);
        }



        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
    public class ResimData : IDisposable
    {
        public string Resim { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
        public string ServiceImage { get; set; }
        public string Tcno { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

}