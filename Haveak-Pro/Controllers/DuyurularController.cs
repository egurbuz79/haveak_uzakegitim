﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Haveak_Pro.Controllers
{
    public class DuyurularController : Controller
    {
        //
        // GET: /Duyurular/

        Heas_UzakEntities Entity = new Heas_UzakEntities();
        public ActionResult Index()
        {
            var Duyurular = Entity.Duyurulars.Where(t => t.IsDelete == false && t.IsActive == true).ToList();
            return View(Duyurular);
        }

    }
}
