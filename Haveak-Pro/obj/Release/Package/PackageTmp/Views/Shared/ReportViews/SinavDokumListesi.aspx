﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Personel Sinav Sonuc Listesi</title>
    <script runat="server">
        void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                List<Haveak_Pro.Models.sp_SinavDokumu_Result> prs = null;              
                
                using (Haveak_Pro.Models.Heas_UzakEntities entity = new Haveak_Pro.Models.Heas_UzakEntities())
                {
                    DateTime baslangic = Convert.ToDateTime(Request.QueryString["Baslangic"]);
                    DateTime bitis = Convert.ToDateTime(Request.QueryString["Bitis"]);
                    prs = entity.sp_SinavDokumu(baslangic, bitis).ToList();


                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/SinavDokumler.rdlc");
                    ReportViewer1.LocalReport.DataSources.Clear();
                   
                    ReportDataSource rdc = new ReportDataSource("SinavDokumu", prs);
                    ReportViewer1.LocalReport.DataSources.Add(rdc);
                    //ReportViewer1.LocalReport.Refresh();
                }

            }
        }
    
    </script>
</head>
<body>  
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ShowPageNavigationControls="false" ShowBackButton="false" ShowFindControls="false"  ShowCredentialPrompts="false" ShowRefreshButton="false" AsyncRendering ="False" SizeToReportContent="True" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">          
        </rsweb:ReportViewer>
    </form>
    </body>
</html>
