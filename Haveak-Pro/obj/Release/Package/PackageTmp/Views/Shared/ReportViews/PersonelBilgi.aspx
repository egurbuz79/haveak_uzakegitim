﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>PersonelBilgi</title>
    <script runat="server">
        void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                List<Haveak_Pro.Models.sp_Personel_Result> prs = null;
                List<Haveak_Pro.Models.sp_PersonelSinavlar_Result> prsnv = null;
                List<Haveak_Pro.Models.sp_PersonelTestler_Result> prstst = null;
                
                
                using (Haveak_Pro.Models.Heas_UzakEntities entity = new Haveak_Pro.Models.Heas_UzakEntities())
                {
                    int idz = Convert.ToInt32(Request.QueryString["idx"]);
                    prs = entity.sp_Personel(idz).ToList();
                    prsnv = entity.sp_PersonelSinavlar(idz).ToList();
                    prstst = entity.sp_PersonelTestler(idz).ToList();
               
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/OgrenciDurum.rdlc");
                    ReportViewer1.LocalReport.DataSources.Clear();
                    
                    string imagepth = prs.Select(t => t.Resim).FirstOrDefault();                    
                    string imagePath = new Uri(Server.MapPath("~/DocumentFiles/Personel/" + imagepth)).AbsoluteUri;
                    ReportParameter parameter = new ReportParameter("Resim1", imagePath, true);
                    ReportViewer1.LocalReport.EnableExternalImages = true;
                    ReportViewer1.LocalReport.SetParameters(parameter);
                    ReportDataSource rdc = new ReportDataSource("DsPersonelGenel", prs);
                    ReportDataSource rdc2 = new ReportDataSource("DsPersonelSinav", prsnv);
                    ReportDataSource rdc3 = new ReportDataSource("DsPersonelTestler", prstst);
                  
                    ReportViewer1.LocalReport.DataSources.Add(rdc);
                    ReportViewer1.LocalReport.DataSources.Add(rdc2);
                    ReportViewer1.LocalReport.DataSources.Add(rdc3);
                   
                    ReportViewer1.LocalReport.Refresh();                  
                }

            }
        }
    
    </script>
</head>
<body>  
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" AsyncRendering="false" SizeToReportContent="true"></rsweb:ReportViewer>
    </form>
    </body>
</html>
