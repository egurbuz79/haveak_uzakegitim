﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;

namespace Haveak_Pro.Areas.PANEL.Controllers
{
    [System.Web.Mvc.OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_Egitim + "," + UserRoleName.ADMIN_PersonelKullanici + "," + UserRoleName.ADMIN_Sinav)]
    public class DuyurularController : Controller
    {
        //
        // GET: /Panel/Duyurular/

        public DuyurularController()
        {
            AktifKullanici.Aktif.SecilenModul = "PANEL";
        }

        Heas_UzakEntities Entity = new Heas_UzakEntities();

        public ActionResult Index()
        {
            int? firmaId = AktifKullanici.Aktif.FirmaId;
            List<Duyurular> data = new List<Duyurular>();
            if (UserRoleName.PANEL_AdminGiris.InRole())
            {
                data = Entity.Duyurulars.Where(t => t.IsDelete == false).ToList();
            }
            else
            {
                data = Entity.Duyurulars.Where(t => t.IsDelete == false && t.Firma_Ref == firmaId).ToList();
            }
            return View(data);
        }
        [HttpPost]
        public ActionResult DuyuruYayinla(string Baslik, string Duyuru, int Tipi, int Oncelik)
        {
            int? firmaId = AktifKullanici.Aktif.FirmaId;

            Duyurular detay = new Duyurular();
            detay.Baslik = Baslik;
            detay.Duyuru = Duyuru;
            detay.IlanTarihi = DateTime.Now;
            detay.DuyuruTarihi = DateTime.Now;
            detay.Tipi = Tipi;
            detay.Oncelik = Oncelik;
            detay.IsActive = true;
            detay.IsDelete = false;
            if (!UserRoleName.PANEL_AdminGiris.InRole())
            {
                detay.Firma_Ref = firmaId;
            }
            Entity.Duyurulars.Add(detay);
            Entity.SaveChanges();
            return RedirectToAction("Index", "Duyurular", new { area = "Panel" });
        }

        public JsonResult DuyuruGuncelle(int id, int tip, int Oncelik, string Baslik, string Duyuru, bool Active)
        {
            bool result = false;
            try
            {
                var data = Entity.Duyurulars.Where(r => r.ID == id).FirstOrDefault();
                data.IsActive = Active;
                data.Baslik = Baslik;
                data.Duyuru = Duyuru;
                data.DuyuruTarihi = DateTime.Now.Date;
                data.Tipi = tip;
                data.Oncelik = Oncelik;
                Entity.SaveChanges();
                result = true;
            }
            catch
            {
                result = false;
            }
            return Json(result, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DuyuruSil(int id)
        {
            bool result = false;
            try
            {
                var data = Entity.Duyurulars.Where(r => r.ID == id).FirstOrDefault();
                data.IsDelete = true;
                Entity.SaveChanges();
                result = true;
            }
            catch
            {
                result = false;
            }
            return Json(result, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
    }
}
