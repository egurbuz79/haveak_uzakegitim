﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;
using System.Transactions;
using PagedList;

namespace Haveak_Pro.Areas.PANEL.Controllers
{
    [System.Web.Mvc.OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_Egitim)]
    public class EgitimIslemlerController : Controller
    {
        //
        // GET: /Panel/EgitimIslemler/
        public EgitimIslemlerController()
        {
            AktifKullanici.Aktif.SecilenModul = "PANEL";

        }
        Heas_UzakEntities Entity = new Heas_UzakEntities();

        #region Kategori İşlemleri
        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public ActionResult EgitimKategori()
        {
            var data = Entity.Bolums.Where(t => t.IsDelete == false && t.IsActive == true).ToList();
            return View(data);
        }
        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        [HttpPost]
        public ActionResult KategoriEkle(string KategoriAdi)
        {

            Bolum blm = new Bolum();
            blm.IsActive = true;
            blm.IsDelete = false;
            blm.BolumAdi = KategoriAdi;
            Entity.Bolums.Add(blm);
            Entity.SaveChanges();
            return RedirectToAction("EgitimKategori", "EgitimIslemler", new { area = "Panel" });
        }
        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public JsonResult KategoriGuncelle(int id, string KategoriAdi)
        {
            bool durum = false;
            try
            {
                Bolum blm = Entity.Bolums.Where(t => t.ID == id).FirstOrDefault();
                blm.BolumAdi = KategoriAdi;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Modül İşlemler
        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public ActionResult UniteModul()
        {
            var data = Entity.UniteModuls.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            return View(data);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        [HttpPost]
        public ActionResult ModulEkle(string Modul)
        {
            UniteModul tip = new UniteModul();
            tip.IsActive = true;
            tip.IsDelete = false;
            tip.Modul = Modul;
            Entity.UniteModuls.Add(tip);
            Entity.SaveChanges();
            return RedirectToAction("UniteModul", "EgitimIslemler", new { area = "Panel" });
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public JsonResult ModulGuncelle(int ID, string Modul)
        {
            bool durum = false;
            try
            {
                var data = Entity.UniteModuls.Where(r => r.ID == ID).FirstOrDefault();
                data.Modul = Modul;
                Entity.SaveChanges();
                durum = false;

            }
            catch (Exception)
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public JsonResult ModulSilme(int ID)
        {
            bool durum = false;
            try
            {
                var data = Entity.UniteModuls.Where(r => r.ID == ID).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch (Exception)
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Ünite İşlemler

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public ActionResult Unite()
        {
            var Detay = Entity.Unites.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            ViewBag.Moduller = Entity.UniteModuls.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            return View(Detay);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        [HttpPost]
        public ActionResult UniteEkle(string UniteAdi, int Modul_Ref, int Sira)
        {
            Unite tip = new Unite();
            tip.IsActive = true;
            tip.IsDelete = false;
            tip.UniteAdi = UniteAdi;
            tip.Sira = Sira;
            tip.Modul_Ref = Modul_Ref;
            Entity.Unites.Add(tip);
            Entity.SaveChanges();
            return RedirectToAction("Unite", "EgitimIslemler", new { area = "PANEL" });
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public JsonResult UniteGuncelle(int ID, string UniteAdi, int Modul_Ref, int Sira)
        {
            bool durum = false;
            try
            {
                var data = Entity.Unites.Where(r => r.ID == ID).FirstOrDefault();
                data.UniteAdi = UniteAdi;
                data.Modul_Ref = Modul_Ref;
                data.Sira = Sira;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public JsonResult UniteSilme(int ID)
        {
            bool durum = false;
            try
            {
                var data = Entity.Unites.Where(r => r.ID == ID).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch (Exception)
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region KursModüller
        public JsonResult SaveKursModul(int Bolum_Ref, string UniteModul_Ref)
        {
            string[] idd = UniteModul_Ref.Split(',');

            bool kaydet = false;
            try
            {
                foreach (var item in idd.Where(t => t.Length > 0))
                {
                    KursModuller mdl = new KursModuller();
                    mdl.Bolum_Ref = Bolum_Ref;
                    mdl.UniteModule_Ref = int.Parse(item);
                    Entity.KursModullers.Add(mdl);
                }
                Entity.SaveChanges();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("BolumeModulAtama", "EgitimIslemler", new { area = "Panel" });
        }
        public ActionResult KategoriyeModulAtama()
        {
            ViewBag.Bolumler = Entity.Bolums.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            //ViewBag.Moduller = Entity.UniteModuls.Where(t => t.IsDelete == false && t.IsActive == true).ToList();
            var data = Entity.KursModullers.Where(r => r.UniteModul.IsActive == true && r.UniteModul.IsDelete == false && r.Bolum.IsActive == true && r.Bolum.IsDelete == false).ToList();
            return View(data);
        }
        public JsonResult DeleteKursModul(int ID)
        {
            bool durum = false;
            try
            {
                KursModuller mdl = Entity.KursModullers.Where(t => t.ID == ID).FirstOrDefault();
                Entity.KursModullers.Remove(mdl);
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SeciliBolumeAtanmamisKurslar(int ID)
        {
            List<int> idControl = Entity.KursModullers.Where(t => t.Bolum_Ref == ID).Select(y => y.UniteModule_Ref.Value).ToList();
            var mdl = from itemz in Entity.UniteModuls where !idControl.Contains(itemz.ID) && itemz.IsActive == true && itemz.IsDelete == false select new { itemz.ID, itemz.Modul };
            return Json(mdl, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Sınıf İşlemler
        public ActionResult Sinif()
        {
            int? firmaId = AktifKullanici.Aktif.FirmaId;
            List<Firmalar> firmalist = new List<Firmalar>();
            if (UserRoleName.PANEL_AdminGiris.InRole())
            {
                firmalist = Entity.Firmalars.Where(w => w.IsDelete == false).ToList();
            }
            else
            {
                if (firmaId != null)
                {
                    firmalist = Entity.Personels.Where(w => w.IsDelete == false && w.Firma_Ref == firmaId.Value).
                          Select(q => q.Firmalar).Take(1).ToList();
                }
            }
            ViewBag.Firmalar = firmalist;

            //var Detay = Entity.Sinifs.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            return View();
        }

        public ActionResult _SinifListesi(int? firmaRef)
        {
            List<Sinif> siniflar = new List<Sinif>();
            if (UserRoleName.PANEL_AdminGiris.InRole())
            {
                if (firmaRef != 0)
                {
                    siniflar = Entity.Sinifs.Where(t => t.IsActive == true && t.IsDelete == false && t.Firma_Ref == firmaRef).ToList();
                }
                else
                {
                    siniflar = Entity.Sinifs.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
                }
            }
            else
            {
                if (firmaRef != 0)
                {
                    siniflar = Entity.Sinifs.Where(t => t.IsActive == true && t.IsDelete == false && t.Firma_Ref == firmaRef).ToList();
                }
            }
            return PartialView("_SinifListe", siniflar);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SinifEkle(string SinifAdi, int Firma_Ref)
        {
            var control = Entity.Sinifs.Where(t => t.SinifAdi == SinifAdi).Count();
            if (control == 0)
            {
                Sinif tip = new Sinif();
                tip.IsActive = true;
                tip.IsDelete = false;
                tip.SinifAdi = SinifAdi;
                tip.Firma_Ref = Firma_Ref;
                Entity.Sinifs.Add(tip);
                Entity.SaveChanges();
            }

            return RedirectToAction("Sinif", "EgitimIslemler", new { area = "Panel" });
        }

        public JsonResult SinifGuncelle(int ID, string SinifAdi, int Firma_Ref)
        {
            bool durum = false;
            try
            {
                var control = Entity.Sinifs.Where(t => t.SinifAdi == SinifAdi).Count();
                var data = Entity.Sinifs.Where(r => r.ID == ID).FirstOrDefault();
                data.SinifAdi = SinifAdi;
                data.Firma_Ref = Firma_Ref;
                Entity.SaveChanges();
                durum = true;

            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SinifSil(int ID)
        {
            bool durum = false;
            try
            {
                var data = Entity.Sinifs.Where(r => r.ID == ID).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch (Exception)
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SinifOgrenciAtama
        public ActionResult EgitimBolumAtama()
        {
            ViewBag.Bolum = Entity.Bolums.Where(t => t.IsDelete == false && t.IsActive == true && t.KursModullers.Count > 0).ToList();
            ViewBag.Sinif = Entity.Sinifs.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            var ogrenciler = Entity.Kullanicis.Where(e => e.IsActive == true && e.IsDelete == false).ToList();
            //var ogrenciler = Entity.Kullanicis.Where(e => e.IsActive == true && e.IsDelete == false && e.Kull_Rolleri.Select(r => r.Kull_Rol_Adi.Moduller_Ref).FirstOrDefault() != 1).Distinct().ToList();
            return View(ogrenciler);
        }

        public ActionResult _EgitimOgrenciler(int? page)
        {
            int pageSize = 12;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<Kullanici> ogrenciler = Entity.Kullanicis.OrderByDescending(m => m.ID).Where(r => r.IsActive == true && r.IsDelete == false).ToPagedList(pageIndex, pageSize); ;
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            return PartialView("_EgitimOgrenciListe", ogrenciler);
        }

        public JsonResult SinifOgrenciKaydet(int[] Bolum_Ref, int[] Personel_Ref, int Sinif_Ref, DateTime BaslamaTarihi, DateTime BitisTarihi)
        {
            bool durum = false;
            try
            {
                foreach (var itemp in Personel_Ref)
                {
                    var kontrol = Entity.Sinif_Ogrenci.Where(r => r.Sinif_Ref == Sinif_Ref && r.Personel_Ref == itemp && r.Bitti == false).FirstOrDefault();
                    if (kontrol == null)
                    {

                        TransactionScope scp = new TransactionScope();
                        {
                            Sinif_Ogrenci ogr = new Sinif_Ogrenci()
                            {
                                Personel_Ref = itemp,
                                Sinif_Ref = Sinif_Ref,
                                Basladi = true,
                                Bitti = false,
                                BaslamaTarihi = BaslamaTarihi,
                                BitisTarihi = BitisTarihi
                            };
                            Entity.Sinif_Ogrenci.Add(ogr);
                            Entity.SaveChanges();
                            AtananBolumler blm = null;
                            foreach (var itembb in Bolum_Ref)
                            {
                                blm = new AtananBolumler();
                                blm.Bolum_Ref = itembb;
                                blm.Sinif_Ogrenci_Ref = ogr.ID;
                                Entity.AtananBolumlers.Add(blm);
                            }
                            Entity.SaveChanges();
                            scp.Complete();
                        }
                        scp.Dispose();

                    }
                    else
                    {
                        TransactionScope scp = new TransactionScope();
                        {

                            kontrol.BaslamaTarihi = BaslamaTarihi;
                            kontrol.BitisTarihi = BitisTarihi;
                            Entity.SaveChanges();

                            AtananBolumler blm;
                            foreach (var itembb in Bolum_Ref)
                            {
                                var bolumKontrol = Entity.AtananBolumlers.Where(r => r.Sinif_Ogrenci_Ref == itemp && r.Bolum_Ref == Sinif_Ref).FirstOrDefault();
                                if (bolumKontrol == null)
                                {
                                    blm = new AtananBolumler();
                                    blm.Bolum_Ref = itembb;
                                    blm.Sinif_Ogrenci_Ref = kontrol.ID;
                                    Entity.AtananBolumlers.Add(blm);
                                }
                                Entity.SaveChanges();
                                scp.Complete();
                            }
                        }
                        scp.Dispose();
                    }
                }
                durum = true;
            }
            catch
            {
                durum = false;
            }

            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);


            //return RedirectToAction("EgitimBolumAtama", "EgitimIslemler", new { area = "Panel" });
        }
        public ActionResult _SinifData(int sinifId)
        {
            var data = Entity.Sinif_Ogrenci.Where(r => r.Sinif_Ref == sinifId && r.Bitti == false).ToList();
            return PartialView("_SinifOgrenciler", data);
        }
        public JsonResult SinifOgrenciGuncelle(int id, int Personel_Ref, int Sinif_Ref, string BaslamaTarihi, string BitisTarihi, int[] Bolum_Ref, int Bitti)
        {
            bool kaydet = false;
            try
            {
                var data = Entity.Sinif_Ogrenci.Where(t => t.ID == id).FirstOrDefault();
                data.BaslamaTarihi = Convert.ToDateTime(BaslamaTarihi);
                data.BitisTarihi = Convert.ToDateTime(BitisTarihi);
                if (Bitti == 1)
                {
                    data.Bitti = true;
                }
                Entity.SaveChanges();

                var kontrol = Entity.AtananBolumlers.Where(t => t.Sinif_Ogrenci_Ref == id).ToList();
                foreach (var items in kontrol)
                {
                    Entity.AtananBolumlers.Remove(items);
                }
                if (kontrol.Count != 0)
                {
                    Entity.SaveChanges();
                }


                foreach (var iteme in Bolum_Ref)
                {
                    AtananBolumler blm = new AtananBolumler();
                    blm.Bolum_Ref = iteme;
                    blm.Sinif_Ogrenci_Ref = id;
                    Entity.AtananBolumlers.Add(blm);
                }
                Entity.SaveChanges();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }

            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Eğitim arşiv İşlemleri

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public ActionResult Index()
        {
            var list = Entity.Egitim_Arsivi.Where(t => t.Unite.IsActive == true && t.Unite.IsDelete == false && t.IsActive == true && t.IsDelete == false && t.Unite.UniteModul.IsActive == true && t.Unite.UniteModul.IsDelete == false).ToList();
            ViewBag.UnitList = Entity.UniteModuls.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            return View(list);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public JsonResult GetUnitList(int bolumRef)
        {
            var unitList = Entity.Unites.Where(r => r.IsActive == true && r.IsDelete == false && r.UniteModul.KursModullers.Select(g => g.Bolum_Ref).FirstOrDefault() == bolumRef).Select(y => new { y.ID, y.UniteAdi }).ToList();
            return Json(unitList, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        [HttpPost, ValidateInput(false)]
        public ActionResult EgitimArsivEkle(int? Idf, string Baslik, string Icerik, int Unite_Ref, HttpPostedFileBase Video)
        {
            if (Idf == null)
            {

                Egitim_Arsivi detail = new Egitim_Arsivi();
                detail.IsActive = true;
                detail.IsDelete = false;
                detail.Baslik = Baslik;
                detail.Icerik = Icerik;
                detail.Unite_Ref = Unite_Ref;
                detail.Video = savedocument(Video, 2, "");
                Entity.Egitim_Arsivi.Add(detail);
                Entity.SaveChanges();
            }
            else
            {
                Egitim_Arsivi detail = Entity.Egitim_Arsivi.Where(r => r.ID == Idf).First();
                detail.Baslik = Baslik;
                detail.Icerik = Icerik;
                detail.Unite_Ref = Unite_Ref;
                if (Video != null)
                {
                    detail.Video = savedocument(Video, 2, detail.Video);
                }
                Entity.SaveChanges();
            }
            return RedirectToAction("Index", "EgitimIslemler", new { area = "Panel" });
        }

        //[Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        //[ValidateInput(false)]
        //public JsonResult EgitimArsivGuncelle(string Idf, string Baslik, string editor1, string Unite_Ref, HttpPostedFileBase Video)
        //{
        //    bool durum = false;
        //    try
        //    {
        //        int id = Convert.ToInt32(Idf);
        //        int unitRef = Convert.ToInt32(Unite_Ref);
        //        Egitim_Arsivi detail = Entity.Egitim_Arsivi.Where(r => r.ID == id).First();
        //        detail.Baslik = Baslik;
        //        detail.Icerik = editor1;
        //        detail.Unite_Ref = unitRef;
        //        if (Video != null)
        //        {
        //            detail.Video = savedocument(Video, 2, detail.Video);
        //        }
        //        Entity.SaveChanges();
        //        durum = true;
        //    }
        //    catch
        //    {
        //        durum = false;
        //    }
        //    return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        //}

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public JsonResult EgitimArsivSilme(int ID)
        {
            bool durum = false;
            try
            {
                var data = Entity.Egitim_Arsivi.Where(r => r.ID == ID).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch (Exception)
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public ActionResult _EgitimData(int id)
        {
            var uniteList = Entity.Unites.Where(e => e.UniteModul.KursModullers.Select(t => t.Bolum_Ref).FirstOrDefault() == id).Select(t => t.ID).ToList();
            List<Egitim_Arsivi> dataAll = new List<Egitim_Arsivi>();
            foreach (var item in uniteList)
            {
                Egitim_Arsivi data = Entity.Egitim_Arsivi.Where(t => t.IsActive == true && t.IsDelete == false && t.Unite_Ref == item).First();
                dataAll.Add(data);
            }
            return PartialView("_EgitimArsivi", dataAll);
        }
        #endregion

        #region Global İşlemler
        protected string savedocument(HttpPostedFileBase document, int type, string gelen)
        {
            if (document != null)
            {
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid() + f.Name;
                string path = "";
                if (type == 1)
                {
                    var silDosya = Server.MapPath("~/" + gelen);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    path = "DocumentFiles/" + Fname;
                }
                else
                {
                    var silDosya = Server.MapPath("~/" + gelen);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    path = "DocumentFiles/videos/" + Fname;
                }
                document.SaveAs(Server.MapPath("~/" + path));
                return path;
            }
            else
            {
                return "";
            }
        }

        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }
        #endregion
    }
}
