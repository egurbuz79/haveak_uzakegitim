﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;
using PagedList;

namespace Haveak_Pro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_PersonelKullanici)]
    public class PersonelIslemlerController : Controller
    {
        //
        // GET: /Panel/PersonelIslemler/
        public PersonelIslemlerController()
        {
            AktifKullanici.Aktif.SecilenModul = "PANEL";
        }

        Heas_UzakEntities Entity = new Heas_UzakEntities();

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public ActionResult PersonelFirma()
        {           
            var firmalar = Entity.Firmalars.ToList();
            return View(firmalar);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        [HttpPost]
        public ActionResult FirmaKaydet(string FirmaAdi, string Email, string Telefon, string Detay)
        {
            Firmalar detay = new Firmalar();
            detay.FirmaAdi = FirmaAdi;
            detay.Email = Email;
            detay.Telefon = Telefon;
            detay.Detay = Detay;
            detay.IsActive = true;
            detay.IsDelete = false;
            Entity.Firmalars.Add(detay);
            Entity.SaveChanges();
            return RedirectToAction("PersonelFirma", "PersonelIslemler", new { area = "Panel" });
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public JsonResult FirmaGuncelle(int id, string FirmaAdi, string Email, string Telefon, string Detay, bool Active)
        {
            bool result = false;
            try
            {
                var data = Entity.Firmalars.Where(r => r.ID == id).FirstOrDefault();
                data.IsActive = Active;
                data.FirmaAdi = FirmaAdi;
                data.Email = Email;
                data.Telefon = Telefon;
                data.Detay = Detay;
                Entity.SaveChanges();
                result = true;
            }
            catch
            {
                result = false;
            }
            return Json(result, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public JsonResult FirmaSil(int id)
        {
            bool result = false;
            try
            {
                var data = Entity.Firmalars.Where(r => r.ID == id).FirstOrDefault();
                data.IsDelete = true;
                Entity.SaveChanges();
                result = true;
            }
            catch
            {
                result = false;
            }
            return Json(result, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        public ActionResult PersonelListe(int? page)
        {
            int? firmaId = AktifKullanici.Aktif.FirmaId;
            int pageSize = 12;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            IPagedList<Personel> data = null;
            if (UserRoleName.PANEL_AdminGiris.InRole())
            {
                data = Entity.Personels.OrderByDescending(m => m.ID).Where(r => r.IsActive == true && r.IsDelete == false).ToPagedList(pageIndex, pageSize);
            }
            else
            {
                data = Entity.Personels.OrderByDescending(m => m.ID).Where(r => r.IsActive == true && r.IsDelete == false && r.Firma_Ref == firmaId).ToPagedList(pageIndex, pageSize);
            }
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            ViewBag.KisiAra = "";
            return View(data);
        }

        public ActionResult PersonelAra(int? page, string adsoyad)
        {
            int pageSize = 12;
            int pageIndex = 1;
            int recordNo = 0;
            string adi = adsoyad.Split(' ')[0];
            string soyadi = string.Empty;
            try
            {
                soyadi = adsoyad.Split(' ')[1];
            }
            catch 
            {
                soyadi = "";
            }
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IQueryable<Personel> data = Entity.Personels.OrderByDescending(m => m.ID).Where(r => r.IsActive == true && r.IsDelete == false);
            if (adi != "" && soyadi == "")
            {
                data = data.Where(t => t.Adi.Contains(adi) || t.Soyadi.Contains(adi));
            }
            if (soyadi != "" && adi == "")
            {
                data = data.Where(t => t.Soyadi.Contains(soyadi));
            }
            if (adi != "" && soyadi != "")
            {
                data = data.Where(r => r.Adi.Contains(adi) && r.Soyadi.Contains(soyadi));
            }
            IPagedList<Personel> datasonuc = data.ToPagedList(pageIndex, pageSize);
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            ViewBag.KisiAra = adsoyad;
            return View("PersonelListe", datasonuc);
        }

        public ActionResult PersonelIslem(int? id)
        {
            int dx = id == null ? 0 : id.Value;
            int? firmaId = AktifKullanici.Aktif.FirmaId;
            List<Firmalar> firmalist = new List<Firmalar>();
            if (UserRoleName.PANEL_AdminGiris.InRole())
            {
                firmalist = Entity.Firmalars.Where(w => w.IsDelete == false).ToList();
            }
            else
            {
                if (firmaId != null)
                {
                    firmalist = Entity.Personels.Where(w => w.IsDelete == false && w.Firma_Ref == firmaId.Value).
                          Select(q => q.Firmalar).Take(1).ToList();
                }
            }
            ViewBag.Firmalar = firmalist;
            return View(dx);
        }


        [HttpPost]
        public ActionResult PersonelEkle(string Adi, string Soyadi, string TCNo, string IsTelefon, string EvTelefon, string CepTelefon, string Email, string Adresi, string Cinsiyet, string DogumTarihi, HttpPostedFileBase Resim, int? Firmalar)
        {
            try
            {
                Personel detay = new Personel();
                detay.IsActive = true;
                detay.IsDelete = false;
                detay.Adi = Adi;
                detay.Soyadi = Soyadi;
                detay.TCNo = TCNo;
                detay.IsTelefon = IsTelefon;
                detay.EvTelefon = EvTelefon;
                detay.CepTelefon = CepTelefon;
                detay.Email = Email;
                detay.Adresi = Adresi;
                detay.Cinsiyet = Cinsiyet;
                detay.DogumTarihi = Convert.ToDateTime(DogumTarihi);
                if (Resim != null)
                {
                    detay.Resim = savedocument(Resim);
                }
                if (Firmalar != null)
                {
                    detay.Firma_Ref = Firmalar;
                }
                Entity.Personels.Add(detay);
                Entity.SaveChanges();
            }
            catch
            {

            }
            return RedirectToAction("PersonelListe", "PersonelIslemler", new { area = "Panel" });
        }

        public JsonResult PersonelGuncelle(int ID, string Adi, string Soyadi, string TCNo, string IsTelefon, string EvTelefon, string CepTelefon, string Email, string Adresi, string Cinsiyet, DateTime DogumTarihi, HttpPostedFileBase Resim, int FirmaRef)
        {
            bool result = false;
            try
            {
                Personel detay = Entity.Personels.Where(r => r.ID == ID).FirstOrDefault();
                detay.Adi = Adi;
                detay.Soyadi = Soyadi;
                detay.TCNo = TCNo;
                detay.IsTelefon = IsTelefon;
                detay.EvTelefon = EvTelefon;
                detay.CepTelefon = CepTelefon;
                detay.Email = Email;
                detay.Adresi = Adresi;
                detay.Cinsiyet = Cinsiyet;
                detay.DogumTarihi = DogumTarihi;
                if (FirmaRef != 0)
                {
                    detay.Firma_Ref = FirmaRef;
                }
                else
                {
                    detay.Firma_Ref = null;
                }

                if (Resim != null)
                {
                    var silDosya = Server.MapPath("~/DocumentFiles/Personel/" + detay.Resim);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    detay.Resim = savedocument(Resim);
                }
                Entity.SaveChanges();
                Kullanici kullanici = Entity.Kullanicis.Where(t => t.Personel_Ref == detay.ID).FirstOrDefault();
                Entity.SaveChanges();

                result = true;
            }
            catch
            {
                result = false;
            }
            return Json(result, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        protected string savedocument(HttpPostedFileBase document)
        {
            if (document != null)
            {
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid().ToString().Split('-')[0] + f.Name;
                document.SaveAs(Server.MapPath("~/DocumentFiles/Personel/" + Fname));
                return Fname;
            }
            else
            {
                return "";
            }

        }

        public ActionResult KullaniciRoller(int? page, string adi, string soyadi)
        {
            int? firmaId = AktifKullanici.Aktif.FirmaId;
            int pageSize = 42;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<Kull_Rolleri> detail = null;
            if ((adi == "" || adi == null) && (soyadi == "" || soyadi == null))
            {
                if (UserRoleName.PANEL_AdminGiris.InRole())
                {
                    detail = Entity.Kull_Rolleri.OrderBy(m => m.Kullanici.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false).ToPagedList(pageIndex, pageSize);
                }
                else
                {
                    detail = Entity.Kull_Rolleri.OrderBy(m => m.Kullanici.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false && r.Kullanici.Personel.Firma_Ref == firmaId).ToPagedList(pageIndex, pageSize);
                }
            }
            else
            {
                if (UserRoleName.PANEL_AdminGiris.InRole())
                {
                    detail = Entity.Kull_Rolleri.OrderBy(m => m.Kullanici.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false && (r.Kullanici.Personel.Adi.Contains(adi) && r.Kullanici.Personel.Soyadi.Contains(soyadi))).ToPagedList(pageIndex, pageSize);
                }
                else
                {
                    detail = Entity.Kull_Rolleri.OrderBy(m => m.Kullanici.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false && r.Kullanici.Personel.Firma_Ref == firmaId && (r.Kullanici.Personel.Adi.Contains(adi) && r.Kullanici.Personel.Soyadi.Contains(soyadi))).ToPagedList(pageIndex, pageSize);
                }
            }
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            //var detail = Entity.Kull_Rolleri.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            ViewBag.Kull_Rol_Name = Entity.Kull_Rol_Adi.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            List<Kullanici> kullanicilar = new List<Kullanici>();
            if (UserRoleName.PANEL_AdminGiris.InRole())
            {
                kullanicilar = Entity.Kullanicis.Where(r => r.IsActive == true && r.IsDelete == false && r.Kull_Rolleri.Count == 0).ToList();
            }
            else
            {
                kullanicilar = Entity.Kullanicis.Where(r => r.IsActive == true && r.IsDelete == false && r.Kull_Rolleri.Count == 0 && r.Personel.Firma_Ref == firmaId).ToList();
            }
            ViewBag.Kullanici = kullanicilar;
            return View(detail);
        }

        [HttpPost]
        public ActionResult KullaniciRollerEkle(KullaniciRollerData model)
        {
            foreach (var item in model.User_Ref)
            {
                foreach (var itemRol in model.User_Role_Name_Ref)
                {
                    var rolKontrol = Entity.Kull_Rolleri.Where(y => y.IsActive == true && y.IsDelete == false && y.User_Ref == item && y.User_Role_Name_Ref == itemRol).FirstOrDefault();
                    if (rolKontrol == null)
                    {
                        Kull_Rolleri tip = new Kull_Rolleri();
                        tip.IsActive = true;
                        tip.IsDelete = false;
                        tip.User_Ref = item;
                        tip.User_Role_Name_Ref = itemRol;
                        Entity.Kull_Rolleri.Add(tip);
                    }
                    else
                    {
                        rolKontrol.IsActive = true;
                        rolKontrol.IsDelete = false;
                    }
                    Entity.SaveChanges();
                }
            }

            return RedirectToAction("KullaniciRoller", "PersonelIslemler", new { area = "Panel" });
        }

        public JsonResult KullRollerGuncelle(int User_Role_Name_RefUp, int User_RefUp, bool control)
        {
            bool result = false;
            try
            {
                var data = Entity.Kull_Rolleri.Where(r => r.User_Ref == User_RefUp && r.User_Role_Name_Ref == User_Role_Name_RefUp).FirstOrDefault();
                if (data == null)
                {
                    Kull_Rolleri yeni = new Kull_Rolleri();
                    yeni.IsActive = true;
                    yeni.IsDelete = false;
                    yeni.User_Ref = User_RefUp;
                    yeni.User_Role_Name_Ref = User_Role_Name_RefUp;
                    Entity.Kull_Rolleri.Add(yeni);
                }
                else
                {
                    if (control)
                    {
                        data.IsActive = true;
                        data.IsDelete = false;
                    }
                    else
                    {
                        data.IsActive = false;
                        data.IsDelete = true;
                    }
                }
                Entity.SaveChanges();
                result = true;
            }
            catch
            {
                result = false;
            }
            return Json(result, "apllication/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Kullanicilar()
        {

            //ViewBag.Siniflar = Entity.Sinifs.Where(t => t.IsActive == true && t.IsDelete == false).ToList();           

            int? firmaId = AktifKullanici.Aktif.FirmaId;
            List<Firmalar> firmalist = new List<Firmalar>();
            if (UserRoleName.PANEL_AdminGiris.InRole())
            {
                firmalist = Entity.Firmalars.Where(w => w.IsDelete == false).ToList();
            }
            else
            {
                if (firmaId != null)
                {
                    firmalist = Entity.Personels.Where(w => w.IsDelete == false && w.Firma_Ref == firmaId.Value).
                          Select(q => q.Firmalar).Take(1).ToList();
                }
            }
            ViewBag.Firmalar = firmalist;

            return View();
        }

        public ActionResult _KullaniciListesi(int? page, int? firmaRef)
        {
            int pageSize = 24;
            int pageIndex = 1;
            int recordNo = 0;

            List<Sinif> siniflar = new List<Sinif>();
            IPagedList<Kullanici> data = null;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            int ogrAdet = 0;
            if (UserRoleName.PANEL_AdminGiris.InRole())
            {
                if (firmaRef != 0)
                {
                    data = Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false && r.Personel.Firma_Ref == firmaRef).ToPagedList(pageIndex, pageSize);
                    ogrAdet = Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false && r.Personel.Firma_Ref == firmaRef).Count();
                    siniflar = Entity.Sinif_Ogrenci.Where(t => t.Sinif.IsActive == true && t.Sinif.IsDelete == false && t.Personel.Firma_Ref == firmaRef).Select(e => e.Sinif).Distinct().ToList();
                }
                else
                {
                    data = Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false).ToPagedList(pageIndex, pageSize);
                    ogrAdet = Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false).Count();
                    siniflar = Entity.Sinifs.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
                }

            }
            else
            {
                if (firmaRef != 0)
                {
                    data = Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false && r.Personel.Firma_Ref == firmaRef && r.Kull_Rolleri.Where(t => t.User_Role_Name_Ref != 1).Count() > 0).ToPagedList(pageIndex, pageSize);
                    ogrAdet = Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false && r.Personel.Firma_Ref == firmaRef && r.Kull_Rolleri.Where(t => t.User_Role_Name_Ref != 1).Count() > 0).Count();
                    siniflar = Entity.Sinif_Ogrenci.Where(t => t.Sinif.IsActive == true && t.Sinif.IsDelete == false && t.Personel.Firma_Ref == firmaRef).Select(e => e.Sinif).Distinct().ToList();
                }
                //data = Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false && r.Kull_Rolleri.Where(t => t.User_Role_Name_Ref != 1).Count() > 0).ToPagedList(pageIndex, pageSize);
            }
            //ViewBag.Personel = Entity.Personels.Where(r => r.IsActive == true && r.IsDelete == false && Entity.Kullanicis.Where(y => y.Personel_Ref == r.ID && y.IsActive == true && y.IsDelete == false).Count() == 0).ToList();
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            ViewBag.OgrenciAdet = ogrAdet;
            ViewBag.Siniflar = siniflar;
            return PartialView("_KullaniciListe", data);
        }


        public JsonResult FirmaPersoneli(int firmaId)
        {
            var personelListe = Entity.Personels.Where(r => r.IsActive == true && r.IsDelete == false && Entity.Kullanicis.Where(y => y.Personel_Ref == r.ID && y.IsActive == true && y.IsDelete == false).Count() == 0).Select(w => new { w.ID, w.Adi, w.Soyadi, w.TCNo, w.Firma_Ref }).ToList();
            if (firmaId != 0)
            {
                personelListe = personelListe.Where(q => q.Firma_Ref == firmaId).ToList();
            }
            return Json(personelListe, "apllication/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PersonelGetir(int sinifi, string padi, string psoyadi)
        {

            List<PersonelBilgi> data = new List<PersonelBilgi>();
            if (sinifi != 0)
            {
                var ddp = (from dx in Entity.Kullanicis
                           orderby dx.Personel.Adi
                           where dx.IsActive == true && dx.IsDelete == false &&
                               dx.Personel.Sinif_Ogrenci.Where(e => e.Sinif_Ref == sinifi).Select(t => t.ID).Count() > 0 &&
                               ((dx.Personel.Adi.Contains(padi)) &&
                               (dx.Personel.Soyadi.Contains(psoyadi)) &&
                               (dx.Personel.Adi.Contains(padi) || dx.Personel.Soyadi.Contains(psoyadi)))
                           select dx).ToList();

                for (int i = 0; i < ddp.Count; i++)
                {
                    PersonelBilgi psr = new PersonelBilgi();
                    psr.ID = ddp[i].ID;
                    var perref = ddp[i].Personel_Ref;
                    psr.Sinif = Entity.Sinif_Ogrenci.Where(r => r.Personel_Ref == perref).Select(e => e.Sinif.SinifAdi).ToList();
                    psr.Adi = ddp[i].Personel.Adi;
                    psr.Soyadi = ddp[i].Personel.Soyadi;
                    psr.Kullaniciadi = ddp[i].KullaniciAdi;
                    psr.BaslangicTarihi = ddp[i].BaslangicTarihi;
                    psr.BitisTarihi = ddp[i].BitisTarihi.Value;
                    psr.Sifre = ddp[i].Sifre;
                    psr.Personel_Ref = ddp[i].Personel_Ref;
                    psr.Resim = ddp[i].Personel.Resim;
                    data.Add(psr);
                }
            }
            else
            {
                var ddp = (from dx in Entity.Kullanicis
                           orderby dx.Personel.Adi
                           where dx.IsActive == true && dx.IsDelete == false &&
                               ((dx.Personel.Adi.Contains(padi)) &&
                               (dx.Personel.Soyadi.Contains(psoyadi)) &&
                               (dx.Personel.Adi.Contains(padi) || dx.Personel.Soyadi.Contains(psoyadi)))
                           select dx).ToList();

                for (int i = 0; i < ddp.Count; i++)
                {
                    PersonelBilgi psr = new PersonelBilgi();
                    psr.ID = ddp[i].ID;
                    var perref = ddp[i].Personel_Ref;
                    psr.Sinif = Entity.Sinif_Ogrenci.Where(r => r.Personel_Ref == perref).Select(e => e.Sinif.SinifAdi).ToList();
                    psr.Adi = ddp[i].Personel.Adi;
                    psr.Soyadi = ddp[i].Personel.Soyadi;
                    psr.Kullaniciadi = ddp[i].KullaniciAdi;
                    psr.BaslangicTarihi = ddp[i].BaslangicTarihi;
                    psr.BitisTarihi = ddp[i].BitisTarihi.Value;
                    psr.Sifre = ddp[i].Sifre;
                    psr.Personel_Ref = ddp[i].Personel_Ref;
                    psr.Resim = ddp[i].Personel.Resim;
                    data.Add(psr);
                }




                //     data = Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true
                //&& r.IsDelete == false &&
                //((r.Personel.Adi.Contains(padi)) &&
                //(r.Personel.Soyadi.Contains(psoyadi)) &&
                //(r.Personel.Adi.Contains(padi) || r.Personel.Soyadi.Contains(psoyadi)))
                //).Select(f => new PersonelBilgi
                //{
                //    ID = f.ID,
                //    Sinif =  Entity.Sinif_Ogrenci.Where(r=>r.Personel_Ref == r.Personel_Ref).Select(e=>e.Sinif.SinifAdi).ToList(),
                //    Adi = f.Personel.Adi,
                //    Soyadi = f.Personel.Soyadi,
                //    Kullaniciadi = f.KullaniciAdi,
                //    BaslangicTarihi = f.BaslangicTarihi,
                //    BitisTarihi = f.BitisTarihi.Value,
                //    Sifre = f.Sifre,
                //    Personel_Ref = f.Personel_Ref
                //})
                //.ToList();
            }

            return Json(data, "apllication/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SinifPersonelGetir(int id, int? firmaRef)
        {
            int? firmaId = AktifKullanici.Aktif.FirmaId;

            List<PersonelBilgi> dataBilgi = new List<PersonelBilgi>();
            List<Kullanici> ddp = new List<Kullanici>();
            if (UserRoleName.PANEL_AdminGiris.InRole())
            {
                if (firmaRef != 0)
                {
                    ddp = (from dx in Entity.Kullanicis
                           orderby dx.Personel.Adi
                           where dx.IsActive == true && dx.IsDelete == false && dx.Personel.Firma_Ref == firmaRef &&
                                               dx.Personel.Sinif_Ogrenci.Where(q => q.Sinif_Ref == id).Count() > 0
                           select dx).ToList();
                }
                else
                {
                    ddp = (from dx in Entity.Kullanicis
                           orderby dx.Personel.Adi
                           where dx.IsActive == true && dx.IsDelete == false &&
                                dx.Personel.Sinif_Ogrenci.Where(q => q.Sinif_Ref == id).Count() > 0
                           select dx).ToList();
                }
            }
            else
            {
                ddp = (from dx in Entity.Kullanicis
                       orderby dx.Personel.Adi
                       where dx.IsActive == true && dx.IsDelete == false && dx.Personel.Firma_Ref == firmaId &&
                                           dx.Personel.Sinif_Ogrenci.Where(q => q.Sinif_Ref == id).Count() > 0
                       select dx).ToList();
            }
            for (int i = 0; i < ddp.Count; i++)
            {
                PersonelBilgi psr = new PersonelBilgi();
                psr.ID = ddp[i].ID;
                var perref = ddp[i].Personel_Ref;
                psr.Sinif = Entity.Sinif_Ogrenci.Where(r => r.Personel_Ref == perref).Select(e => e.Sinif.SinifAdi).ToList();
                psr.Adi = ddp[i].Personel.Adi;
                psr.Soyadi = ddp[i].Personel.Soyadi;
                psr.Kullaniciadi = ddp[i].KullaniciAdi;
                psr.BaslangicTarihi = ddp[i].BaslangicTarihi;
                psr.BitisTarihi = ddp[i].BitisTarihi.Value;
                psr.Sifre = ddp[i].Sifre;
                psr.Personel_Ref = ddp[i].Personel_Ref;
                psr.Resim = ddp[i].Personel.Resim;
                dataBilgi.Add(psr);
            }




            //data.Add(dataBilgi);
            //}

            return Json(dataBilgi, "apllication/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult KullaniciKontrol(string KullaniciAdi)
        {
            bool kontrol = Entity.Kullanicis.Where(t => t.KullaniciAdi == KullaniciAdi && t.IsActive == true && t.IsDelete == false).FirstOrDefault() == null ? false : true;
            return Json(kontrol, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult KullaniciEkle(string KullaniciAdi, string Sifre, DateTime BaslangicTarihi, DateTime BitisTarihi, int Personel_Ref)
        {
            var kontrol = Entity.Kullanicis.Where(t => t.KullaniciAdi == KullaniciAdi && t.IsActive == false).FirstOrDefault();
            if (kontrol != null)
            {
                kontrol.IsActive = true;
                kontrol.IsDelete = false;
                kontrol.BaslangicTarihi = BaslangicTarihi;
                kontrol.BitisTarihi = BitisTarihi;
                kontrol.KullaniciAdi = KullaniciAdi;
                kontrol.Sifre = Sifre;
            }
            else
            {
                Kullanici tip = new Kullanici();
                tip.IsActive = true;
                tip.IsDelete = false;
                tip.KullaniciAdi = KullaniciAdi;
                tip.Sifre = Sifre;
                tip.BaslangicTarihi = BaslangicTarihi;
                tip.BitisTarihi = BitisTarihi;
                tip.Personel_Ref = Personel_Ref;
                Entity.Kullanicis.Add(tip);
            }

            Entity.SaveChanges();

            return RedirectToAction("Kullanicilar", "PersonelIslemler", new { area = "Panel" });
        }

        public JsonResult KullaniciGuncelle(int ID, string KullaniciAdi, string Sifre, DateTime BaslangicTarihi, DateTime BitisTarihi)
        {
            bool result = false;
            try
            {
                var data = Entity.Kullanicis.Where(r => r.ID == ID).FirstOrDefault();
                data.KullaniciAdi = KullaniciAdi;
                data.Sifre = Sifre;
                data.BaslangicTarihi = BaslangicTarihi;
                data.BitisTarihi = BitisTarihi;
                Entity.SaveChanges();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return Json(result, "apllication/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult KullaniciSil(int ID)
        {
            bool result = false;
            try
            {
                var data = Entity.Kullanicis.Where(r => r.ID == ID).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return Json(result, "apllication/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult PersonelRapor(int? idx)
        {
            return View(idx);
        }

    }

    public class KullaniciRollerData
    {
        public int[] User_Role_Name_Ref { get; set; }
        public int[] User_Ref { get; set; }
    }

    public class PersonelBilgi : IDisposable
    {
        public int ID { get; set; }
        public List<string> Sinif { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
        public string Kullaniciadi { get; set; }
        public string Sifre { get; set; }
        public DateTime BaslangicTarihi { get; set; }
        public DateTime BitisTarihi { get; set; }
        public int Personel_Ref { get; set; }
        public string Resim { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
