﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;

using System.Web.Security;
using System.Text;


namespace Haveak_Pro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class HomeController : Controller
    {
        //
        // GET: /Panel/Home/

        Heas_UzakEntities Entity = new Heas_UzakEntities();
        public HomeController()
        {
            AktifKullanici.Aktif.SecilenModul = "PANEL";          
                
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_Egitim+ "," + UserRoleName.ADMIN_PersonelKullanici + "," + UserRoleName.ADMIN_Sinav)]
        public ActionResult Index()
        {
            ViewBag.Sinavlar = Entity.Personel_Sinavlar.Where(t => t.SinavAciklama == true && t.IsDelete == false).Select(r => r.Durum).ToList();
            var sorular = Entity.SinavTipis.ToList();
            var listSoru = sorular.Where(t => t.UniteSorulars.Where(r=>r.IsActive == true && r.IsDelete == false).Count() > 0).Select(q=>q.UniteSorulars).ToList();

            List<SinavDurum> datac = new List<SinavDurum>();
            foreach (var items in listSoru)
            {               
                var datax = items.GroupBy(t => new { t.SinavTipi.Baslik, t.SinavTipi_Ref }).Select(q => new SinavDurum { Baslik = q.Key.Baslik, sinavRef = q.Count() }).FirstOrDefault();
                datac.Add(datax);
            }
            ViewBag.Sorular = datac;

            //ViewBag.Astro = Entity.Bagajlars.Where(t => t.IsActive == true && t.IsDelete == false).Select(r => r.ID).Count();
            //ViewBag.Hieman = Entity.HBagajlars.Where(t => t.IsActive == true && t.IsDelete == false).Select(r => r.ID).Count();
            //ViewBag.SBagaj = Entity.SBagajlars.Where(t => t.IsActive == true && t.IsDelete == false).Select(r => r.ID).Count();
            //ViewBag.Madde = Entity.Maddelers.Where(t => t.IsActive == true && t.IsDelete == false).Select(r => r.ID).Count();
            //ViewBag.TMaddeler = Entity.SBMaddeResimlers.Where(t => t.IsActive == true && t.IsDelete == false).Select(r => r.ID).Count();
            ViewBag.TumKullanici = Entity.Kullanicis.Where(t => t.IsActive == true && t.IsDelete == false).Count();
            DateTime bugun =  DateTime.Now.Date;
            ViewBag.AktifUyeler = Entity.Kullanicis.Where(r => r.Sistemde == true && r.Songiristarihi.Value == bugun).ToList();
            return View();
        }

        #region Resim Tip İşlemler
        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        //public ActionResult ResimTip()
        //{
        //    var Detay = Entity.ResimTipis.ToList();
        //    return View(Detay);
        //}
        //[HttpPost]
        //public ActionResult SaveResimTip(string ResimTip)
        //{
        //    ResimTipi tip = new ResimTipi();
        //    tip.IsActive = true;
        //    tip.IsDelete = false;
        //    tip.TipAdi = ResimTip;
        //    Entity.ResimTipis.Add(tip);
        //    Entity.SaveChanges();
        //    return RedirectToAction("ResimTip", "Home", new { area = "Panel" });
        //}

        //public ActionResult UpdateResimTip(int ID, string ResimTipUp, bool IsActive, bool IsDelete)
        //{
        //    string result = "";
        //    try
        //    {
        //        var data = Entity.ResimTipis.Where(r => r.ID == ID).FirstOrDefault();
        //        data.TipAdi = ResimTipUp;
        //        data.IsActive = IsActive;
        //        data.IsDelete = IsDelete;
        //        Entity.SaveChanges();
        //        result = "[{\"durum\":true}]";
        //    }
        //    catch (Exception)
        //    {
        //        result = "[{\"durum\":false}]";
        //    }
        //    ContentResult cr = new ContentResult();
        //    cr.ContentEncoding = System.Text.Encoding.UTF8;
        //    cr.ContentType = "application/json";
        //    cr.Content = result;
        //    return cr;
        //}
        #endregion       

     
        public ActionResult SenMassageUser(int Personel_Ref, string Icerik, int Alici_Ref)
        {
            //string gonderen = "";
            //int kullaniciID = Entity.Kullanicis.Where(y => y.Personel_Ref == Personel_Ref).Select(r => r.ID).FirstOrDefault();
            //var control = Entity.Kull_Rolleri.Where(t => t.User_Ref == kullaniciID).ToList();
            //foreach (var item in control)
            //{
            //    if (item.User_Role_Name_Ref == 1)
            //    {
            //        gonderen = "out";
            //    }
            //    else
            //    {
            //        gonderen = "in";
            //    }
            //}
            string result = "";
            try
            {
                var data = new Mesajlar();
                data.IsActive = true;
                data.IsDelete = false;
                data.Gonderen_Ref = Personel_Ref;
                data.Alici_Ref = Alici_Ref;
                data.Icerik = Icerik;               
                data.Tarih = DateTime.Now;             
                Entity.Mesajlars.Add(data);
                Entity.SaveChanges();
                result = "[{\"durum\":true}]";
            }
            catch (Exception)
            {
                result = "[{\"durum\":false}]";
            }
            ContentResult cr = new ContentResult();
            cr.ContentEncoding = System.Text.Encoding.UTF8;
            cr.ContentType = "application/json";
            cr.Content = result;
            return cr;
        }


        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }
    }
    public class SinavDurum
    {
        public int sinavRef { get; set; }
        public string Baslik { get; set; }
    }
}
