﻿using Haveak_Pro.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Haveak_Pro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_PersonelKullanici)]
    public class RaporlamaController : Controller
    {
        //
        // GET: /PANEL/Raporlama/

        Heas_UzakEntities Entity = new Heas_UzakEntities();

        public ActionResult KullaniciGirisKontrol(int? page)
        {
            int pageSize = 24;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<Kullanici> data = Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false).ToPagedList(pageIndex, pageSize);
            //ViewBag.Personel = Entity.Personels.Where(r => r.IsActive == true && r.IsDelete == false && Entity.Kullanicis.Where(y => y.Personel_Ref == r.ID && y.IsActive == true && y.IsDelete == false).Count() == 0).ToList();
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.Siniflar = Entity.Sinifs.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            ViewBag.RecordNo = recordNo;
            return View(data);
        }

        public JsonResult getPersonel(int param)
        {
            List<sp_Personel_Result> dty = Entity.sp_Personel(param).ToList();
            return Json(dty, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPersonelSinavlar(int param)
        {
            List<sp_PersonelSinavlar_Result> dty = Entity.sp_PersonelSinavlar(param).ToList();
            return Json(dty, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPersonelTestler(int param)
        {
            List<sp_PersonelTestler_Result> dty = Entity.sp_PersonelTestler(param).ToList();
            return Json(dty, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getEgitimResimler(string tcno)
        {
            ImageConvert ic = new ImageConvert();
            List<EducationImages_View> files = ic.getEducationImages(tcno);
            return Json(files, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }

    }
}
