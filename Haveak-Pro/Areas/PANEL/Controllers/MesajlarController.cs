﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;

namespace Haveak_Pro.Areas.PANEL.Controllers
{
    public class MesajlarController : Controller
    {
        //
        // GET: /Panel/Mesajlar/
        Heas_UzakEntities Entity = new Heas_UzakEntities();
        public ActionResult Index()
        {
            var list = Entity.Mesajlars.ToList();
            ViewBag.Personel = Entity.Personels.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            return View(list);
        }

        [HttpPost]
        public ActionResult SendMessage(int Personel_Ref, string Icerik)
        {
            var mesaj = new Mesajlar();
            mesaj.IsActive = true;
            mesaj.IsDelete = false;
            //mesaj.Personel_Ref = Personel_Ref;
            mesaj.Icerik = Icerik;           
            mesaj.Tarih = DateTime.Now;
            Entity.Mesajlars.Add(mesaj);
            Entity.SaveChanges();
            return RedirectToAction("Index", "Mesajlar", new { area="Panel" });
        }

        

    }
}
