﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;
//using Haveak_Pro.Areas.PANEL.Models;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using Newtonsoft.Json.Linq;
using Haveak_Pro.Areas.PANEL.Models;
using Haveak_Pro.Areas.SINAV.Models;

namespace Haveak_Pro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Authorize(Roles = UserRoleName.ADMIN_Sinav + "," + UserRoleName.PANEL_AdminGiris)]
    public class SinavIslemlerController : Controller
    {
        //
        // GET: /Panel/SınavIslemler/
        Heas_UzakEntities Entity = new Heas_UzakEntities();
        public SinavIslemlerController()
        {
            AktifKullanici.Aktif.SecilenModul = "PANEL";
        }

        #region Soru İşlemler
        public ActionResult YeniSoruTanimlama()
        {
            ViewBag.UniteModuller = Entity.UniteModuls.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            //ViewBag.STMKategori = Entity.SBTehlikeliMKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToList(); ;
            //ViewBag.SBagajKategori = Entity.SBagajKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            //ViewBag.SinavSorular = Entity.BaslikMaddes.Where(t => t.KategoriMadde_Ref == 1007).ToList();
            ViewBag.SinavTipi = Entity.SinavTipis.ToList();
            return View();
        }

        public ActionResult SoruGuncelle(int id)
        {
            ViewBag.UniteModuller = Entity.UniteModuls.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            //ViewBag.STMKategori = Entity.SBTehlikeliMKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToList(); ;
            //ViewBag.SBagajKategori = Entity.SBagajKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            //ViewBag.SinavSorular = Entity.BaslikMaddes.Where(t => t.KategoriMadde_Ref == 1007).ToList();
            ViewBag.SinavTipi = Entity.SinavTipis.ToList();
            var data = Entity.UniteSorulars.Where(g => g.ID == id && g.IsActive == true).FirstOrDefault();
            return View(data);
        }

        [HttpPost]
        public ActionResult SoruKaydet(int SinavTipi_Ref, int? Unite_Ref, string Soru, List<string> SoruSikki, List<string> CevapIcerik, List<bool> DogruSecenek)
        {

            UniteSorular soruDetay = new UniteSorular();
            soruDetay.IsActive = true;
            soruDetay.IsDelete = false;

            soruDetay.Soru = Soru;
            soruDetay.Unite_Ref = Unite_Ref.Value;
            soruDetay.SinavTipi_Ref = SinavTipi_Ref;

            Entity.UniteSorulars.Add(soruDetay);
            Entity.SaveChanges();

            if (SoruSikki != null)
            {
                var tSoruSikki = SoruSikki.Select((z, Index) => new { Name = z, Ix = Index });
                var tCevapIcerik = CevapIcerik.Select((z, Index) => new { Name = z, Ix = Index });
                var tDogruSecenek = DogruSecenek.Select((z, Index) => new { Name = z, Ix = Index });
                var Sorular = from SoruSikkix in tSoruSikki
                              join CevapIcerikx in tCevapIcerik on SoruSikkix.Ix equals CevapIcerikx.Ix
                              join DogruSecenekx in tDogruSecenek on SoruSikkix.Ix equals DogruSecenekx.Ix
                              select new SoruSiklari
                              {
                                  IsActive = true,
                                  IsDelete = false,
                                  CevapIcerik = CevapIcerikx.Name,
                                  UniteSorular_Ref = soruDetay.ID,
                                  DogruSecenek = DogruSecenekx.Name,
                                  SoruSikki = SoruSikkix.Name
                              };
                foreach (var itemSoru in Sorular)
                {
                    Entity.SoruSiklaris.Add(itemSoru);
                }
                Entity.SaveChanges();
            }

            return RedirectToAction("YeniSoruTanimlama", "SinavIslemler", new { area = "Panel" });
        }

        public JsonResult SoruAktifPasif(int ID, bool aktif)
        {
            var data = Entity.UniteSorulars.Where(t => t.ID == ID).FirstOrDefault();
            data.IsActive = aktif;
            Entity.SaveChanges();
            return Json(null, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SoruGuncelleGenel(int ID, int? Unite_Ref, string Soru)
        {
            bool kaydet = false;
            UniteSorular soruDetay = Entity.UniteSorulars.Where(r => r.ID == ID).First();
            soruDetay.Soru = Soru;
            soruDetay.Unite_Ref = Unite_Ref == null ? 0 : Unite_Ref.Value;
            try
            {
                Entity.SaveChanges();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            Entity.SaveChanges();

            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SoruGuncelleSiklar(int ID, string SoruSikki, string CevapIcerik, bool DogruSecenek)
        {
            bool kaydet = false;
            SoruSiklari detay = Entity.SoruSiklaris.Where(t => t.ID == ID).First();
            detay.CevapIcerik = CevapIcerik;
            detay.DogruSecenek = DogruSecenek;
            detay.SoruSikki = SoruSikki;
            try
            {
                Entity.SaveChanges();

                List<int> idx = Entity.SoruSiklaris.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true).Select(y => y.ID).ToList();
                foreach (var item in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    SoruSiklari detay2 = Entity.SoruSiklaris.Where(t => t.ID == item.value).First();
                    detay2.DogruSecenek = false;
                    if (detay2.ID == detay.ID)
                    {
                        detay2.DogruSecenek = DogruSecenek;
                    }
                    Entity.SaveChanges();
                }
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }


            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SoruEkleSiklar(int UniteSorular_Ref, string SoruSikki, string CevapIcerik, bool DogruSecenek)
        {
            bool kaydet = false;
            SoruSiklari detay = new SoruSiklari();
            detay.IsActive = true;
            detay.IsDelete = false;
            detay.UniteSorular_Ref = UniteSorular_Ref;
            detay.CevapIcerik = CevapIcerik;
            detay.DogruSecenek = DogruSecenek;
            detay.SoruSikki = SoruSikki;
            try
            {
                Entity.SoruSiklaris.Add(detay);
                Entity.SaveChanges();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SoruSiklarSil(int ID)
        {
            string kaydet = "";
            SoruSiklari detay = Entity.SoruSiklaris.Where(t => t.ID == ID).First();
            detay.IsActive = false;
            detay.IsDelete = true;
            detay.DogruSecenek = false;
            try
            {
                Entity.SaveChanges();
                string[] siklar = { "a)", "b)", "c)", "d)", "e)", "f)", "g)" };
                List<int> idx = Entity.SoruSiklaris.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true && t.IsDelete == false).Select(y => y.ID).ToList();
                foreach (var item in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    SoruSiklari detay2 = Entity.SoruSiklaris.Where(t => t.ID == item.value && t.IsActive == true && t.IsDelete == false).FirstOrDefault();
                    if (detay2 != null)
                    {
                        detay2.DogruSecenek = false;
                        if (item.Index == 0)
                        {
                            detay2.DogruSecenek = true;
                        }
                        Entity.SaveChanges();
                    }

                }
                foreach (var itemCevap in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    SoruSiklari detay2Cevap = Entity.SoruSiklaris.Where(t => t.ID == itemCevap.value && t.IsActive == true && t.IsDelete == false).FirstOrDefault();
                    if (detay2Cevap != null)
                    {
                        detay2Cevap.SoruSikki = siklar[itemCevap.Index];
                        Entity.SaveChanges();
                    }
                }

                kaydet = "Soru Sikki Başarı ile güncellendi";
            }
            catch
            {
                kaydet = "Güncelleme Sırasında bir hata oluştu  tekrar deneyiniz! (Soru Şıkkı)";
            }


            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SoruEkleAltSiklar(int UniteSorular_Ref, string AltSoruSikki, string AltCevapIcerik, bool AltDogruSecenek)
        {
            bool kaydet = false;
            AltSiklar detay = new AltSiklar();
            detay.IsActive = true;
            detay.IsDelete = false;
            detay.UniteSorular_Ref = UniteSorular_Ref;
            detay.AltCevapIcerik = AltCevapIcerik;
            detay.AltDogruSecenek = AltDogruSecenek;
            detay.AltSoruSikki = AltSoruSikki;
            try
            {
                Entity.AltSiklars.Add(detay);
                Entity.SaveChanges();
                kaydet = true;
                //kaydet += detay.ID;
                //kaydet += ",Alt Soru Sikki Başarı ile Eklendi";
            }
            catch
            {
                kaydet = false;
            }


            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SoruGuncelleAltSiklar(int ID, string AltSoruSikki, string AltCevapIcerik, bool AltDogruSecenek)
        {
            string kaydet = "";
            AltSiklar detay = Entity.AltSiklars.Where(t => t.ID == ID).First();
            detay.AltCevapIcerik = AltCevapIcerik;
            detay.AltDogruSecenek = AltDogruSecenek;
            detay.AltSoruSikki = AltSoruSikki;
            try
            {
                Entity.SaveChanges();

                List<int> idx = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref).Select(y => y.ID).ToList(); ;
                foreach (var item in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    AltSiklar detay2 = Entity.AltSiklars.Where(t => t.ID == item.value).First();
                    detay2.AltDogruSecenek = false;
                    if (detay2.ID == detay.ID)
                    {
                        detay2.AltDogruSecenek = AltDogruSecenek;
                    }
                    Entity.SaveChanges();
                }
                kaydet = "Soru Sikki Başarı ile güncellendi";
            }
            catch
            {
                kaydet = "Güncelleme Sırasında bir hata oluştu  tekrar deneyiniz! (Soru Şıkkı)";
            }


            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AltSoruSikkiGuncelle(int ID)
        {
            bool kaydet = false;
            AltSiklar detay = Entity.AltSiklars.Where(t => t.ID == ID && t.IsActive == true && t.IsDelete == false).First();
            try
            {
                var detay2 = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true && t.IsDelete == false).ToList();
                foreach (var item in detay2)
                {
                    item.AltDogruSecenek = false;
                    Entity.SaveChanges();
                }
                detay.AltDogruSecenek = true;
                Entity.SaveChanges();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SoruSikkiGuncelle(int ID)
        {
            bool kaydet = false;
            SoruSiklari detay = Entity.SoruSiklaris.Where(t => t.ID == ID).First();
            try
            {
                var detay2 = Entity.SoruSiklaris.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true && t.IsDelete == false).ToList();
                foreach (var item in detay2)
                {
                    item.DogruSecenek = false;
                    Entity.SaveChanges();
                }
                detay.DogruSecenek = true;
                Entity.SaveChanges();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TanimliSorular(int? soruTip)
        {
            ViewBag.SinavTipi = Entity.SinavTipis.ToList();
            int soruTipIndex = soruTip.HasValue ? Convert.ToInt32(soruTip) : -1;

            ViewBag.Sinavlar = Entity.Sinavlars.Where(t => t.IsActive == true).ToList();
            ViewBag.Uniteler = Entity.Unites.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            List<UniteSorular> list = null;
            if (soruTipIndex == 0)
            {
                list = Entity.UniteSorulars.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            }
            else if (soruTipIndex == -1)
            {
                list = new List<UniteSorular>();
            }
            else
            {
                list = Entity.UniteSorulars.Where(t => t.IsActive == true && t.IsDelete == false && t.SinavTipi_Ref == soruTipIndex).ToList();
            }
            ViewBag.SinavTipDn = soruTipIndex;
            return View(list);
        }

        public ActionResult TSoruGuncelle(int id)
        {
            ViewBag.Uniteler = Entity.Unites.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            UniteSorular data = Entity.UniteSorulars.Where(t => t.ID == id).FirstOrDefault();
            //ViewBag.usiklar = Entity.SoruSiklaris.Where(r => r.UniteSorular_Ref == data.ID && r.IsActive == true).Select(t => new SoruSiklariLocal { ID = t.ID, SoruSikki = t.SoruSikki, CevapIcerik = t.CevapIcerik, DogruSecenek = t.DogruSecenek }).ToList();
            //ViewBag.altSiklar = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == data.ID && t.IsActive == true).ToList();

            return View(data);
        }

        public ActionResult SoruIcerik(int id)
        {
            ViewBag.list = Entity.UniteSorulars.Where(t => t.Unite_Ref == id).Select(r => new { r.ID, r.IsActive, r.SinavTipi_Ref, r.Soru }).ToList();
            return View();

        }

        public ActionResult SinavSorular(int id)
        {
            ViewData["SinavTipi"] = Entity.SinavTipis.ToList();
            ViewData["Unite"] = Entity.Unites.Where(r => r.IsActive == true).ToList();
            var data = Entity.Sinavlars.Where(t => t.ID == id).FirstOrDefault();
            return View(data);
        }

        public ActionResult _USorular(int id, int tip, int sinavno)
        {
            List<UniteSorular> data = new List<UniteSorular>();
            if (tip == (int)Enumlar.SoruTipleri.TEST)
            {
                data = Entity.UniteSorulars.Where(t => t.Unite_Ref == id && (t.SinavTipi_Ref == tip || t.SinavTipi_Ref == (int)Enumlar.SoruTipleri.TEORIK)).ToList();
            }
            else
            {
                data = Entity.UniteSorulars.Where(t => t.Unite_Ref == id && t.SinavTipi_Ref == tip).ToList();
            }

            ViewBag.Sinavno = sinavno;
            ViewBag.Tip = tip;
            return PartialView("_UniteSorular", data);
        }

        public JsonResult SinavSoruKaydet(int SinavTipi_Ref, int UniteSorular_Ref, int Sinavlar_Ref)
        {
            bool durum = false;
            try
            {
                var data = new Sinav_Sorular();
                data.IsActive = true;
                data.IsDelete = false;
                data.Sinavlar_Ref = Sinavlar_Ref;
                data.SinavTipi_Ref = SinavTipi_Ref;
                data.UniteSorular_Ref = UniteSorular_Ref;
                Entity.Sinav_Sorular.Add(data);
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }


            //var tSinavTipi_Ref = SinavTipi_Ref.Select((t, Index) => new { Name = t, Ix = Index });
            //var tUniteSorular_Ref = UniteSorular_Ref.Select((t, Index) => new { Name = t, Ix = Index });
            //var tSinavlar_Ref = Sinavlar_Ref.Select((t, Index) => new { Name = t, Ix = Index });

            //var subDetail = from SinavTipifx in tSinavTipi_Ref
            //                join UniteSorulartx in tUniteSorular_Ref on SinavTipifx.Ix equals UniteSorulartx.Ix
            //                join Sinavlarfx in tSinavlar_Ref on SinavTipifx.Ix equals Sinavlarfx.Ix

            //                    select new Sinav_Sorular
            //                    {
            //                        IsActive = true,
            //                        IsDelete = false,
            //                         Sinavlar_Ref = Sinavlarfx.Name,
            //                         SinavTipi_Ref = SinavTipifx.Name,
            //                         UniteSorular_Ref = UniteSorulartx.Name
            //                    };
            //    foreach (var itemSinav in subDetail)
            //    {
            //        Entity.Sinav_Sorular.Add(itemSinav);                   
            //    }
            //    Entity.SaveChanges();

            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }

        public JsonResult TanimliSoruGuncelle(int id)
        {
            bool durum = false;
            try
            {
                var data = Entity.Sinav_Sorular.Where(t => t.ID == id).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Sınav İşlemler
        public ActionResult Sinavlar()
        {
            var data = Entity.Sinavlars.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            return View(data);
        }

        public JsonResult SinavlarGuncelle(int id, string SinavNo, string SoruSayisi, string Aciklama)
        {
            bool durum = false;
            try
            {
                var data = Entity.Sinavlars.Where(t => t.ID == id).FirstOrDefault();
                data.SinavNo = SinavNo;
                data.SoruSayisi = int.Parse(SoruSayisi);
                data.Aciklama = Aciklama;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SinavlarSil(int id)
        {
            bool durum = false;
            try
            {
                var data = Entity.Sinavlars.Where(t => t.ID == id).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SinavKaydet(string SinavNo, int SoruSayisi, string Aciklama)
        {
            var kontrol = Entity.Sinavlars.Where(y => y.SinavNo == SinavNo).Count();
            if (kontrol == 0)
            {
                Sinavlar data = new Sinavlar();
                data.IsActive = true;
                data.IsDelete = false;
                data.SinavNo = SinavNo;
                data.SoruSayisi = SoruSayisi;
                data.Aciklama = Aciklama;
                Entity.Sinavlars.Add(data);
                Entity.SaveChanges();
            }
            return RedirectToAction("Sinavlar", "SinavIslemler", new { area = "Panel" });
        }

        public JsonResult SinavSilme(int sinavID)
        {
            bool durum = false;
            try
            {
                var durumKontrol = Entity.Sinavlars.Where(t => t.IsActive == true && t.IsDelete == false && t.ID == sinavID && t.Personel_Sinavlar.Count == 0).FirstOrDefault();
                if (durumKontrol != null)
                {
                    durumKontrol.IsActive = false;
                    durumKontrol.IsDelete = true;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SinavDetay(int id)
        {
            var pcevap = Entity.SinavCevaplars.Where(y => y.Personel_Sinavlar_Ref == id).ToList();
            return View(pcevap);
        }

        public ActionResult SinavCevaplama()
        {
            ViewBag.Sinavlar = Entity.Sinavlars.Where(t => t.IsActive == true).ToList();
            ViewBag.Firmalar = Entity.Firmalars.Where(y => y.IsActive == true && y.IsDelete == false).ToList();

            return View();
        }

        public ActionResult _SinavCevapList(int firmaRef, string tcno, string adi, string soyadi)
        {
            DateTime Bugun = DateTime.Now.Date;           
            IQueryable<Personel_Sinavlar> data = Entity.Personel_Sinavlar.Where(t => t.IsActive == true && ((t.SinavTarihi <= Bugun && t.Bitti == 1) || (t.Basladi == 0 && t.Bitti == 0)));
            if (firmaRef != -2)
            {
                data = data.Where(t => t.Sinif_Ogrenci.Personel.Firma_Ref == firmaRef);
            }
            List<Personel_Sinavlar> bilgi = null;
            if (string.IsNullOrEmpty(adi) && string.IsNullOrEmpty(soyadi) && string.IsNullOrEmpty(tcno))
            {
                bilgi = data.ToList();
            }
            else if(string.IsNullOrEmpty(tcno))
            {
                bilgi = data.Where(q => q.Sinif_Ogrenci.Personel.Adi.Contains(adi) && q.Sinif_Ogrenci.Personel.Soyadi.Contains(soyadi)).ToList();
            }
            else
            {
                bilgi = data.Where(q => q.Sinif_Ogrenci.Personel.TCNo.Contains(tcno)).ToList();
            }
            //ViewBag.SinavID = sinavIndex;
            //ResimKlasorKontrol();
            return PartialView("_SinavCevaplarNo", bilgi);
        }

        public JsonResult SPKaydetData(List<String> SOgrenciID, int SinavID, string SinavTarih, string BaslamaSaati, string BitisSaati)
        {
            string durum = "Kayıt ekleme Hatalı";
            foreach (var item in SOgrenciID)
            {
                int idp = int.Parse(item);
                var controldd = Entity.Personel_Sinavlar.Where(r => r.Sinif_Ogrenci_Ref == idp && r.Sinavlar_Ref == SinavID).Count();
                if (controldd == 0)
                {
                    double surex = TimeSpan.Parse(BitisSaati).TotalMinutes - TimeSpan.Parse(BaslamaSaati).TotalMinutes;
                    int zaman = Convert.ToInt32(surex);
                    var data = new Personel_Sinavlar();
                    data.IsActive = true;
                    data.IsDelete = false;
                    data.Sinif_Ogrenci_Ref = idp;
                    data.Sinavlar_Ref = SinavID;
                    data.SinavAciklama = false;
                    data.SinavNotuUygulama = 0;
                    data.SinavNotuTeorik = 0;
                    data.Durum = "AÇIKLANMADI";
                    data.DogruSayisiUygulama = 0;
                    data.YanlisSayisiTeorik = 0;
                    data.UygulamaBos = 0;
                    data.YanlisSayisiUgulama = 0;
                    data.DogruSayisiTeorik = 0;
                    data.TeorikBos = 0;
                    DateTime starih = Convert.ToDateTime(SinavTarih);
                    data.SinavTarihi = Convert.ToDateTime(starih.ToShortDateString());
                    data.BaslamaSaati = TimeSpan.Parse(BaslamaSaati);
                    data.BitisSaati = TimeSpan.Parse(BitisSaati);
                    data.SinavSuresi = zaman;
                    data.Basladi = 0;
                    data.Bitti = 0;
                    data.GirisSaati = null;
                    data.CikisSaati = null;
                    Entity.Personel_Sinavlar.Add(data);
                    Entity.SaveChanges();
                    durum = "ekleme İşlemi Gerçekleşti";
                }
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        public ActionResult PersonelSinav()
        {
            ViewBag.Sinif = Entity.Sinifs.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            ViewBag.SinifOgrenci = Entity.Sinif_Ogrenci.ToList();
            ViewBag.Sinavlar = Entity.Sinavlars.Where(y => y.IsActive == true).ToList();
            return View();
        }

        public JsonResult SPsil(int id)
        {
            string durum = "";
            try
            {
                var data = Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefault();
                bool kontrol = data.Basladi == 1 ? false : true;
                if (kontrol)
                {
                    data.IsActive = false;
                    data.IsDelete = true;
                    Entity.SaveChanges();
                    durum = "true";
                }
            }
            catch
            {
                durum = "false";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SinavSonuclandirma(List<String> postData)
        {
            bool durum = false;
            try
            {
                foreach (var item in postData)
                {
                    List<SinavSonuclari> cevaplar = new List<SinavSonuclari>();
                    int id = int.Parse(item);
                    int sinavEtiket = Entity.Personel_Sinavlar.Where(t => t.ID == id && t.IsActive == true && t.IsDelete == false).Select(y => y.Sinavlar_Ref).FirstOrDefault();
                    var sinavSorular = Entity.Sinav_Sorular.Where(r => r.Sinavlar_Ref == sinavEtiket && r.IsActive == true && r.IsDelete == false).ToList();
                    foreach (var item2 in sinavSorular)
                    {
                        var cevapkontrol = item2.SinavCevaplars.Where(r => r.Personel_Sinavlar_Ref == id).Select(t => new { t.Netice, t.Sınav_Sorular_Ref }).FirstOrDefault();

                        string cevabi = "";
                        if (cevapkontrol == null)
                        {
                            cevabi = "YOK";
                        }
                        else
                        {
                            if (cevapkontrol.Netice == true)
                            {
                                cevabi = "True";
                            }
                            else
                            {
                                cevabi = "False";
                            }
                        }
                        SinavSonuclari datax = new SinavSonuclari();
                        datax.PerID = id;
                        datax.SoruID = item2.ID;
                        datax.Cevap = cevabi;
                        datax.SoruTipi = item2.SinavTipi_Ref;
                        cevaplar.Add(datax);
                    }

                    int TeorikDogruSayisi = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "True").Count();
                    int TeorikYanlisSayisi = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "False").Count();
                    int TeorikBos = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "YOK").Count();

                    decimal TamPuan = 100;

                    int ToplamSoruTeorik = sinavSorular.Where(y => y.SinavTipi_Ref == 2).Count();
                    decimal soruPuanTeorik = 0;
                    if (ToplamSoruTeorik != 0)
                    {
                        soruPuanTeorik = TamPuan / ToplamSoruTeorik;
                    }
                    decimal SinavNotuTeorik = soruPuanTeorik * TeorikDogruSayisi;

                    string Netice = "";
                    int teorikSoruVarmi = sinavSorular.Where(e => e.SinavTipi_Ref == (int)Enumlar.SoruTipleri.TEORIK).Count();


                    if (SinavNotuTeorik >= 70)
                    {
                        Netice = "GEÇTİ";
                    }
                    else
                    {
                        Netice = "KALDI";
                    }



                    var perSinavIsleme = Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefault();
                    perSinavIsleme.DogruSayisiTeorik = TeorikDogruSayisi;
                    perSinavIsleme.YanlisSayisiTeorik = TeorikYanlisSayisi;
                    perSinavIsleme.SinavNotuTeorik = SinavNotuTeorik;
                    perSinavIsleme.TeorikBos = TeorikBos;
                    perSinavIsleme.Durum = Netice;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SinavSonuclandirmaTekrar(int id)
        {
            string durum = string.Empty;
            try
            {
                using (Heas_UzakEntities Entity = new Heas_UzakEntities())
                {
                    List<SinavSonuclari> cevaplar = new List<SinavSonuclari>();
                    Personel_Sinavlar perSinavIsleme = Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefault();
                    List<Sinav_Sorular> sinavSorular = Entity.Sinav_Sorular.Where(r => r.Sinavlar_Ref == perSinavIsleme.Sinavlar_Ref && r.IsActive == true && r.IsDelete == false).ToList();

                    foreach (var item2 in sinavSorular)
                    {
                        var cevapkontrol = item2.SinavCevaplars.Where(r => r.Personel_Sinavlar_Ref == id).Select(t => new { t.Netice, t.Sınav_Sorular_Ref }).FirstOrDefault();

                        string cevabi = "";
                        if (cevapkontrol == null)
                        {
                            cevabi = "YOK";
                        }
                        else
                        {
                            if (cevapkontrol.Netice == true)
                            {
                                cevabi = "True";
                            }
                            else
                            {
                                cevabi = "False";
                            }
                        }
                        SinavSonuclari datax = new SinavSonuclari();
                        datax.PerID = id;
                        datax.SoruID = item2.ID;
                        datax.Cevap = cevabi;
                        datax.SoruTipi = item2.SinavTipi_Ref;
                        cevaplar.Add(datax);
                    }

                    int TeorikDogruSayisi = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "True").Count();
                    int TeorikYanlisSayisi = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "False").Count();
                    int TeorikBos = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "YOK").Count();

                    decimal TamPuan = 100;

                    int ToplamSoruTeorik = sinavSorular.Where(y => y.SinavTipi_Ref == 2).Count();
                    decimal soruPuanTeorik = 0;
                    if (ToplamSoruTeorik != 0)
                    {
                        soruPuanTeorik = TamPuan / ToplamSoruTeorik;
                    }
                    decimal SinavNotuTeorik = soruPuanTeorik * TeorikDogruSayisi;
                                        
                    string Netice = string.Empty;
                    //List<ResultDetail> sonuc = ResimKontrolDogrulama.BenzerlikOranlar(perSinavIsleme.KlasorYolu, perSinavIsleme.Sinif_Ogrenci.Personel.TCNo);
                    //if (sonuc.Where(r => r.statu == false).Count() > 0)
                    //{
                    //    Netice = "KALDI";
                    //    perSinavIsleme.SertifikaNo = "SINAV KURALLARINA UYULMADI";
                    //}
                    //else
                    //{
                        if (SinavNotuTeorik >= 70)
                        {
                            Netice = "GEÇTİ";
                            int user = AktifKullanici.Aktif.User_Ref;
                            int buyil = DateTime.Now.Date.Year;
                            int oncekiyilkontrol = buyil - 1;
                            int aykontrol = DateTime.Now.Date.Month;
                            IQueryable<Personel_Sinavlar> personelSinav = Entity.Personel_Sinavlar;
                            Personel_Sinavlar personeOncekiYilSinav = personelSinav.Where(e => e.Sinif_Ogrenci.Personel_Ref == user &&
                               e.SinavTarihi.Value.Year == oncekiyilkontrol &&
                               e.SinavTarihi.Value.Month == aykontrol
                               ).FirstOrDefault();

                            string psw = "SGHL/MİA/HEAŞ/UZP/";
                            string seri = "";
                            string adet = "";
                            int buYilSinavSayisi = personelSinav.Where(w => w.SinavTarihi.Value.Year == buyil).Select(q => q.ID).Count();
                            if (personeOncekiYilSinav != null)
                            {
                                seri = buyil + "B";
                            }
                            else
                            {
                                seri = buyil + "T";
                            }
                            adet = "000" + (buYilSinavSayisi < 9 ? "00" + buYilSinavSayisi.ToString() : buYilSinavSayisi.ToString());

                            psw += seri + "/" + adet;
                            perSinavIsleme.SertifikaNo = psw;
                        }
                        else
                        {
                            Netice = "KALDI";
                            perSinavIsleme.SertifikaNo = "TEKRAR SINAVA GİRECEK";
                        }
                    //}
                    perSinavIsleme.Basladi = 1;
                    perSinavIsleme.Bitti = 1;
                    perSinavIsleme.CikisSaati = TimeSpan.Parse(DateTime.Now.ToLongTimeString());
                    perSinavIsleme.DogruSayisiTeorik = TeorikDogruSayisi;
                    perSinavIsleme.YanlisSayisiTeorik = TeorikYanlisSayisi;
                    perSinavIsleme.SinavNotuTeorik = SinavNotuTeorik;
                    perSinavIsleme.TeorikBos = TeorikBos;
                    perSinavIsleme.SinavAciklama = true;
                    perSinavIsleme.DuyuruTarihi = DateTime.Now.Date;
                    perSinavIsleme.Durum = Netice;
                    int sonucss = Entity.SaveChanges();

                    //if (SinavNotuTeorik >= 70)
                    //{
                    //    durum = SonucGonder(perSinavIsleme/*, resimler*/);
                    //}

                    //List<string> adres = new List<string>();
                    //adres.Add(perSinavIsleme.Sinif_Ogrenci.Personel.Firmalar.Email);

                    //string firma = perSinavIsleme.Sinif_Ogrenci.Personel.Firmalar.FirmaAdi;
                    //string kisi = perSinavIsleme.Sinif_Ogrenci.Personel.Adi + " " + perSinavIsleme.Sinif_Ogrenci.Personel.Soyadi;

                    //string body = "Sayın <I>" + firma.ToUpper() + " Yetkilisi; </I> <br />";
                    //string sinavsonuc = SinavNotuTeorik >= 70 ? "BAŞARILI" : "BAŞARISIZ";
                    //body += kisi + " isimli personelinizin girmiş olduğu Kurs1 Eğitim Sınav sonucunda <b>[" + sinavsonuc + "] olmuştur.</b> dir. <br />";
                    //body += "Bu mesaj bilgilendirme amacıyla tarfınıza gönderilmiştir.";

                    //try
                    //{
                    //    Haveak_Pro.Models.MailHelper.SendMail(adres, "Personel 'Kurs1' sınav sonuc bildirimi", body);
                    //}
                    //catch
                    //{
                    //    durum = "E-Posta bilgisi alıcılara gönderilmedi!";
                    //}
                }
            }
            catch (Exception ex)
            {
                durum = ex.Message;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DuyuruGonder(int id)
        {
            bool durum = false;
            //string[] resimler = null;
            //List<string> Rsmler = null;
            try
            {
                var item = Entity.Personel_Sinavlar.Where(t => t.Durum != "AÇIKLANMADI" && t.ID == id).FirstOrDefault();
                item.SinavAciklama = true;
                item.DuyuruTarihi = DateTime.Now.Date;
                try
                {
                    //string path = Server.MapPath("~/DocumentFiles/SinavResim/" + item.KlasorYolu);
                    //string[] filePaths = Directory.GetFiles(path);
                    //resimler = new string[filePaths.Length];
                    //Rsmler = new List<string>();
                    //foreach (var adres in filePaths)
                    //{
                    //    using (System.Drawing.Image image = System.Drawing.Image.FromFile(adres))
                    //    {
                    //        using (MemoryStream m = new MemoryStream())
                    //        {
                    //            image.Save(m, image.RawFormat);
                    //            byte[] imageBytes = m.ToArray();
                    //            // Convert byte[] to Base64 String
                    //            string base64String = Convert.ToBase64String(imageBytes);
                    //            Rsmler.Add(base64String);
                    //        }
                    //    }
                    //}
                    //if (filePaths.Count() == 0)
                    //{
                    //    Rsmler = new List<string>();
                    //    resimler = new string[2];
                    //    Rsmler.Add("resimyok1.jpg");
                    //    Rsmler.Add("resimyok2.jpg");
                    //    for (int i = 0; i < Rsmler.Count; i++)
                    //    {
                    //        resimler[i] = Rsmler[i];
                    //    }
                    //}
                }
                catch
                {
                    //Rsmler = new List<string>();
                    //resimler = new string[2];
                    //Rsmler.Add("resimyok1.jpg");
                    //Rsmler.Add("resimyok2.jpg");
                    //for (int i = 0; i < Rsmler.Count; i++)
                    //{
                    //    resimler[i] = Rsmler[i];
                    //}
                }

                //WsPersonelSinavSonuc.heasEgitimServiceInput inp = new WsPersonelSinavSonuc.heasEgitimServiceInput()
                //{
                //    egitimSonucu = 1,
                //    kimlikNo = item.Sinif_Ogrenci.Personel.TCNo,
                //    referansId = item.Sinif_Ogrenci.AktifPersonelId.Value.ToString(),
                //    resimler = resimler,
                //    sertifikaNo = item.SertifikaNo
                //};
                //WsPersonelSinavSonuc.HeasEgitimClient wservis = new WsPersonelSinavSonuc.HeasEgitimClient();
                //WsPersonelSinavSonuc.heasServiceOutput sonuc = wservis.SinavSonuc(inp);
                DataSerialize dataYeni = new DataSerialize
                {
                    kimlikNo = item.Sinif_Ogrenci.Personel.TCNo,
                    egitimSonucu = item.SinavNotuTeorik >= 70 ? 1 : 0,
                    referansId = item.Sinif_Ogrenci.AktifPersonelId.Value.ToString(),
                    //resimler = resimler,
                    sertifikaNo = item.SertifikaNo,
                    sno = item.ID.ToString()
                };
                KartAktarimRest.POSTHeas(dataYeni);
                durum = true;
                Entity.SaveChanges();

            }
            catch
            {
                durum = false;
            }

            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult _SinavaAtananPersonel(int id)
        {
            var data = Entity.Personel_Sinavlar.Where(e => e.IsActive == true && e.IsDelete == false && e.Sinavlar_Ref == id).ToList();
            return PartialView("_PersonelSinavlar", data);
        }

        public ActionResult _SinifOgrenci(int sinifID)
        {
            DateTime zaman = DateTime.Now.Date;
            var list =  Entity.Sinif_Ogrenci.Where(t => t.Sinif_Ref == sinifID && t.BitisTarihi >= zaman).ToList();
            //var sonlst = list.Where(t=>t.Personel.Kullanicis.Where(r=>r.BitisTarihi.Value)).FirstOrDefault();
            //foreach (var item in list)
            //{                
            //    int kontrol = item.Personel.Kullanicis.Where(y=>y.BitisTarihi >= zaman).Count();
            //    if (kontrol != 0)
            //    {

            //    }
            //}


            return PartialView("_SinifOgrenciler", list);
        }

        public JsonResult AtanmisPersonelCikarma(int sinavID)
        {
            var list = Entity.Personel_Sinavlar.Where(t => t.Sinavlar_Ref == sinavID && t.IsActive == true).Select(y => y.Sinif_Ogrenci.Personel_Ref).ToList();
            return Json(list, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SinavDokum(string baslangic, string bitis)
        {
            Tarihler data = null;
            try
            {
                if (baslangic != null)
                {
                    data = new Tarihler();
                    data.Baslangic = Convert.ToDateTime(baslangic);
                    data.Bitis = Convert.ToDateTime(bitis);
                }
            }
            catch
            {
                data = null;
            }
            return View(data);
        }

        public JsonResult Capture_Delete(string folder)
        {
            bool durum = false;
            try
            {
                var pathf = Server.MapPath("~/DocumentFiles/SinavResim/" + folder);
                Directory.Delete(pathf, true);
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Sınav Tipi İşlemleri

        public ActionResult SinavTip()
        {
            var data = Entity.SinavTipis.ToList();
            return View(data);
        }
        [HttpPost]
        public ActionResult SinavTipiEkle(string Baslik)
        {
            SinavTipi blm = new SinavTipi();
            blm.Baslik = Baslik;
            Entity.SinavTipis.Add(blm);
            Entity.SaveChanges();
            return RedirectToAction("SinavTip", "SinavIslemler", new { area = "Panel" });
        }

        public JsonResult SinavTipiGuncelle(int id, string Baslik)
        {
            bool durum = false;
            try
            {
                SinavTipi blm = Entity.SinavTipis.Where(t => t.ID == id).FirstOrDefault();
                blm.Baslik = Baslik;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Test Tanımlama
        public ActionResult Testler()
        {
            ViewBag.Moduller = Entity.UniteModuls.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            var data = Entity.Testlers.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            return View(data);
        }

        public JsonResult TestlerGuncelle(int id, string TestNo, string SoruSayisi, int Sure, string Aciklama, int UniteModul_Ref)
        {
            bool durum = false;
            try
            {
                var kontrol = Entity.Testlers.Where(y => y.TestNo == TestNo && y.UniteModul_Ref == UniteModul_Ref).Count();
                if (kontrol == 0)
                {
                    var data = Entity.Testlers.Where(t => t.ID == id).FirstOrDefault();
                    data.TestNo = TestNo;
                    data.SoruSayisi = int.Parse(SoruSayisi);
                    data.Sure = Sure;
                    data.Aciklama = Aciklama;
                    data.UniteModul_Ref = UniteModul_Ref;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TestSilme(int id)
        {
            bool durum = false;
            try
            {
                var durumKontrol = Entity.Testlers.Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id && t.Personel_Testler.Count == 0).FirstOrDefault();
                if (durumKontrol != null)
                {
                    durumKontrol.IsActive = false;
                    durumKontrol.IsDelete = true;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TestKaydet(string TestNo, int SoruSayisi, int Sure, string Aciklama, int UniteModul_Ref)
        {
            var kontrol = Entity.Testlers.Where(y => y.TestNo == TestNo && y.UniteModul_Ref == UniteModul_Ref).Count();
            if (kontrol == 0)
            {
                Testler data = new Testler();
                data.IsActive = true;
                data.IsDelete = false;
                data.TestNo = TestNo;
                data.SoruSayisi = SoruSayisi;
                data.Sure = Sure;
                data.Aciklama = Aciklama;
                data.UniteModul_Ref = UniteModul_Ref;
                Entity.Testlers.Add(data);
                Entity.SaveChanges();
            }
            return RedirectToAction("Testler", "SinavIslemler", new { area = "Panel" });
        }

        public ActionResult TestSorular(int id, int UniteModul_Ref)
        {
            ViewData["Unite"] = Entity.Unites.Where(r => r.IsActive == true && r.Modul_Ref == UniteModul_Ref).ToList();
            ViewBag.UniteModulRef = UniteModul_Ref;
            var data = Entity.Testlers.Where(t => t.ID == id).FirstOrDefault();
            return View(data);
        }

        public JsonResult TestKursModulUniteler(int id)
        {
            var data = Entity.Unites.Where(t => t.IsActive == true && t.IsDelete == false && t.Modul_Ref == id).Select(r => new { r.ID, r.UniteAdi }).ToList();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TestSoruKaydet(int Test_Ref, int UniteSorular_Ref)
        {
            bool durum = false;
            try
            {
                var kontrol = Entity.Test_Sorular.Where(r => r.Testler_Ref == Test_Ref && r.UniteSorular_Ref == UniteSorular_Ref).FirstOrDefault();
                if (kontrol == null)
                {
                    var data = new Test_Sorular();
                    data.IsActive = true;
                    data.IsDelete = false;
                    data.Testler_Ref = Test_Ref;
                    data.UniteSorular_Ref = UniteSorular_Ref;
                    Entity.Test_Sorular.Add(data);
                    Entity.SaveChanges();
                }
                else
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    Entity.SaveChanges();
                }
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }


        public JsonResult TanimliSoruGuncelleTest(int id)
        {
            bool durum = false;
            try
            {
                var data = Entity.Test_Sorular.Where(t => t.ID == id).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Global İşlemler
        protected string savedocument(HttpPostedFileBase document)
        {
            if (document != null)
            {
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid() + f.Name;
                string path = "Areas/Sinavlar/Images/" + Fname;
                document.SaveAs(Server.MapPath("~/" + path));
                return path;
            }
            else
            {
                return "";
            }

        }

        public string ServerTime()
        {
            return DateTime.Now.ToString();
        }

        void ResimKlasorKontrol()
        {
            DateTime bugun = DateTime.Now.Date;
            var sinavlar = Entity.Personel_Sinavlar.Where(q => q.SinavTarihi < bugun).ToList();
            if (sinavlar.Count > 0)
            {
                foreach (var klasor in sinavlar)
                {
                    try
                    {
                        string yol = klasor.KlasorYolu;
                        Personel_Sinavlar klasoryol = klasor;
                        var pathf = Server.MapPath("~/DocumentFiles/SinavResim/" + yol);
                        Directory.Delete(pathf, true);
                        klasoryol.KlasorYolu = "-";
                    }
                    catch
                    {
                    }
                }
                Entity.SaveChanges();
            }
        }

        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }
        #endregion
    }

    #region Parametreler
    public class SinavSonuclari : IDisposable
    {
        public int PerID { get; set; }
        public int SoruID { get; set; }
        public string Cevap { get; set; }
        public int SoruTipi { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class SoruSiklariLocal : IDisposable
    {

        public int ID { get; set; }
        public string SoruSikki { get; set; }
        public string CevapIcerik { get; set; }
        public bool DogruSecenek { get; set; }


        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class ResimIslem : IDisposable
    {
        public int? ID { get; set; }
        public string Baslik { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}

public class Tarihler : IDisposable
{
    public DateTime Baslangic { get; set; }
    public DateTime Bitis { get; set; }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
