﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Configuration;
using System.Web.SessionState;
using System.Runtime;


namespace Haveak_Pro.Areas.SINAV.Models
{
    [Serializable]
    public class CacheEntityTIP
    {
        public int ID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string TipNo { get; set; }
        public int Personel_Ref { get; set; }
        public int TipSinavlar_Ref { get; set; }
        public decimal TipNotu { get; set; }
        public Nullable<int> DogruSayisi { get; set; }
        public Nullable<int> YanlisSayisi { get; set; }
        public Nullable<System.DateTime> TipTarihi { get; set; }
        public Nullable<System.TimeSpan> GirisSaati { get; set; }
        public Nullable<System.TimeSpan> CikisSaati { get; set; }
        public Nullable<int> TipSuresi { get; set; }
        public Nullable<int> Basladi { get; set; }
        public int Bitti { get; set; }

        private List<CacheEntityTIP> dta;
        public List<CacheEntityTIP> DataTIP
        {
            get { return dta; }
            set { dta = value; }
        }
    }

    public class CacheFunctionTIP
    {
        public object DataTIPCache(List<CacheEntityTIP> data)
        {
            if (HttpContext.Current.Session["TIPSinavSorular"] != null)
            {
                HttpContext.Current.Session["TIPSınavSorular"] = null;
                HttpContext.Current.Session.Add("TIPSinavSorular", data);
            }
            else
            {
                HttpContext.Current.Session.Add("TIPSinavSorular", data);
            }
            object dsa = HttpContext.Current.Session["TIPSinavSorular"];
            return dsa;
        }


    }
}