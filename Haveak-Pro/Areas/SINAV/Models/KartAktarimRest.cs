﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace Haveak_Pro.Areas.SINAV.Models
{
    public class KartAktarimRest
    {
        public static string POSTHeas(DataSerialize datax)
        {
           
            string url = "https://hkts.sgairport.com/HeasRest/api/rest/egitim/sinavsonuc"; 

            //string data = "{\"referansId\":\"1\",\"kimlikNo\":\"1234\",\"sertifikaNo\":\"sno: abc1234\",\"egitimSonucu\":1,\"resimler\":[\"123\",\"456\"]}";
            string data = JsonConvert.SerializeObject(datax);
            bool isJsonText = true;

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            System.Net.HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            //request.KeepAlive = true;
            //request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.None;
            request.Headers.Add("Authorization", "Basic aGVhc2VnaXRpbTpoZTEyMw==");

            try
            {
                string postData = data;
                DataContractJsonSerializer ser = new DataContractJsonSerializer(data.GetType());
                StreamWriter writer = new StreamWriter(request.GetRequestStream());

                if (!isJsonText)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    postData = serializer.Serialize(data);
                }

                writer.Write(postData);
                writer.Close();

                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }

            }
            catch (WebException ex)
            {
                if (ex.Response == null) throw ex;
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    return errorText;
                }
            }
        }
    }
}