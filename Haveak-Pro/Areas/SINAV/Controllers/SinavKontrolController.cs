﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;
using System.Drawing;
using System.Drawing.Imaging;
using Haveak_Pro.WsPersonelSinavSonuc;
using Haveak_Pro.Areas.SINAV.Models;

namespace Haveak_Pro.Areas.SINAV.Controllers
{
    public class SinavKontrolController : Controller
    {
        public JsonResult Capture(string imagePath, string sinavno, int per_sinav)
        {
            bool durum = false;
            try
            {
                var kullanici = AktifKullanici.Aktif;
                using (Heas_UzakEntities db = new Heas_UzakEntities())
                {
                    string randeger = Guid.NewGuid().ToString().Split('-')[0];
                    Personel_Sinavlar klasor = db.Personel_Sinavlar.Where(e => e.ID == per_sinav).FirstOrDefault();
                    if (string.IsNullOrEmpty(klasor.KlasorYolu) || !Directory.Exists(Server.MapPath("~/DocumentFiles/SinavResim/" + klasor.KlasorYolu)))
                    {
                        var pathf = Server.MapPath("~/DocumentFiles/SinavResim/" + randeger + "-" + sinavno + "-" + kullanici.EName + kullanici.ESurName);
                        Directory.CreateDirectory(pathf);
                        klasor.KlasorYolu = randeger + "-" + sinavno + "-" + kullanici.EName + kullanici.ESurName;
                        db.SaveChanges();
                        string tcno = klasor.Sinif_Ogrenci.Personel.TCNo;
                        var orjImgPath = Server.MapPath("~/DocumentFiles/PersonelWebResim/" + tcno + ".jpg");
                        System.IO.File.Copy(orjImgPath, pathf);
                    }
                    DateTime nm = DateTime.Now;
                    string date = nm.ToString("hmmss");
                    var path = Server.MapPath("~/DocumentFiles/SinavResim/" + klasor.KlasorYolu + "/" + date + "-" + kullanici.EName + kullanici.ESurName + ".jpg");

                    Image resim = Base64ToImage(imagePath);
                    resim.Save(path, ImageFormat.Jpeg);
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum);
        }

        protected Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }
                    
    }
    public class SinavSonuclari : IDisposable
    {
        public int PerID { get; set; }
        public int SoruID { get; set; }
        public string Cevap { get; set; }
        public int SoruTipi { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    //public class WsSinavSonucu : IDisposable
    //{
    //    public Boolean egitimSonucu { get; set; }
    //    public String kimlikNo { get; set; }
    //    public String referansId { get; set; }
    //    public String[] resimler { get; set; }
    //    public String sertifikaNo { get; set; }

    //    public void Dispose()
    //    {
    //        GC.SuppressFinalize(this);
    //    }
    //}
}
