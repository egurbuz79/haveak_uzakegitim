﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;
using Haveak_Pro.Areas.SINAV.Models;
using System.IO;

namespace Haveak_Pro.Areas.SINAV.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /SINAV/Home/

        Heas_UzakEntities Entity = new Heas_UzakEntities();
        int user = AktifKullanici.Aktif.User_Ref;

        #region Sınav İşlemler
        public ActionResult Index()
        {
            ViewBag.Duyurular = Entity.Duyurulars.Where(t => t.IsDelete == false && t.IsActive == true && t.Tipi == 2).ToList();
            ViewBag.SinavSonucu = Entity.Personel_Sinavlar.Where(r => r.Bitti == 1 && r.Durum != "AÇIKLANMADI").ToList();
            var acikKalanSinav = Entity.Personel_Sinavlar.Where(r => r.IsActive == true && r.Sinif_Ogrenci.Personel_Ref == user && r.Bitti == 0).ToList();
            foreach (var item in acikKalanSinav)
            {
                //DateTime pTarih = item.SinavTarihi.Value;
                //TimeSpan girisSaati = item.GirisSaati.HasValue == false ? TimeSpan.Zero : item.GirisSaati.Value;
                //DateTime girisZaman = pTarih.Add(girisSaati);
                //DateTime bitisGerekenSure = girisZaman.Add(TimeSpan.FromMinutes(item.SinavSuresi.Value));

                if (item.SinavTarihi != null)
                {
                    DateTime tarih = item.SinavTarihi.Value;
                    try
                    {
                        TimeSpan bitisSaati = item.BitisSaati.Value;
                        DateTime bugun = DateTime.Now.Date;
                        TimeSpan aktifSaat = TimeSpan.Parse(DateTime.Now.ToShortTimeString());
                        if (tarih <= bugun && bitisSaati < aktifSaat)
                        {
                            var sinavBitir = Entity.Personel_Sinavlar.Where(t => t.ID == item.ID).First();
                            sinavBitir.Bitti = 1;
                            sinavBitir.CikisSaati = bitisSaati;
                            Entity.SaveChanges();
                        }
                    }
                    catch
                    {
                    }
                }
            }
            int ilkSinavId = acikKalanSinav.Select(r => r.ID).FirstOrDefault();
            if (acikKalanSinav.Count > 1)
            {
                TimeSpan bitisSaati = DateTime.Now.TimeOfDay;
                var sinavBitir = Entity.Personel_Sinavlar.OrderBy(q => q.ID).Where(t => t.ID == ilkSinavId).First();
                if (sinavBitir != null)
                {
                    sinavBitir.Durum = "KALDI";
                    sinavBitir.Bitti = 1;
                    sinavBitir.CikisSaati = bitisSaati;
                    Entity.SaveChanges();
                }

            }

            return View();
        }

        public ActionResult _SSoruDetay(int Sinav_Ref, int idx, int SoruNo, int Personel_Sinav_Ref)
        {
            int userRef = AktifKullanici.Aktif.User_Ref;
            Sinav_Sorular data = (from d in Entity.Sinav_Sorular
                                  where
                                      d.Sinavlar_Ref == Sinav_Ref &&
                                      d.ID == idx &&
                                      d.Sinavlar.Personel_Sinavlar.OrderByDescending(t => t.ID).Where(r => r.Sinif_Ogrenci.Personel_Ref == userRef).Select(t => t.Sinavlar_Ref).FirstOrDefault() == Sinav_Ref
                                  select d).First();
            //Sinav_Sorular data = Entity.Sinav_Sorular.Where(t => t.Sinavlar_Ref == Sinav_Ref && t.ID == idx).First();
            ViewBag.Personel_Sinav_Ref = Personel_Sinav_Ref;
            ViewBag.Soruno = SoruNo;
            ViewBag.SonsinifOgrenciRef = data.Sinavlar.Personel_Sinavlar.OrderByDescending(t => t.ID).Select(r => r.Sinif_Ogrenci_Ref).FirstOrDefault();

            return PartialView("_SoruIcerik", data);
        }

        public ActionResult _SinavKontrolBir()
        {
            //var kontrol = AktifKullanici.Aktif.ModullerRef;
            bool durum = false;
            List<Personel_Sinavlar> data = Entity.Personel_Sinavlar.Where(t => t.IsActive == true && t.IsDelete == false && t.Sinif_Ogrenci.Personel_Ref == user).ToList();
            var uniteler = Entity.OkunanUnitelers.Where(w => w.AtananBolumler.Sinif_Ogrenci.Personel_Ref == user).Select(q => new { q.Unite_Ref, q.Unite.Modul_Ref }).ToList();
            foreach (var item in data)
            {
                int modulref = uniteler.Select(e => e.Modul_Ref.Value).FirstOrDefault();
                //var modulref = Entity.Sinav_Sorular.Where(w => w.Sinavlar_Ref == item.Sinavlar_Ref).Select(q => q.UniteSorular.Unite.Modul_Ref).FirstOrDefault();
                int sinavKontrol = Entity.Unites.OrderByDescending(e => e.Sira).Where(e => e.Modul_Ref.Value == modulref).Select(q => q.ID).FirstOrDefault();
                durum = uniteler.Where(w => w.Unite_Ref == sinavKontrol).FirstOrDefault() == null ? false : true;
            }
            ViewBag.SinavKontrol = durum;
            return PartialView("_SinavKontrol", data);
        }

        public ActionResult _SinavDuyuruBir()
        {
            List<Personel_Sinavlar> data = Entity.Personel_Sinavlar.OrderByDescending(w => w.SinavTarihi).Where(t => t.IsActive == true && t.IsDelete == false && t.Sinif_Ogrenci.Personel_Ref == user).ToList();
            return PartialView("_SinavDuyurular", data);
        }

        public ActionResult _SinavCevaplar(int id)
        {
            var pcevap = Entity.SinavCevaplars.Where(y => y.Personel_Sinavlar_Ref == id).ToList();
            return PartialView("_SinavDetay", pcevap);
        }

        public JsonResult SinavCevap(int Personel_SinavRef, int SinavSoruRef, int? Cevap)
        {
            string durum = "";
            try
            {
                var control = Entity.SinavCevaplars.Where(t => t.Personel_Sinavlar_Ref == Personel_SinavRef && t.Sınav_Sorular_Ref == SinavSoruRef).FirstOrDefault();
                if (control != null)
                {
                    control.Personel_Sinavlar_Ref = Personel_SinavRef;
                    control.Sınav_Sorular_Ref = SinavSoruRef;
                    control.GirilenCevap = Cevap;

                    control.DogruCevap = Entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefault();

                    var unite = Entity.Sinav_Sorular.Where(r => r.ID == SinavSoruRef).Select(n => new { n.UniteSorular_Ref, n.SinavTipi_Ref }).FirstOrDefault();

                    bool sonuc = false;

                    if (control.DogruCevap == true)
                    {
                        sonuc = true;
                    }
                    else
                    {
                        sonuc = false;
                    }

                    control.Netice = sonuc;
                    Entity.SaveChanges();
                    durum = "Guncellendi";
                }
                else
                {
                    int unite_Ref = Entity.Sinav_Sorular.Where(t => t.ID == SinavSoruRef).Select(r => r.UniteSorular_Ref).FirstOrDefault();
                    var data = new SinavCevaplar();
                    data.Personel_Sinavlar_Ref = Personel_SinavRef;
                    data.Sınav_Sorular_Ref = SinavSoruRef;
                    data.GirilenCevap = Cevap;
                    data.DogruCevap = Entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefault();

                    var unite = Entity.Sinav_Sorular.Where(r => r.ID == SinavSoruRef).Select(n => new { n.UniteSorular_Ref, n.SinavTipi_Ref }).FirstOrDefault();


                    bool sonuc = false;


                    if (data.DogruCevap == true)
                    {
                        sonuc = true;
                    }
                    else
                    {
                        sonuc = false;
                    }

                    data.Netice = sonuc;
                    Entity.SinavCevaplars.Add(data);
                    Entity.SaveChanges();

                    durum = "Tamam";
                }

            }
            catch
            {
                durum = "İşlem sırasında hata meydana geldi tekrar deneyiniz!";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SinavaBasla(int id)
        {
            int user = AktifKullanici.Aktif.User_Ref;
            List<Sinav_Sorular> data = null;
            Personel_Sinavlar aktifSinav = Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefault();
            if (aktifSinav != null)
            {
                if (aktifSinav.BaslamaSaati == null)
                {
                    aktifSinav.SinavTarihi = DateTime.Today;
                    TimeSpan sure = TimeSpan.Parse("00:" + aktifSinav.SinavSuresi.Value.ToString() + ":00");
                    string bsaati = DateTime.Now.ToShortTimeString();
                    aktifSinav.BaslamaSaati = TimeSpan.Parse(bsaati);
                    string bitiss = DateTime.Now.Add(sure).ToShortTimeString();
                    aktifSinav.BitisSaati = TimeSpan.Parse(bitiss);
                    Entity.SaveChanges();
                }
                ViewBag.AktifSinav = aktifSinav.Sinavlar_Ref;
                ViewBag.PerSinavID = id;
                data = Entity.Sinav_Sorular.Where(r => r.Sinavlar_Ref == aktifSinav.Sinavlar_Ref && r.IsActive == true).ToList();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            return View(data);
        }

        public JsonResult SinavStart(int id)
        {
            string durum = "";
            Personel_Sinavlar aktifSinav = Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefault();
            if (aktifSinav.Basladi == 0)
            {
                aktifSinav.SinavTarihi = DateTime.Today;
                aktifSinav.Basladi = 1;
                aktifSinav.GirisSaati = TimeSpan.Parse(DateTime.Now.ToLongTimeString());
                Entity.SaveChanges();
                durum = "tamam";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SinavBitir(int aktifSinavID)
        {
            bool durum = false;
            try
            {
                Personel_Sinavlar aktifSinav = Entity.Personel_Sinavlar.Where(t => t.ID == aktifSinavID).FirstOrDefault();
                aktifSinav.Bitti = 1;
                aktifSinav.CikisSaati = TimeSpan.Parse(DateTime.Now.ToLongTimeString());
                //int apId = Convert.ToInt32(aktifSinav.Sinif_Ogrenci.AktifPersonelId);


                //SinavSonucuDurumTUR(apId);                
                //Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SinavSonuclandirma(int id)
        {
            string durum = string.Empty;
            try
            {
                using (Heas_UzakEntities Entity = new Heas_UzakEntities())
                {
                    List<SinavSonuclari> cevaplar = new List<SinavSonuclari>();
                    Personel_Sinavlar perSinavIsleme = Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefault();
                    List<Sinav_Sorular> sinavSorular = Entity.Sinav_Sorular.Where(r => r.Sinavlar_Ref == perSinavIsleme.Sinavlar_Ref && r.IsActive == true && r.IsDelete == false).ToList();

                    foreach (var item2 in sinavSorular)
                    {
                        var cevapkontrol = item2.SinavCevaplars.Where(r => r.Personel_Sinavlar_Ref == id).Select(t => new { t.Netice, t.Sınav_Sorular_Ref }).FirstOrDefault();

                        string cevabi = "";
                        if (cevapkontrol == null)
                        {
                            cevabi = "YOK";
                        }
                        else
                        {
                            if (cevapkontrol.Netice == true)
                            {
                                cevabi = "True";
                            }
                            else
                            {
                                cevabi = "False";
                            }
                        }
                        SinavSonuclari datax = new SinavSonuclari();
                        datax.PerID = id;
                        datax.SoruID = item2.ID;
                        datax.Cevap = cevabi;
                        datax.SoruTipi = item2.SinavTipi_Ref;
                        cevaplar.Add(datax);
                    }

                    int TeorikDogruSayisi = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "True").Count();
                    int TeorikYanlisSayisi = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "False").Count();
                    int TeorikBos = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "YOK").Count();

                    decimal TamPuan = 100;

                    int ToplamSoruTeorik = sinavSorular.Where(y => y.SinavTipi_Ref == 2).Count();
                    decimal soruPuanTeorik = 0;
                    if (ToplamSoruTeorik != 0)
                    {
                        soruPuanTeorik = TamPuan / ToplamSoruTeorik;
                    }
                    decimal SinavNotuTeorik = soruPuanTeorik * TeorikDogruSayisi;

                    try
                    {
                        string tcno = AktifKullanici.Aktif.identity;
                        string personelpath = Server.MapPath("~/DocumentFiles/PersonelWebResim/" + tcno + ".jpg");
                        string path = Server.MapPath("~/DocumentFiles");
                        if (!System.IO.File.Exists(path + "/SinavResim/" + perSinavIsleme.KlasorYolu + "/" + tcno + ".jpg"))
                        {
                            System.IO.File.Copy(personelpath, path + "/SinavResim/" + perSinavIsleme.KlasorYolu + "/" + tcno + ".jpg");
                            string[] egitimresimler = Directory.GetFiles(path + "/Egitim/" + tcno, "*.jpg");
                            int resindex = 0;
                            foreach (var item in egitimresimler)
                            {
                                resindex++;
                                System.IO.File.Copy(item, path + "/SinavResim/" + perSinavIsleme.KlasorYolu + "/" + resindex + ".jpg");
                            }
                        }
                    }
                    catch
                    {
                    }
                    string body = string.Empty;
                    string firma = perSinavIsleme.Sinif_Ogrenci.Personel.Firmalar.FirmaAdi;
                    string kisi = perSinavIsleme.Sinif_Ogrenci.Personel.Adi + " " + perSinavIsleme.Sinif_Ogrenci.Personel.Soyadi;
                    body = "Sayın <I>" + firma.ToUpper() + " Yetkilisi; </I> <br />";

                    string Netice = string.Empty;
                    List<ResultDetail> sonuc = ResimKontrolDogrulama.BenzerlikOranlar(perSinavIsleme.KlasorYolu, perSinavIsleme.Sinif_Ogrenci.Personel.TCNo);
                    IQueryable<Personel_Sinavlar> personelSinav = Entity.Personel_Sinavlar;
                    int user = AktifKullanici.Aktif.User_Ref;
                    int buyil = DateTime.Now.Date.Year;
                    int personelToplamSinav = personelSinav.Where(e => e.Sinif_Ogrenci.Personel_Ref == user && e.SinavTarihi.Value.Year == buyil).Count();

                    if (sonuc.Where(r => r.statu == false).Count() > 0)
                    {
                        Netice = "KALDI";
                        perSinavIsleme.SertifikaNo = "SINAV KURALLARINA UYULMADI";

                        var sinavdurum = "Sınav kurallarına uyulmadı! (" + personelToplamSinav + ") Sınava katılım";
                        body += kisi + " isimli personelinizin girmiş olduğu Kurs1 Eğitim Sınav sonucunda <b>[" + sinavdurum + "] başarısız olmuştur.</b> <br />";
                        body += "Bu mesaj bilgilendirme amacıyla tarafınıza gönderilmiştir.";
                    }
                    else
                    {
                        if (SinavNotuTeorik >= 70)
                        {
                            Netice = "GEÇTİ";
                            int oncekiyilkontrol = buyil - 1;
                            int aykontrol = DateTime.Now.Date.Month;

                            Personel_Sinavlar personeOncekiYilSinav = personelSinav.Where(e => e.Sinif_Ogrenci.Personel_Ref == user &&
                               e.SinavTarihi.Value.Year == oncekiyilkontrol &&
                               e.SinavTarihi.Value.Month == aykontrol
                               ).FirstOrDefault();

                            string psw = "SGHL/MİA/HEAŞ/UZP/";
                            string seri = "";
                            string adet = "";
                            int buYilSinavSayisi = personelSinav.Where(w => w.SinavTarihi.Value.Year == buyil).Select(q => q.ID).Count();
                            if (personeOncekiYilSinav != null)
                            {
                                seri = buyil + "B";
                            }
                            else
                            {
                                seri = buyil + "T";
                            }
                            adet = "000" + (buYilSinavSayisi < 9 ? "00" + buYilSinavSayisi.ToString() : buYilSinavSayisi.ToString());

                            psw += seri + "/" + adet;
                            perSinavIsleme.SertifikaNo = psw;
                        }
                        else
                        {
                            Netice = "KALDI";
                            perSinavIsleme.SertifikaNo = "TEKRAR SINAVA GİRECEK";
                        }
                        string sinavsonuc = SinavNotuTeorik >= 70 ? "BAŞARILI" : "BAŞARISIZ";               
                        var sinavdurum = SinavNotuTeorik < 70 ? sinavsonuc + " (" + personelToplamSinav + ") Sınava katılım" : sinavsonuc;
                        body += kisi + " isimli personelinizin girmiş olduğu Kurs1 Eğitim Sınav sonucunda <b>[" + sinavdurum + "] olmuştur.</b> dir. <br />";                        
                        body += "Bu mesaj bilgilendirme amacıyla tarafınıza gönderilmiştir.";
                    }
                    perSinavIsleme.Bitti = 1;
                    perSinavIsleme.CikisSaati = TimeSpan.Parse(DateTime.Now.ToLongTimeString());
                    perSinavIsleme.DogruSayisiTeorik = TeorikDogruSayisi;
                    perSinavIsleme.YanlisSayisiTeorik = TeorikYanlisSayisi;
                    perSinavIsleme.SinavNotuTeorik = SinavNotuTeorik;
                    perSinavIsleme.TeorikBos = TeorikBos;
                    perSinavIsleme.SinavAciklama = true;
                    perSinavIsleme.DuyuruTarihi = DateTime.Now.Date;
                    perSinavIsleme.Durum = Netice;
                    int sonucss = Entity.SaveChanges();

                    if (SinavNotuTeorik >= 70)
                    {
                        durum = SonucGonder(perSinavIsleme/*, resimler*/);
                    }

                    List<string> adres = new List<string>();
                    adres.Add(perSinavIsleme.Sinif_Ogrenci.Personel.Firmalar.Email);
                    
                    try
                    {
                        MailHelper.SendMail(adres, "Personel 'Kurs1' sınav sonuc bildirimi", body);
                        durum = "Personel kayıt işlemi başarıyla gerçekleşti";
                    }
                    catch
                    {
                        durum = "E-Posta bilgisi alıcılara gönderilmedi!";
                    }
                }
            }
            catch (Exception ex)
            {
                durum = ex.Message;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public string SonucGonder(Personel_Sinavlar data/*, string[] resimler*/)
        {
            string sonucServis = string.Empty;
            try
            {
                //try
                //{
                //    HeasEgitimClient sonucGonder = new HeasEgitimClient();
                //    sonucGonder.SinavSonuc(new heasEgitimServiceInput
                //    {
                //        kimlikNo = data.Sinif_Ogrenci.Personel.TCNo,
                //        egitimSonucu = data.SinavNotuTeorik >= 70 ? 1 : 0,
                //        referansId = data.Sinif_Ogrenci.AktifPersonelId.Value.ToString(),
                //        resimler = resimler,
                //        sertifikaNo = data.SertifikaNo
                //    });
                //}
                //catch
                //{                    
                //}


                DataSerialize dataYeni = new DataSerialize
                {
                    kimlikNo = data.Sinif_Ogrenci.Personel.TCNo,
                    egitimSonucu = data.SinavNotuTeorik >= 70 ? 1 : 0,
                    referansId = data.Sinif_Ogrenci.AktifPersonelId.Value.ToString(),
                    //resimler = resimler,
                    sertifikaNo = data.SertifikaNo
                };
                sonucServis = KartAktarimRest.POSTHeas(dataYeni);
            }
            catch (Exception ex)
            {
                sonucServis = ex.Message;
            }
            return sonucServis;
        }

        //protected SinavSonuc SinavSonucuDurumTUR(string aktifPersonelId)
        //{
        //    int apID = Convert.ToInt32(aktifPersonelId);          
        //    var data = Entity.Personel_Sinavlar.OrderByDescending(q => q.ID).
        //        Where(e => e.Sinif_Ogrenci.AktifPersonelId == apID).
        //        Select(w =>
        //            new SinavSonuc
        //            {
        //                AdSoyad = w.Sinif_Ogrenci.Personel.Adi + "_" + w.Sinif_Ogrenci.Personel.Soyadi,
        //                Not = w.SinavNotuTeorik.Value,
        //                KlasorYol = w.KlasorYolu,
        //                AktifPersonelId = w.Sinif_Ogrenci.AktifPersonelId.Value.ToString(),
        //                SertifikaNo = w.SertifikaNo
        //            }).FirstOrDefault();

        //    string path = System.Web.HttpContext.Current.Server.MapPath("~/DocumentFiles/Personel/" + data.KlasorYol);
        //    string[] filePaths = Directory.GetFiles(path);
        //    System.Collections.Generic.List<String> Rsmler = new System.Collections.Generic.List<String>();
        //    foreach (var adres in filePaths)
        //    {
        //        using (System.Drawing.Image image = System.Drawing.Image.FromFile(adres))
        //        {
        //            using (MemoryStream m = new MemoryStream())
        //            {
        //                image.Save(m, image.RawFormat);
        //                byte[] imageBytes = m.ToArray();
        //                // Convert byte[] to Base64 String
        //                string base64String = Convert.ToBase64String(imageBytes);
        //                Rsmler.Add(base64String);
        //            }
        //        }

        //    }
        //    for (int i = 0; i < Rsmler.Count; i++)
        //    {
        //        data.Resimler[i] = Rsmler[i];
        //    }
        //    return data;
        //}

        public ActionResult KameraKontrol(string name, string surname)
        {
            ViewBag.adi = name;
            ViewBag.soyadi = surname;
            return View();
        }


        #endregion

        #region Test İşlemler
        public ActionResult TesteBasla(int id)
        {
            int user = AktifKullanici.Aktif.User_Ref;
            List<Test_Sorular> data = Entity.Test_Sorular.Where(r => r.Testler_Ref == id && r.IsActive == true).ToList();
            Personel_Testler aktifTest = Entity.Personel_Testler.Where(t => t.Sinif_Ogrenci.Personel_Ref == user && t.Testler_Ref == id && t.Bitti == 0).FirstOrDefault();
            int prsID = 0;
            int aktifTst = 0;
            if (aktifTest != null)
            {
                aktifTst = aktifTest.Testler_Ref;
                prsID = aktifTest.ID;
            }
            ViewBag.AktifTest = aktifTst;
            ViewBag.PerTestID = prsID;

            //else
            //{
            //    return RedirectToAction("Index", "Home");
            //}

            return View(data);
        }

        public ActionResult _TSoruDetay(int Test_Ref, int idx, int SoruNo)
        {
            int userRef = AktifKullanici.Aktif.User_Ref;
            Test_Sorular data = (from d in Entity.Test_Sorular
                                 where
                                     d.Testler_Ref == Test_Ref &&
                                     d.ID == idx &&
                                     d.Testler.Personel_Testler.Where(r => r.Sinif_Ogrenci.Personel_Ref == userRef).Select(t => t.Testler_Ref).FirstOrDefault() == Test_Ref
                                 select d).FirstOrDefault();
            //Sinav_Sorular data = Entity.Sinav_Sorular.Where(t => t.Sinavlar_Ref == Sinav_Ref && t.ID == idx).First();

            ViewBag.Soruno = SoruNo;
            ViewBag.TestRef = Test_Ref;
            return PartialView("_SoruIcerikTest", data);
        }

        public ActionResult _TestCevaplar(int id)
        {
            var pcevap = Entity.TestCevaplars.Where(y => y.Personel_Testler_Ref == id).ToList();
            return PartialView("_TestDetay", pcevap);
        }

        public JsonResult TestStart(int id, int sinif_Ogrenci_ref)
        {
            string durum = "";
            Personel_Testler aktifTest = new Personel_Testler();
            aktifTest.IsActive = true;
            aktifTest.IsDelete = false;
            aktifTest.Sinif_Ogrenci_Ref = sinif_Ogrenci_ref;
            aktifTest.Testler_Ref = id;
            aktifTest.TestAciklama = false;
            aktifTest.TestNotu = 0;
            aktifTest.Durum = "AÇIKLANMADI";
            aktifTest.DogruSayisi = 0;
            aktifTest.YanlisSayisi = 0;
            aktifTest.Bos = 0;
            aktifTest.TestTarihi = DateTime.Now.Date;
            aktifTest.GirisSaati = TimeSpan.Parse(DateTime.Now.ToLongTimeString());
            aktifTest.Basladi = 1;
            aktifTest.Bitti = 0;
            Entity.Personel_Testler.Add(aktifTest);
            Entity.SaveChanges();
            durum = "tamam";

            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TestBitir(int aktifTestID)
        {
            string durum = "";
            try
            {
                int user = AktifKullanici.Aktif.User_Ref;
                Personel_Testler aktifTest = Entity.Personel_Testler.Where(t => t.ID == aktifTestID).FirstOrDefault();
                aktifTest.Bitti = 1;
                aktifTest.CikisSaati = TimeSpan.Parse(DateTime.Now.ToLongTimeString());
                TimeSpan dakika = aktifTest.CikisSaati.Value - aktifTest.GirisSaati.Value;
                aktifTest.TestSuresi = dakika.Minutes == 0 ? 1 : dakika.Minutes;
                Entity.SaveChanges();
                bool sonucu = TestSonuclandirma(aktifTest.ID, aktifTest.Testler_Ref);
                if (sonucu)
                {
                    durum = "tamam";
                }
            }
            catch
            {
                durum = "hata";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        public bool TestSonuclandirma(int id, int testRef)
        {
            bool durum = false;
            try
            {
                //List<ResultDetail> resimDogrulama = ResimKontrolDogrulama.BenzerlikOranlar(testRef);
                //string filename = string.Empty;
                //double smilarity = 0;
                //bool statu = false;
                //string result = string.Empty;
                //System.Drawing.Image errorimage = null;
                //double kisibenzerlikortalamasi = 0;

                //for (int i = 0; i < resimDogrulama.Count; i++)
                //{
                //    filename = resimDogrulama[i].filename;
                //    smilarity = resimDogrulama[i].smilarity;
                //    kisibenzerlikortalamasi += smilarity;
                //    statu = resimDogrulama[i].statu;
                //    result = resimDogrulama[i].result;
                //    errorimage = resimDogrulama[i].errorimage;
                //}

                //double ortalamaSonucu = kisibenzerlikortalamasi / resimDogrulama.Count;
                //if (ortalamaSonucu < 65)
                //{
                //    durum = false;
                //}
                //else
                //{
                List<TestSonuclari> cevaplar = new List<TestSonuclari>();
                var testSorular = Entity.Test_Sorular.Where(r => r.Testler_Ref == testRef && r.IsActive == true && r.IsDelete == false).ToList();
                foreach (var item2 in testSorular)
                {
                    var cevapkontrol = item2.TestCevaplars.Where(r => r.Personel_Testler_Ref == id).Select(t => new { t.Netice, t.Test_Sorular_Ref }).FirstOrDefault();
                    string cevabi = "";
                    if (cevapkontrol == null)
                    {
                        cevabi = "YOK";
                    }
                    else
                    {
                        if (cevapkontrol.Netice == true)
                        {
                            cevabi = "True";
                        }
                        else
                        {
                            cevabi = "False";
                        }
                    }
                    TestSonuclari datax = new TestSonuclari();
                    datax.PerID = id;
                    datax.SoruID = item2.ID;
                    datax.Cevap = cevabi;
                    cevaplar.Add(datax);
                }

                int DogruSayisi = cevaplar.Where(t => t.Cevap == "True").Count();
                int YanlisSayisi = cevaplar.Where(t => t.Cevap == "False").Count();
                int Bos = cevaplar.Where(t => t.Cevap == "YOK").Count();

                decimal TamPuan = 100;

                int ToplamSoru = testSorular.Count();

                decimal soruPuan = 0;
                if (ToplamSoru != 0)
                {
                    soruPuan = TamPuan / ToplamSoru;
                }

                decimal TestNotu = soruPuan * DogruSayisi;

                string Netice = "";
                if (TestNotu >= 70)
                {
                    Netice = "GEÇTİ";
                }
                else
                {
                    Netice = "KALDI";
                }

                var perTestIsleme = Entity.Personel_Testler.Where(t => t.ID == id).FirstOrDefault();
                perTestIsleme.DogruSayisi = DogruSayisi;
                perTestIsleme.YanlisSayisi = YanlisSayisi;
                perTestIsleme.TestNotu = TestNotu;
                perTestIsleme.Bos = Bos;
                perTestIsleme.Durum = Netice;
                perTestIsleme.TestAciklama = true;
                Entity.SaveChanges();
                durum = true;
                //}
            }
            catch
            {
                durum = false;
            }
            return durum;
        }

        public ActionResult Modul_Testler(int id, int? sonkonu)
        {
            int user_ref = AktifKullanici.Aktif.User_Ref;
            var testler = Entity.Testlers.Where(t => t.UniteModul_Ref == id && t.IsActive == true && t.IsDelete == false).ToList();
            ViewBag.Testler = Entity.Personel_Testler.Where(t => t.Sinif_Ogrenci.Personel_Ref == user_ref && t.Testler.UniteModul_Ref == id).ToList();
            ViewBag.Idx = id;
            List<Personel_Testler> data = Entity.Personel_Testler.Where(t => t.IsActive == true && t.IsDelete == false && t.Sinif_Ogrenci.Personel_Ref == user_ref).ToList();
            ViewBag.TestCevaplar = data;
            var bittiKontrol = Entity.Personel_Testler.Where(e => e.Bitti == 1 && e.TestAciklama == false).ToList();
            foreach (var itembt in bittiKontrol)
            {
                if (itembt != null)
                {
                    TestBitir(itembt.ID);
                }
            }
            ViewBag.Sonkonu = sonkonu ?? 0;
            return View(testler);
        }


        public JsonResult TestCevap(int Personel_TestRef, int TestSoruRef, int? Cevap, int? AltCevap)
        {
            string durum = "";
            try
            {
                var control = Entity.TestCevaplars.Where(t => t.Personel_Testler_Ref == Personel_TestRef && t.Test_Sorular_Ref == TestSoruRef).FirstOrDefault();
                if (control != null)
                {
                    control.Personel_Testler_Ref = Personel_TestRef;
                    control.Test_Sorular_Ref = TestSoruRef;
                    control.GirilenCevap = Cevap;
                    control.AltCevap = AltCevap;
                    control.DogruCevap = Entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefault();
                    control.DogruAltCevap = Entity.AltSiklars.Where(y => y.ID == AltCevap).Select(r => r.AltDogruSecenek).FirstOrDefault();

                    var unite = Entity.Test_Sorular.Where(r => r.ID == TestSoruRef).Select(n => new { n.UniteSorular_Ref }).FirstOrDefault();
                    int altsik = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == unite.UniteSorular_Ref).Count();


                    bool sonuc = false;

                    if (altsik != 0)
                    {
                        if (control.DogruCevap == true && control.DogruAltCevap == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    else if (altsik == 0)
                    {
                        var secenek = Entity.SoruSiklaris.Where(r => r.ID == Cevap && r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefault();
                        if (secenek == "a)")
                        {
                            if (control.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                        else
                        {

                            if (control.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                    }

                    control.Netice = sonuc;
                    Entity.SaveChanges();
                    durum = "Guncellendi";
                }
                else
                {
                    int unite_Ref = Entity.Test_Sorular.Where(t => t.ID == TestSoruRef).Select(r => r.UniteSorular_Ref).FirstOrDefault();
                    var data = new TestCevaplar();
                    data.Personel_Testler_Ref = Personel_TestRef;
                    data.Test_Sorular_Ref = TestSoruRef;
                    data.GirilenCevap = Cevap;
                    data.AltCevap = AltCevap;
                    data.DogruCevap = Entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefault();
                    data.DogruAltCevap = Entity.AltSiklars.Where(y => y.ID == AltCevap).Select(r => r.AltDogruSecenek).FirstOrDefault();

                    var unite = Entity.Test_Sorular.Where(r => r.ID == TestSoruRef).Select(n => new { n.UniteSorular_Ref }).FirstOrDefault();
                    int altsik = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == unite.UniteSorular_Ref).Count();

                    bool sonuc = false;
                    if (altsik != 0)
                    {
                        if (data.DogruCevap == true && data.DogruAltCevap == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    else if (altsik == 0)
                    {
                        var secenek = Entity.SoruSiklaris.Where(r => r.ID == Cevap && r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefault();
                        if (secenek == "a)")
                        {
                            if (data.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                        else
                        {

                            if (data.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }

                        }
                    }

                    data.Netice = sonuc;
                    Entity.TestCevaplars.Add(data);
                    Entity.SaveChanges();

                    durum = "Tamam";
                }

            }
            catch
            {
                durum = "İşlem sırasında hata meydana geldi tekrar deneyiniz!";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion



    }
    public class TestSonuclari : IDisposable
    {
        public int PerID { get; set; }
        public int SoruID { get; set; }
        public string Cevap { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class SoruSiklariLocal : IDisposable
    {

        public int ID { get; set; }
        public string SoruSikki { get; set; }
        public string CevapIcerik { get; set; }
        public bool DogruSecenek { get; set; }


        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class SinavSonuc : IDisposable
    {
        public String AktifPersonelId { get; set; }
        public String AdSoyad { get; set; }
        public Decimal Not { get; set; }
        public String KlasorYol { get; set; }
        public String[] Resimler { get; set; }
        public String SertifikaNo { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
