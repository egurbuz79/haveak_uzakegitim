﻿
using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Haveak_Pro.Areas.SINAV.Controllers
{
    public class SoruIncelemeController : Controller
    {
        //
        // GET: /SINAV/SoruInceleme/
        Heas_UzakEntities Entity = new Heas_UzakEntities();
        public ActionResult Soru_Detay(int Sinav_Ref, int idx, int SoruNo)
        {     
            int userRef = AktifKullanici.Aktif.User_Ref;
            Sinav_Sorular data = (from d in Entity.Sinav_Sorular
                                  where
                                      d.Sinavlar_Ref == Sinav_Ref &&
                                      d.ID == idx &&
                                      d.Sinavlar.Personel_Sinavlar.Where(r => r.Sinif_Ogrenci.Personel_Ref == userRef).Select(t => t.Sinavlar_Ref).FirstOrDefault() == Sinav_Ref
                                  select d).First();           
            ViewBag.Soruno = SoruNo;
            return View(data);
        }

    }
}
