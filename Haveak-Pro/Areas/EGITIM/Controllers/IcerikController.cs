﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Haveak_Pro.Areas.EGITIM.Controllers
{
    [Authorize(Roles = UserRoleName.EGITIM_OgrenciGirisi)]
    public class IcerikController : Controller
    {
        //
        // GET: /EGITIM/Icerik/
        Heas_UzakEntities Entity = new Heas_UzakEntities();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult BolumBasliklari(int id)
        {
            int kullaniciID = AktifKullanici.Aktif.User_Ref;
            var data = Entity.Unites.OrderBy(t => t.Sira).Where(t => t.Modul_Ref == id && t.IsActive == true && t.IsDelete == false).Select(e => new
            {
                e.ID,
                e.UniteAdi,
                abRef = e.OkunanUnitelers.Where(r => r.AtananBolumler.Sinif_Ogrenci.Personel_Ref == kullaniciID).Select(q => q.AtananBolumler_Ref).Count(),
                obref = e.OkunanUnitelers.OrderByDescending(q => q.AtananBolumler_Ref).Where(r => r.AtananBolumler.Sinif_Ogrenci.Personel_Ref == kullaniciID).Select(q => q.AtananBolumler_Ref).FirstOrDefault()
            }).ToList();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Konu(int id, int? atananblm)
        {
            List<Egitim_Arsivi> Uniteler = Entity.Egitim_Arsivi.Where(r => r.IsActive == true && r.IsDelete == false && r.Unite_Ref == id).ToList();
            var modulID = Entity.Unites.Where(t => t.ID == id && t.IsDelete == false).Select(r => new { r.Modul_Ref, r.Sira }).FirstOrDefault();
            ViewBag.UniteID = Entity.Unites.OrderBy(r => r.Sira).Where(t => t.Modul_Ref == modulID.Modul_Ref && t.IsActive && !t.IsDelete).Select(y => y.ID).ToList();
            //int atanaBolumRef = Entity.KursModullers.Where(e => e.).First();      
            ViewBag.Sirano = modulID.Sira ?? 0;
            ViewBag.AtananBolum = atananblm ?? 0;
            return View(Uniteler);
        }

        public JsonResult ImageCapture(string imagePath, int sortno)
        {
            bool durum = false;
            try
            {
                int imageno = 0;
                switch (sortno)
                {
                    case 1:
                        imageno = 1;
                        break;
                    case 6:
                        imageno = 2;
                        break;
                    default:
                        imageno = 3;
                        break;
                }
                //DateTime tarih = DateTime.Now;
                //string saat = DateTime.Now.ToString("hh_mm_ss");
                //string zaman = tarih.Year.ToString() + tarih.Month.ToString() + tarih.Day.ToString() + "_" + saat;
                string tcno = AktifKullanici.Aktif.identity;

                var path = Server.MapPath("~/DocumentFiles/Egitim/" + tcno);
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                var pathnew = path + "/" + imageno + ".jpg";
                Image image = Base64ToImage(imagePath);
                image.Save(pathnew, ImageFormat.Jpeg);
                durum = true;
            }
            catch(Exception ex)
            {
                durum = false;
            }
            return Json(durum);
        }

        protected Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }
        //public JsonResult AtananBolumGetir(int SinifOgrenci_Ref)
        //{ 

        //}
        //public JsonResult Testler(int modulRef)
        //{
        //    var data = Entity.Testlers.Where(t => t.UniteModul_Ref == modulRef && t.IsActive == true && t.IsDelete == false).Select(r => new { r.ID, r.TestNo, r.UniteModul_Ref}).ToList();
        //    return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        //}

    }
}
