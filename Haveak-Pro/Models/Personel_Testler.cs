//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Haveak_Pro.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Personel_Testler
    {
        public Personel_Testler()
        {
            this.TestCevaplars = new HashSet<TestCevaplar>();
        }
    
        public int ID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public int Sinif_Ogrenci_Ref { get; set; }
        public int Testler_Ref { get; set; }
        public bool TestAciklama { get; set; }
        public decimal TestNotu { get; set; }
        public string Durum { get; set; }
        public Nullable<int> DogruSayisi { get; set; }
        public Nullable<int> YanlisSayisi { get; set; }
        public Nullable<int> Bos { get; set; }
        public Nullable<System.DateTime> TestTarihi { get; set; }
        public Nullable<System.TimeSpan> GirisSaati { get; set; }
        public Nullable<System.TimeSpan> CikisSaati { get; set; }
        public Nullable<int> TestSuresi { get; set; }
        public int Basladi { get; set; }
        public int Bitti { get; set; }
    
        public virtual Sinif_Ogrenci Sinif_Ogrenci { get; set; }
        public virtual Testler Testler { get; set; }
        public virtual ICollection<TestCevaplar> TestCevaplars { get; set; }
    }
}
