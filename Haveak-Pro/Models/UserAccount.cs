﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Models
{
    public class UserAccount : IIdentity
    {
        List<string> roller = new List<string>();
        Dictionary<string, object> bilgiler = new Dictionary<string, object>();
        public int Id { get; set; }
        public string AdSoyad { get; set; }
        public string AuthenticationType
        {
            get { return "Forms"; }
        }
        public bool IsAuthenticated
        {
            get { return true; }
        }
        private string kullaniciAdi = "";

        public string Name
        {
            get
            {
                return kullaniciAdi;
            }
        }
        public List<string> Roller { get; set; }
        public List<string> Moduller { get; set; }
        public UserAccount(string name)
        {
            this.kullaniciAdi = name;
        }
        public UserAccount(string name, List<string> roles)
        {
            this.kullaniciAdi = name;
            if (roles != null)
                this.Roller = roles;
        }
        public KimlikPrensibi ToPrensip()
        {
            KimlikPrensibi prensip = new KimlikPrensibi(this);
            return prensip;
        }
    }
    public class KimlikPrensibi : IPrincipal
    {
        UserAccount kimlik;
        public IIdentity Identity
        {
            get { return kimlik; }
        }
        public bool IsInRole(List<string> role)
        {
            return kimlik.Roller == role;
        }
        internal KimlikPrensibi(UserAccount kimlik)
        {
            this.kimlik = kimlik;
        }

        #region IPrincipal Members


        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
    public static class AktifKullanici
    {
        /// <summary>
        /// İstediğiniz Session Üzerinden Set Edebilirsiniz :
        /// </summary>
        /// <param name="session"></param>
        /// <param name="kimlik"></param>
        public static void SetKimlikToSession(this HttpSessionStateBase session, UserAccount kimlik)
        {
            if (session != null)
                session["Kimlik"] = kimlik;
        }
        /// <summary>
        /// İstediğiniz Session Üzerinden Alabilirsiniz
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static UserAccount GetKimlikFromSession(this HttpSessionStateBase session)
        {
            if (session != null && session["Kimlik"] != null)
            {
                return (UserAccount)session["Kimlik"];
            }
            return null;
        }

        public static bool SetUserFromSession(this HttpSessionStateBase session)
        {
            if (session != null && session["Kimlik"] != null)
            {
                var kimlik = (UserAccount)session["Kimlik"];
                HttpContext.Current.User = kimlik.ToPrensip();
                return true;
            }
            return false;
        }

        public static int Id
        {
            get
            {
                var ki = (UserAccount)HttpContext.Current.User.Identity;
                return ki.Id;
            }
        }
        public static UserAccount Aktif
        {

            get
            {
                UserAccount userControl;
                try
                {
                    userControl = (UserAccount)HttpContext.Current.User.Identity;
                }
                catch (Exception)
                {
                    userControl = null;
                }
                return userControl;
            }
        }

    }
}