﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Models
{
    public static class UserRoleName
    {

        public const string PANEL_AdminGiris = "PANEL/AdminGirisi";
        //public const string GUVENLIK_OgrenciGirisi = "GUVENLIK/OgrenciGirisi";
        public const string EGITIM_OgrenciGirisi = "EGITIM/OgrenciGirisi";
        public const string SINAVLAR_OgrenciGirisi = "SINAVLAR/OgrenciGirisi";       
        //public const string ADMIN_BagajTehlikelimadde = "ADMIN/BagajTehlikelimadde";
        public const string ADMIN_Egitim = "ADMIN/Egitim";
        public const string ADMIN_Sinav = "ADMIN/Sinav";
        public const string ADMIN_PersonelKullanici = "ADMIN/PersonelKullanici";
        //public const string PERSONNEL_PersonelİletişimBilgileri = "PERSONNEL/PersonelİletişimBilgileri";
       


        public static string[] GetRoleNames()
        {
            var fields = typeof(UserRoleName).GetFields();
            List<string> roles = new List<string>();
            foreach (var field in fields)
            {
                var objFielValue = field.GetValue(null);
                if (objFielValue != null)
                    roles.Add(objFielValue.ToString());
            }
            return roles.ToArray();
        }

        public static bool InRole(this string String)
        {
            return HttpContext.Current.User.IsInRole(String);
        }
    }

}