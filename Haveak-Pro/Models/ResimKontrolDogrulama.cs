﻿
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;


namespace Haveak_Pro.Models
{
    public static class ResimKontrolDogrulama
    {
        private static List<ResultDetail> Matching(string pathAddress, string orgImage = "")
        {
            string imgPath = HttpContext.Current.Server.MapPath("~/DocumentFiles/SinavResim/") + pathAddress;
            List<ResultDetail> results = new List<ResultDetail>();

            if (Directory.Exists(imgPath))
            {
                string[] dbImages = Directory.GetFiles(imgPath, "*.jpg");
                for (int i = 0; i < dbImages.Length; i++)
                {
                    string filename = Path.GetFileName(dbImages[i]);
                    if (filename != "1.jpg" && filename != "2.jpg" && filename != "3.jpg")
                    {
                        CascadeClassifier face = new CascadeClassifier(HttpContext.Current.Server.MapPath("~/haarcascade_frontalface_default.xml"));
                        ResultDetail detail = new ResultDetail();
                        Image<Bgr, byte> source = null;
                        if (!string.IsNullOrEmpty(orgImage))
                        {
                            source = new Image<Bgr, byte>(orgImage);
                        }
                        else
                        {
                            source = new Image<Bgr, byte>(dbImages[0]);
                        }
                        Image<Bgr, byte> template = new Image<Bgr, byte>(dbImages[i]);

                        double[] minValues, maxValues;
                        using (Image<Gray, float> result = source.MatchTemplate(template, Emgu.CV.CvEnum.TemplateMatchingType.CcoeffNormed))
                        {
                            Point[] minLocations, maxLocations;
                            result.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);
                            if (maxValues[0] > 0.7)
                            {
                                detail.statu = true;
                                detail.result = "Kişi Doğrulandı";
                            }
                            else
                            {
                                detail.statu = false;
                                detail.result = "Kişi Doğrulanmadı!";
                                detail.errorimage = template.Bitmap;
                            }
                            detail.filename = dbImages[i];
                            detail.smilarity = Math.Ceiling((double)maxValues[0] * 100) / 100;
                            results.Add(detail);
                        }
                    }
                }

            }
            return results;
        }

        public static List<ResultDetail> BenzerlikOranlar(string klasorYolu, string tcno)
        {
            //string orjResim = string.Empty;           
            string orjResim = HttpContext.Current.Server.MapPath("~/DocumentFiles/SinavResim/" + klasorYolu + "/" + tcno + ".jpg");
            if (!System.IO.File.Exists(orjResim))
            {
                orjResim = "";
            }
            List<ResultDetail> sonuc = ResimKontrolDogrulama.Matching(klasorYolu, orjResim);
            return sonuc;
        }
    }

    public class ResultDetail
    {
        public bool statu { get; set; }
        public string filename { get; set; }
        public double smilarity { get; set; }
        public string result { get; set; }
        public Image errorimage { get; set; }
    }
}