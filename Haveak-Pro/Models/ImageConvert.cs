﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Haveak_Pro.Models
{
    public class ImageConvert
    {
        public string ImageWrite(string image, string tckn)
        {
            string serverPath = string.Empty;
            //try
            //{
            //    serverPath = System.Web.Hosting.HostingEnvironment.MapPath("~/DocumentFiles/Personel/");
            //}
            //catch
            //{
            serverPath = HttpContext.Current.Server.MapPath("~/DocumentFiles/Personel/");
            //}
            string path = serverPath + tckn + ".txt";
            if (!System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(path))
                {
                    sw.WriteLine("data:image/jpeg;base64," + image);
                }
            }
            return tckn + ".txt";
        }

        public string ImageReader(string adres)
        {
            string serverPath = string.Empty;
            string image = string.Empty;
            //try
            //{
            //    serverPath = System.Web.Hosting.HostingEnvironment.MapPath("~/DocumentFiles/Personel/");
            //}
            //catch
            //{
            serverPath = HttpContext.Current.Server.MapPath("~/DocumentFiles/Personel/");
            //}
            string path = serverPath + adres;
            if (System.IO.File.Exists(path))
            {
                image = File.ReadAllText(path);
            }
            else
            {
                image = "/DocumentFiles/Personel/RYok.jpg";
            }
            return image;
        }

        public List<EducationImages_View> getEducationImages(string tcno)
        {
            var filesLocation = HttpContext.Current.Server.MapPath("~/DocumentFiles/PersonelWebResim");
            DirectoryInfo hdDirectoryInWhichToSearch = new DirectoryInfo(filesLocation);
            FileInfo[] filesInDir = hdDirectoryInWhichToSearch.GetFiles("*" + tcno + "*.*");
            List<EducationImages_View> files = new List<EducationImages_View>();
            foreach (FileInfo foundFile in filesInDir)
            {
                EducationImages_View dgr = new EducationImages_View();
                dgr.resim = foundFile.Name;
                dgr.otarih = foundFile.LastWriteTime.ToShortDateString();
                dgr.osaat = foundFile.LastWriteTime.ToLongTimeString();
                files.Add(dgr);
            }
            return files;
        }
    }
}