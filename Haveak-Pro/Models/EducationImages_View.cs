﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Models
{
    public class EducationImages_View
    {
        public string resim { get; set; }
        public string otarih { get; set; }
        public string osaat { get; set; }
    }
}