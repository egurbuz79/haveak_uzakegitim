﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Security;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web;

namespace Haveak_Pro.Models
{
    public class KimlikIdentity : IIdentity
    {

        List<string> roller = new List<string>();
        List<string> moduller = new List<string>();


        Dictionary<string, object> bilgiler = new Dictionary<string, object>();
        public int Id { get; set; }
        public string identity { get; set; }
        public string EName { get; set; }
        public string ESurName { get; set; }
        public string UserName { get; set; }
        public string EMail { get; set; }
        public string Telephone { get; set; }
        public string Picture { get; set; }       
        public bool? Notification { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int UserType { get; set; }
        public int User_Ref { get; set; }
        public string SecilenModul { get; set; }
        public List<string> Roller { get; set; }
        public List<string> Moduller { get; set; }
        public List<int> ModullerRef { get; set; }
        public int? FirmaId { get; set; }
        public string FirmaAdi { get; set; }
        private string _serviceImage;
        public string ServiceImage
        {
            get {                 
                return "data:image/jpeg;base64,"+ _serviceImage;
            }
            set { _serviceImage = value; }
        }
        

        //  public Dictionary<string, object> Bilgiler { get { return bilgiler; } set { bilgiler = value; } }

        public string AuthenticationType
        {
            get { return "Forms"; }
        }
        public bool IsAuthenticated
        {
            get { return true; }
        }
        private string kullaniciAdi = "";
        public string Name
        {
            get
            {
                return kullaniciAdi;
            }
        }

        public KimlikIdentity(string name)
        {
            this.kullaniciAdi = name;
        }
        public KimlikIdentity(string name, string[] roles)
        {
            this.kullaniciAdi = name;
            if (roles != null)
                this.Roller.AddRange(roles);
        }
        public KimlikPrensibi ToPrensip()
        {
            KimlikPrensibi prensip = new KimlikPrensibi(this);
            return prensip;
        }
    }

    public class KimlikPrensibi : IPrincipal
    {
        KimlikIdentity kimlik;
        public IIdentity Identity
        {
            get { return kimlik; }
        }
        public bool IsInRole(string role)
        {
            return kimlik.Roller.IndexOf(role) >= 0;
        }
        internal KimlikPrensibi(KimlikIdentity kimlik)
        {
            this.kimlik = kimlik;
        }
    }
    public static class AktifKullanici
    {
        /// <summary>
        /// İstediğiniz Session Üzerinden Set Edebilirsiniz :
        /// </summary>
        /// <param name="session"></param>
        /// <param name="kimlik"></param>
        public static void SetKimlikToSession(this HttpSessionStateBase session, KimlikIdentity kimlik)
        {
            if (session != null)
                session["Kimlik"] = kimlik;
        }
        /// <summary>
        /// İstediğiniz Session Üzerinden Alabilirsiniz
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static KimlikIdentity GetKimlikFromSession(this HttpSessionStateBase session)
        {
            if (session != null && session["Kimlik"] != null)
            {
                return (KimlikIdentity)session["Kimlik"];
            }
            return null;
        }

        public static bool SetUserFromSession(this HttpSessionStateBase session)
        {
            if (session != null && session["Kimlik"] != null)
            {
                var kimlik = (KimlikIdentity)session["Kimlik"];
                HttpContext.Current.User = kimlik.ToPrensip();
                return true;
            }
            return false;
        }

        public static int Id
        {
            get
            {
                var ki = (KimlikIdentity)HttpContext.Current.User.Identity;
                return ki.Id;
            }
        }

        public static KimlikIdentity Aktif
        {
            get
            {
                try
                {
                    return (KimlikIdentity)HttpContext.Current.User.Identity;
                }
                catch
                {
                    try
                    {
                        var context = new RequestContext(new HttpContextWrapper(System.Web.HttpContext.Current), new RouteData());
                        var urlHelper = new UrlHelper(context);
                        var url = urlHelper.Action("Giris", "Kontrol", new { area = "" });
                        //FormsAuthentication.SignOut();
                        //HttpContext.Current.Session.Abandon();
                        HttpContext.Current.Response.Redirect(url, true);
                    }
                    catch
                    {
                        HttpContext.Current.Response.Redirect("~/Kontrol/Giris",true);
                    }
                    return null;
                }
            }
        }

    }
}